export default {
  loginUrl: "https://www.abdelrahman-amr.com/api/login_check",
  getUserInformation: "https://www.abdelrahman-amr.com/api/user/me",
  getUserAudios: "https://www.abdelrahman-amr.com/api/user/orders",
  getAllAudios: "https://www.abdelrahman-amr.com/web/app_dev.php/app/sounds",
  latestImagesUr: "",
  resetPassword: "https://abdelrahman-amr.com/resetting/request",
  register: "https://www.abdelrahman-amr.com/web/app_dev.php/app/register",
  buyProducts: "https://74.208.236.45:80/web/app_dev.php/api/user/post/order",
};
