/**
 * The initial values for the Media redux state.
 */
export const INITIAL_STATE = {
  userPlaylists: [],
  playlistToDisplay: null,
  isAddPlaylistModalVisible: false,
  isAddPlaylistInfoModalVisible: false,
  isAddSongsToPlaylistModalVisible: false,
  isPlaying: false,
  isAutoPlay: false,
  selectedSongs: [],
  userAudios: [],
  latestAudios: [],
  muted: false,
  volume: 1.0,
  songToPlay: null,
  isPlayerShowen: false,
  audiosToPlay: [],
  audioNamesArray: [],
  indexToPlay: 0,
  nbrParts: 0,
  songPlaying : null,
  isFullPlayerShow: false,
  audio: null,
};
