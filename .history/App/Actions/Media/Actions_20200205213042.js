import * as Types from "./Types";
import axios from "axios";
import ApiService from "../../Services/ApiService";
import _ from "lodash";
import RNFetchBlob from "rn-fetch-blob";
import reactotron from "reactotron-react-native";
export const setPlaylist = playlist => {
  return dispatch => {
    dispatch({ type: Types.SET_PLAYLIST, playlist });
  };
};

export const openPlaylistModal = () => {
  return dispatch => {
    dispatch({ type: Types.OPEN_PLAYLIST_MODAL_VISIBLE });
  };
};

export const closePlaylistModal = () => {
  return dispatch => {
    dispatch({ type: Types.CLOSE_PLAYLIST_MODAL_VISIBLE });
  };
};
export const openPlaylistInfoModal = () => {
  return dispatch => {
    dispatch({ type: Types.OPEN_PLAYLISTINFO_MODAL_VISIBLE });
  };
};

export const closePlaylistInfoModal = () => {
  return dispatch => {
    dispatch({ type: Types.CLOSE_PLAYLISTINFO_MODAL_VISIBLE });
  };
};

export const openAddSongsModal = () => {
  return dispatch => {
    dispatch({ type: Types.OPEN_ADD_SONGS_MODAL_VISIBLE });
  };
};

export const closeAddSongsModal = () => {
  return dispatch => {
    dispatch({ type: Types.CLOSE_ADD_SONGS_MODAL_VISIBLE });
  };
};

export const changePlaylistTitle = title => {
  return dispatch => {
    dispatch({ type: Types.CHANGE_PLAYLIST_TITLE, title });
  };
};

export const createPlaylist = playlist => {
  return (dispatch, getState) => {
    const userPlaylistCopy = getState().media.userPlaylists.slice();
    userPlaylistCopy.push(playlist);
    dispatch({ type: Types.CREATE_PLAYLIST, userPlaylistCopy });
  };
};

export const deletePlaylist = playlist => {
  return dispatch => {
    dispatch({ type: Types.DELETE_PLAYLIST, playlist });
  };
};

export const addSongToPlaylist = song => {
  return dispatch => {
    dispatch({ type: Types.ADD_SONG_TO_PLAYLIST, song });
  };
};

export const toggleFullSizePlayer = () => {
  return dispatch => {
    dispatch({ type: Types.TOGGLE_FULL_SIZE });
  };
};

/**
 * Remove Song from Playlist
 * @param {song} song
 */
export const removeSongFromPlaylist = song => {
  return dispatch => {
    dispatch({ type: Types.REMOVE_SONG_FROM_PLAYLIST, song });
  };
};

/**
 * Handle Auto Play
 */
export const handleAutoPlay = () => {
  return dispatch => {
    dispatch({ type: Types.HANDLE_AUTOPLAY });
  };
};

/**
 * Set Playlist to download
 */
export const togglePlaylist = playlist => {
  return (dispatch, getState) => {
    let userPlayListsCopy = [];
    let updatedPlaylist = {};
    const { userPlaylists } = getState().media;
    let indexToChange = -1;
    userPlayListsCopy = userPlaylists.map((item, index) => {
      if (item.id === playlist.id) {
        indexToChange = index;
        return {
          ...item,
          downloaded: !item.downloaded
        };
      }
      return item;
    });
    updatedPlaylist = userPlayListsCopy[indexToChange];
    dispatch({
      type: Types.TOGGLE_DOWNLOAD_PLAYLIST,
      userPlayListsCopy,
      updatedPlaylist
    });
    // reactotron.log("This is our All Playlist user after update ", newUserPlaylists);
  };
};

/**
 * Prepare Playlist to download
 */
export const prepareDownload = () => {
  return (dispatch, getState) => {
    const { playlistToDisplay, userPlaylists } = getState().media;
    let playlistCopy = { ...playlistToDisplay };
    let userPlayListsCopy = [];
    if (playlistToDisplay.downloaded) {
      if (playlistToDisplay.songs.length === 0) {
        return;
      } else {
        //If Songs's Array is not Empty;
        //Prepare Song's Array for download
        playlistToDisplay.songs.forEach(el => {
          let ArrayToDownload = prepareAudioArraysForDownload(el);
          reactotron.log("Array pushed to Donwloads :", ArrayToDownload);
          playlistCopy = Object.assign({}, playlistCopy, {
            downloadedSongs: [
              ...playlistCopy.downloadedSongs,
              ...ArrayToDownload
            ]
          });
        });
        let updatedPlaylist = { ...playlistCopy };
        userPlayListsCopy = userPlaylists.map(el => {
          if (el.id === updatedPlaylist.id) {
            return {
              ...updatedPlaylist
            };
          }
          return el;
        });
        dispatch({
          type: Types.PREPARE_AUDIO_FOR_DOWNLOAD,
          userPlayListsCopy,
          updatedPlaylist
        });
      }
    } else {
      let updatedPlaylist = Object.assign({}, playlistCopy, {
        downloadedSongs: [],
      })
      userPlayListsCopy = userPlaylists.map(playlist => {
        if(playlist.id === updatedPlaylist.id) {
          return {
            ...updatedPlaylist,
          }
        }
        return playlist
      })
      dispatch({
        type: Types.PREPARE_AUDIO_FOR_DOWNLOAD,
        userPlayListsCopy,
        updatedPlaylist
      });
    }
  };
};

/**
 * Download Playlist 
 */
export const downloadAudios = () => {
  return (dispatch, getState) => {
    const { playlistToDisplay, userPlayLists } = getState().media;
    let playlistCopy = {};
    let userPlayListsCopy = []
    // Here we have an array of the downloadedSongs we should start downloading;
    if (playlistToDisplay.downloaded && playlistToDisplay.downloadedSongs.length > 0) {
      reactotron.log("STARTING TO DOWNLOAD AUDIOS", playlistToDisplay);
        playlistToDisplay.downloadedSongs.forEach( element => {
          reactotron.log("we are downloading this element", element )
          RNFetchBlob.config({
            fileCache: true,
            appendExt: "mp3"
          })
            .fetch("GET", element.uri)
            .then((resp) => {
              // the temp file path
              //console.log("our Result from download", res);
              //reactotron.log("is it null", _.isEmpty(resp.path()));
             /*  playlistCopy = Object.assign({}, playlistToDisplay, {
                downloadedSongs: playlistToDisplay.map(el => {
                  if(el.uri === element.uri){
                    return {
                      ...el,
                      downloadUri: "file://"+resp.path()
                    }
                  }
                  return el;
                })
              }); */

              reactotron.log("PLAYLIST SHOULD BE UPDATED NOW", playlistCopy);
              reactotron.log("The file saved to ", resp.path());
            })
            .catch(error => {
              reactotron.log("Error Downloading Audio", error);
            });
        })
        //We should have an updated playlist containing download uri;
        //let updatedPlaylist = {...playlistCopy};
        reactotron.log("Download URI should be inside ", playlistCopy);
        /* userPlayListsCopy = userPlayLists.map(playlist => {
          if(playlist.id === updatedPlaylist.id){
            return {
              ...updatedPlaylist
            }
          }
          return playlist;
        });
        dispatch({type: Types.DOWNLOAD_AUDIOS_SUCCESS, userPlayListsCopy, updatedPlaylist }); */
    }
  };
};

/**
 * extract URI's audio from song Object and return array of object;
 */
const prepareAudioArraysForDownload = song => {
  let audioObject = song.audiofile;
  let audioArray = [];
  let audioNames = song.nameaudio;
  for (let key in audioObject) {
    let item = {
      uri: audioObject[key],
      title: audioNames[key],
      downloadUri: null
    };
    audioArray.push(item);
  }
  //reactotron.log("Return array", audioArray, "From Object", song);
  return audioArray;
};

/**
 * Fetch All Displayed audios
 */
export const fetchAllAudios = () => {
  return (dispatch, getState) => {
    dispatch({ type: Types.LOADING });
    axios({
      method: "GET",
      url: ApiService.getAllAudios
    })
      .then(result => {
        //console.log("USER AUDIOS RESULT",JSON.stringify(result))
        let audios = result.data;
        dispatch({ type: Types.FETCH_ALL_AUDIOS_SUCCESS, audios });
      })
      .catch(error => {
        console.log("Error User Audios :", error);
        dispatch({ type: Types.FETCH_ALL_AUDIOS_FAILURE, error });
      });
  };
};

/**
 * Loading song to the player
 * @param {*} song
 */
export const startPlaying = song => {
  return dispatch => {
    let audioObject = song.audiofile;
    let audioArray = [];
    let audioNames = song.nameaudio;

    let songPlaying = {};
    let index = 0;
    let nbrParts = song.nbrparts;
    for (let key in audioObject) {
      let item = {
        uri: audioObject[key],
        title: audioNames[key],
        downloadUri: null
      };
      audioArray.push(item);
    }
    songPlaying = audioArray[0];
    dispatch({ type: Types.SET_SONG_AUDIOS, audioArray, nbrParts, index });
    dispatch({ type: Types.PLAY_SONG, song, songPlaying });
  };
};

/**
 * Loading song to the player
 * @param {*} song
 */
export const startPlayingDownload = song => {
  return dispatch => {
    let audioArray = song.downloadedAudios;
    dispatch({ type: Types.SET_SONG_AUDIOS_FROM_DOWNLOAD, audioArray });
    dispatch({ type: Types.PLAY_SONG_FROM_DOWNLOAD, song });
  };
};

/**
 * Pause Playing the current song
 */
export const pausePlaying = () => {
  return dispatch => {
    dispatch({ type: Types.PAUSE_SONG });
  };
};

/**
 * Resume Playing the current song
 */
export const resumePlaying = () => {
  return dispatch => {
    dispatch({ type: Types.RESUME_SONG });
  };
};

/**
 * Stop Playing the current song
 */
export const stopPlaying = () => {
  return dispatch => {
    dispatch({ type: Types.STOP_SONG });
  };
};

/**
 * Pause Playing the current song DOWNLOADED PLAYER
 */
export const pausePlayingDownload = () => {
  return dispatch => {
    dispatch({ type: Types.PAUSE_SONG_FROM_DOWNLOAD });
  };
};

/**
 * Resume Playing the current song DOWNLOADED PLAYER
 */
export const resumePlayingDownload = () => {
  return dispatch => {
    dispatch({ type: Types.RESUME_SONG_FROM_DOWNLOAD });
  };
};

/**
 * Stop Playing the current song DOWNLOADED PLAYER
 */
export const stopPlayingDownload = () => {
  return dispatch => {
    dispatch({ type: Types.STOP_SONG_FROM_DOWNLOAD });
  };
};

/**
 * Fetch user bought audios
 */
export const getUserAudios = () => {
  return (dispatch, getState) => {
    const { token } = getState().auth;

    axios({
      method: "GET",
      url: ApiService.getUserAudios,
      headers: {
        Authorization: "Bearer " + token
      }
    })
      .then(response => {
        let userAudios = response.data;
        //console.log("response from fetch user audios", response);
        dispatch({ type: Types.GET_USER_AUDIO_SUCCESS, userAudios });
      })
      .catch(error => {
        console.log("response from fetch user audios", error);
        dispatch({ type: Types.GET_USER_AUDIO_FAILURE, error });
      });
  };
};

/**
 * SHOW MINI PLAYER
 */
export const showMiniPlayer = () => {
  return dispatch => {
    dispatch({ type: Types.SHOW_MINI_PLAYER });
  };
};

/**
 * HIDE MINI PLAYER
 */
export const hideMiniPlayer = () => {
  return dispatch => {
    dispatch({ type: Types.HIDE_MINI_PLAYER });
  };
};
/**
 * SHOW MINI PLAYER DOWNLOAD
 */
export const showMiniPlayerDownload = () => {
  return dispatch => {
    dispatch({ type: Types.SHOW_MINI_PLAYER_DOWNLOAD });
  };
};

/**
 * HIDE MINI PLAYER DOWNLOAD
 */
export const hideMiniPlayerDownload = () => {
  return dispatch => {
    dispatch({ type: Types.HIDE_MINI_PLAYER });
  };
};

/**
 * SET THE TITLE OF THE AUDIO PLAYING
 * @param {title} of the audio
 */
export const setAudioTitle = audio => {
  return dispatch => {
    dispatch({ type: Types.SET_AUDIO_TITLE, audio });
  };
};

/**
 * Play song From Download
 */
export const playSongFromDownload = val => {
  if (val) {
  } else {
  }
};

export const audioIsReady = () => {
  return dispatch => {
    dispatch({ type: Types.AUDIOMODE_SET_SUCCESS });
  };
};

export const audioIsNotReady = () => {
  return dispatch => {
    dispatch({ type: Types.AUDIOMODE_SET_FAILURE });
  };
};
