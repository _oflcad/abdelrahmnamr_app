/**
 * The initial values for the Media redux state.
 */
export const INITIAL_STATE = {
  userPlaylists: [],
  playlistToDisplay: null,
  isAddPlaylistModalVisible: false,
  isAddPlaylistInfoModalVisible: false,
  isAddSongsToPlaylistModalVisible: false,
  isPlaying: false,
  isPlayingDownload: false,
  isAutoPlay: false,
  selectedSongs: [],
  userAudios: [],
  latestAudios: [],
  muted: false,
  volume: 1.0,
  songToPlay: {
    image: require('../../Assets/Images/first.jpg')
  },
  isPlayerShowen: false,
  isPlayerShowenDownload: false,
  audiosToPlay: [],
  audioNamesArray: [],
  indexToPlay: 0,
  nbrParts: 0,
  songPlaying : null,
  isFullPlayerShow: false,
  audio: null,
  audioModeIsSet: false,
};
