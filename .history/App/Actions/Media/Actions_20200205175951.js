import * as Types from "./Types";
import axios from "axios";
import ApiService from "../../Services/ApiService";
import _ from "lodash";
import RNFetchBlob from "rn-fetch-blob";
import reactotron from "reactotron-react-native";
export const setPlaylist = playlist => {
  return dispatch => {
    dispatch({ type: Types.SET_PLAYLIST, playlist });
  };
};

export const openPlaylistModal = () => {
  return dispatch => {
    dispatch({ type: Types.OPEN_PLAYLIST_MODAL_VISIBLE });
  };
};

export const closePlaylistModal = () => {
  return dispatch => {
    dispatch({ type: Types.CLOSE_PLAYLIST_MODAL_VISIBLE });
  };
};
export const openPlaylistInfoModal = () => {
  return dispatch => {
    dispatch({ type: Types.OPEN_PLAYLISTINFO_MODAL_VISIBLE });
  };
};

export const closePlaylistInfoModal = () => {
  return dispatch => {
    dispatch({ type: Types.CLOSE_PLAYLISTINFO_MODAL_VISIBLE });
  };
};

export const openAddSongsModal = () => {
  return dispatch => {
    dispatch({ type: Types.OPEN_ADD_SONGS_MODAL_VISIBLE });
  };
};

export const closeAddSongsModal = () => {
  return dispatch => {
    dispatch({ type: Types.CLOSE_ADD_SONGS_MODAL_VISIBLE });
  };
};

export const changePlaylistTitle = title => {
  return dispatch => {
    dispatch({ type: Types.CHANGE_PLAYLIST_TITLE, title });
  };
};

export const createPlaylist = playlist => {
  return (dispatch, getState) => {
    const userPlaylistCopy = getState().media.userPlaylists.slice();
    userPlaylistCopy.push(playlist)
    dispatch({ type: Types.CREATE_PLAYLIST, userPlaylistCopy });
  };
};

export const deletePlaylist = playlist => {
  return dispatch => {
    dispatch({ type: Types.DELETE_PLAYLIST, playlist });
  };
};

export const addSongToPlaylist = song => {
  return dispatch => {
    dispatch({ type: Types.ADD_SONG_TO_PLAYLIST, song });
  };
};

export const toggleFullSizePlayer = () => {
  return dispatch => {
    dispatch({ type: Types.TOGGLE_FULL_SIZE });
  };
};

/**
 * Remove Song from Playlist
 * @param {song} song
 */
export const removeSongFromPlaylist = song => {
  return dispatch => {
    dispatch({ type: Types.REMOVE_SONG_FROM_PLAYLIST, song });
  };
};

/**
 * Handle Auto Play
 */
export const handleAutoPlay = () => {
  return dispatch => {
    dispatch({ type: Types.HANDLE_AUTOPLAY });
  };
};

/**
 * Set Playlist to download
 */
export const downloadPlaylist = playlist => {
  return (dispatch, getState) => {
    let userPlayListsCopy = [];
    let updatedPlaylist = {};
    const { userPlaylists } = getState().media;
    let indexToChange = -1
    userPlayListsCopy = userPlaylists.map((item,index) => {
         if(item.id === playlist.id) {
           indexToChange = index;
           return {
             ...item,
             downloaded: !item.downloaded,
           }
         }
         return item;
       });
     updatedPlaylist = userPlayListsCopy[indexToChange];
     reactotron.log('updated playlist', updatedPlaylist);
     if(updatedPlaylist.downloaded) {
      let playlistCopy  = { ...updatedPlaylist};
    //If Songs's Array is empty Dispatch The same playlist ie Do nothing;
    if(playlist.songs.length === 0) {
      return;
    } else {
    //If Songs's Array is not Empty;
    //Prepare Song's Array for download
    playlist.songs.forEach(el => {
      let ArrayToDownload  =  prepareAudioArraysForDownload(el);
      reactotron.log('Array pushed to Donwloads :' , ArrayToDownload);
      playlistCopy = Object.assign({}, playlist, {
        downloadedSongs: [ ...playlistCopy.downloadedSongs , ...ArrayToDownload],
      });
    });
    let updatedPlaylist = {...playlistCopy};
    userPlaylists[indexToChange] = updatedPlaylist;
    dispatch({
      type: Types.TOGGLE_DOWNLOAD_PLAYLIST,
      userPlayListsCopy,
      updatedPlaylist
    });
    reactotron.log("This should be our new Playlist", playlistCopy);
      //dispatch({type: Types.AUDIO_PREPARED_FOR_DOWNLOAD, playlistCopy});
    // If downloadedSongs Array is Empty then  Start Downloading
    }
    }else {
      dispatch({
        type: Types.TOGGLE_DOWNLOAD_PLAYLIST,
        userPlayListsCopy,
        updatedPlaylist
      });
    }
     

    
   // reactotron.log("This is our All Playlist user after update ", newUserPlaylists);
    /* if (downloadedPlaylist.downloaded) {
      downloadedPlaylist.songs.forEach(el => {
        let audioObject = el.audiofile;
        let audioArray = [];
        let audioNames = el.nameaudio;
        for (let key in audioObject) {
          let item = {
            uri: audioObject[key],
            title: audioNames[key],
            downloadUri: null
          };
          audioArray.push(item);
        }

        if (audioArray.length > 0)
          console.log("Audios Array Length", audioArray.length);
        let downloadedAudioArray = [];
        audioArray.forEach((element, index) => {
          let path = 
          RNFetchBlob.config({
            fileCache: true,
            appendExt: "mp3"
          })
            .fetch("GET", element.uri)
            .then((resp) => {
              // the temp file path
              //console.log("our Result from download", res);
              reactotron.log("is it null", _.isEmpty(resp.path()));
              if (resp.path() && !_.isEmpty(resp.path())) {
                element.downloadUri = "file://" + resp.path();
              }

              console.log("new ELEMENT", element);
              console.log("The file saved to ", resp.path());
              // DISPATCH SET
            })
            .catch(error => {
              console.log("Error Downloading Audio", error);
            });
        });
        downloadedPlaylist.downloadedAudios = audioArray;
        var newUserPlaylists = getState().media.userPlaylists;
        newUserPlaylists[indexToChange] = downloadedPlaylist;
      });
      dispatch({
        type: Types.TOGGLE_DOWNLOAD_PLAYLIST,
        newUserPlaylists,
        downloadedPlaylist
      });
    } else {
      var newUserPlaylists = getState().media.userPlaylists;
      newUserPlaylists[indexToChange] = downloadedPlaylist;
    } */
  };
};

/**
 * extract URI's audio from song Object and return array of object;
 */
const prepareAudioArraysForDownload = (song) => {
  let audioObject = song.audiofile;
    let audioArray = [];
    let audioNames = song.nameaudio;
    for (let key in audioObject) {
      let item = {
        uri: audioObject[key],
        title: audioNames[key],
        downloadUri: null
      };
      audioArray.push(item);
    }
    //reactotron.log("Return array", audioArray, "From Object", song);
    return audioArray;
}

/**
 * Fetch All Displayed audios
 */
export const fetchAllAudios = () => {
  return (dispatch, getState) => {
    dispatch({ type: Types.LOADING });
    axios({
      method: "GET",
      url: ApiService.getAllAudios
    })
      .then(result => {
        //console.log("USER AUDIOS RESULT",JSON.stringify(result))
        let audios = result.data;
        dispatch({ type: Types.FETCH_ALL_AUDIOS_SUCCESS, audios });
      })
      .catch(error => {
        console.log("Error User Audios :", error);
        dispatch({ type: Types.FETCH_ALL_AUDIOS_FAILURE, error });
      });
  };
};

/**
 * Loading song to the player
 * @param {*} song
 */
export const startPlaying = song => {
  return dispatch => {
    let audioObject = song.audiofile;
    let audioArray = [];
    let audioNames = song.nameaudio;

    let songPlaying = {};
    let index = 0;
    let nbrParts = song.nbrparts;
    for (let key in audioObject) {
      let item = {
        uri: audioObject[key],
        title: audioNames[key],
        downloadUri: null
      };
      audioArray.push(item);
    }
    songPlaying = audioArray[0];
    dispatch({ type: Types.SET_SONG_AUDIOS, audioArray, nbrParts, index });
    dispatch({ type: Types.PLAY_SONG, song, songPlaying });
  };
};

/**
 * Loading song to the player
 * @param {*} song
 */
export const startPlayingDownload = song => {
  return dispatch => {
    let audioArray = song.downloadedAudios;
    dispatch({ type: Types.SET_SONG_AUDIOS_FROM_DOWNLOAD, audioArray });
    dispatch({ type: Types.PLAY_SONG_FROM_DOWNLOAD, song });
  };
};

/**
 * Pause Playing the current song
 */
export const pausePlaying = () => {
  return dispatch => {
    dispatch({ type: Types.PAUSE_SONG });
  };
};

/**
 * Resume Playing the current song
 */
export const resumePlaying = () => {
  return dispatch => {
    dispatch({ type: Types.RESUME_SONG });
  };
};

/**
 * Stop Playing the current song
 */
export const stopPlaying = () => {
  return dispatch => {
    dispatch({ type: Types.STOP_SONG });
  };
};

/**
 * Pause Playing the current song DOWNLOADED PLAYER
 */
export const pausePlayingDownload = () => {
  return dispatch => {
    dispatch({ type: Types.PAUSE_SONG_FROM_DOWNLOAD });
  };
};

/**
 * Resume Playing the current song DOWNLOADED PLAYER
 */
export const resumePlayingDownload = () => {
  return dispatch => {
    dispatch({ type: Types.RESUME_SONG_FROM_DOWNLOAD });
  };
};

/**
 * Stop Playing the current song DOWNLOADED PLAYER
 */
export const stopPlayingDownload = () => {
  return dispatch => {
    dispatch({ type: Types.STOP_SONG_FROM_DOWNLOAD });
  };
};

/**
 * Fetch user bought audios
 */
export const getUserAudios = () => {
  return (dispatch, getState) => {
    const { token } = getState().auth;

    axios({
      method: "GET",
      url: ApiService.getUserAudios,
      headers: {
        Authorization: "Bearer " + token
      }
    })
      .then(response => {
        let userAudios = response.data;
        //console.log("response from fetch user audios", response);
        dispatch({ type: Types.GET_USER_AUDIO_SUCCESS, userAudios });
      })
      .catch(error => {
        console.log("response from fetch user audios", error);
        dispatch({ type: Types.GET_USER_AUDIO_FAILURE, error });
      });
  };
};

/**
 * SHOW MINI PLAYER
 */
export const showMiniPlayer = () => {
  return dispatch => {
    dispatch({ type: Types.SHOW_MINI_PLAYER });
  };
};

/**
 * HIDE MINI PLAYER
 */
export const hideMiniPlayer = () => {
  return dispatch => {
    dispatch({ type: Types.HIDE_MINI_PLAYER });
  };
};
/**
 * SHOW MINI PLAYER DOWNLOAD
 */
export const showMiniPlayerDownload = () => {
  return dispatch => {
    dispatch({ type: Types.SHOW_MINI_PLAYER_DOWNLOAD });
  };
};

/**
 * HIDE MINI PLAYER DOWNLOAD
 */
export const hideMiniPlayerDownload = () => {
  return dispatch => {
    dispatch({ type: Types.HIDE_MINI_PLAYER });
  };
};

/**
 * SET THE TITLE OF THE AUDIO PLAYING
 * @param {title} of the audio
 */
export const setAudioTitle = audio => {
  return dispatch => {
    dispatch({ type: Types.SET_AUDIO_TITLE, audio });
  };
};

/**
 * Play song From Download
 */
export const playSongFromDownload = val => {
  if (val) {
  } else {
  }
};


export const audioIsReady = () => {
  return dispatch => {
    dispatch({type: Types.AUDIOMODE_SET_SUCCESS })
  }
}

export const audioIsNotReady = () => {
  return dispatch => {
    dispatch({type: Types.AUDIOMODE_SET_FAILURE })
  }
}