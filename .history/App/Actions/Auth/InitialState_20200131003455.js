/**
 * The initial values for the Application Container redux state.
 */
export const INITIAL_STATE = {
  isAuthenticated : false,
  authError: null,
  token: null,
  isLoading: false,
  confirmEmail: false,
  user: null,
}
