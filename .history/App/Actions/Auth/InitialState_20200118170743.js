/**
 * The initial values for the Application Container redux state.
 */
export const INITIAL_STATE = {
  isAuthenticated : false,
  token: null,
  isLoading: false,
  confirmEmail: false,
  user: null,
}
