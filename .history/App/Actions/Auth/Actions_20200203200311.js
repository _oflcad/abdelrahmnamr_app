import * as Types from "./Types";
import * as MediaTypes from "../Media/Types";
import axios from "axios";
import ApiService from "../../Services/ApiService";
import reactotron from "reactotron-react-native";

export const setFirstTimer = language => {
  return dispatch => {
    dispatch({ type: Types.FIRST_TIMER, language });
  };
};

export const setDeviceLanguage = language => {
  return dispatch => {
    dispatch({ type: Types.SET_DEVICE_LANGUAGE, language });
  };
};

export const authenticateSucess = (token) => {
  return dispatch => {
    dispatch({type: Types.AUTHENTICATE_USER_SUCCESS, token});
  }
}

export const authenticateUser = (email, password) => {
  return dispatch => {
    dispatch({ type: Types.LOADING });
    console.log("username", email);
    console.log("password", password);
    var formdata = new FormData();
    formdata.append("_username", email);
    formdata.append("_password", password);

    axios({
      method: "POST",
      url: ApiService.loginUrl,
      headers: {
        Accept: "application/json",
        "Content-Type": "multipart/form-data; charset=utf-8;",
        _username: email,
        _password: password
      },
      data: formdata,
      withCredentials: true
    })
      .then(response => {
        if(response.status === 200) {
          
        
        console.log("response status : ", response.status);
        let { token } = response.data;
        dispatch({ type: Types.AUTHENTICATE_USER_SUCCESS, token });
        axios({
          method: "GET",
          url: ApiService.getUserInformation,
          headers: {
            Authorization: "Bearer " + token
          }
        })
          .then(response => {
            let user = response.data;
            reactotron.log("FETCH USER INFO response", user);
            dispatch({ type: Types.GET_USER_SUCCESS, user });
          })
          .catch(error => {
            reactotron.log("FETCH USER INFO error", error);
            dispatch({ type: Types.GET_USER_FAILURE, error });
          });
        } else {
          let errorMsg = "Request failed with status code 401";
          dispatch({ type: Types.AUTHENTICATE_USER_FAILURE, errorMsg });  
        }
      })
      .catch(error => {
        let errorMsg = "Request failed with status code 401";
        dispatch({ type: Types.AUTHENTICATE_USER_FAILURE, errorMsg });
        console.log("error from user auth", error.message);
      });
  };
};

export const authenticateFailure = () => {
  return dispatch => {
    dispatch({type: Types.AUTHENTICATE_USER_FAILURE });
  }
}

export const getUserInformation = tokens => {
  return (dispatch, getState) => {
    dispatch({ type: Types.LOADING });
    const { token } = getState().auth;
    reactotron.log("TOKEN", token);
  };
};

export const registerUser = user => {
  return dispatch => {
    dispatch({ type: Types.LOADING });
    const {
      name,
      lastname,
      username,
      email,
      password,
      confirmpassword,
      country,
      phone
    } = user;
    var formdata = new FormData();
    formdata.append("name", name);
    formdata.append("username", username);
    formdata.append("email", email);
    formdata.append("lastname", lastname);
    formdata.append("password", password);
    formdata.append("confirmpassword", confirmpassword);
    formdata.append("country", country);
    formdata.append("phone", phone);

    axios({
      method: "POST",
      url: ApiService.register,
      data: formdata
    })
      .then(response => {
        console.log("REGISTER USER RESPONSE", response);
        dispatch({ type: Types.REGISTER_USER_SUCCESS, email });
      })
      .catch(error => {
        dispatch({ type: Types.REGISTER_USER_FAILURE });
      });
  };
};

export const logoutUser = () => {
  return dispatch => {
    dispatch({ type: Types.LOGOUT_USER });
    dispatch({ type: MediaTypes.LOGOUT_USER });
  };
};

export const clearAuthError = () => {
  return dispatch => {
    dispatch({type: Types.CLEAR_AUTHEROOR})
  }
}