import * as Types from "./Types";
import axios from "axios";
import ApiService from "../../Services/ApiService";
import NavigationService from "../../Services/NavigationService";
import reactotron from "reactotron-react-native";
import { FileSystem } from "react-native-unimodules";

export const setFirstTimer = language => {
  return dispatch => {
    dispatch({ type: Types.FIRST_TIMER, language });
  };
};

export const setDeviceLanguage = language => {
  return dispatch => {
    dispatch({ type: Types.SET_DEVICE_LANGUAGE, language });
  };
};

export const fetchLatestImages = () => {
  return dispatch => {
    // API CALL FOR LATEST 6 IMAGES
    dispatch({ type: Types.FETCH_LATEST_IMAGES });
  };
};

export const fetchLatestNews = () => {
  return dispatch => {
    dispatch({ type: Types.LOADINGS });
    try {
      axios
        .get(ApiService.newsUrl)
        .then(response => {
          dispatch({ type: Types.FETCH_LATEST_NEWS_SUCCESS });
        })
        .catch(error => {
          dispatch({ type: Types.FETCH_LATEST_NEWS_FAILURE });
        });
    } catch (error) {
      dispatch({ type: Types.FETCH_LATEST_NEWS_FAILURE });
    }
  };
};

export const fetchLatestSessions = () => {
  return dispatch => {
    dispatch({ type: Types.LOADINGS });
    try {
      axios
        .get(ApiService.newsUrl)
        .then(response => {
          dispatch({ type: Types.FETCH_LATEST_SESSIONS_SUCCESS });
        })
        .catch(error => {
          dispatch({ type: Types.FETCH_LATEST_SESSIONS_FAILURE });
        });
    } catch (error) {
      dispatch({ type: Types.FETCH_LATEST_SESSIONS_FAILURE });
    }
  };
};

export const fetchLatestBlog = () => {
  return dispatch => {
    dispatch({ type: Types.LOADINGS });
    try {
      axios
        .get(ApiService.newsUrl)
        .then(response => {
          dispatch({ type: Types.FETCH_LATEST_BLOG_SUCCESS, response });
        })
        .catch(error => {
          dispatch({ type: Types.FETCH_LATEST_BLOG_FAILURE, error });
        });
    } catch (error) {
      dispatch({ type: Types.FETCH_LATEST_BLOG_FAILURE });
    }
  };
};

export const setItem = item => {
  return dispatch => {
    dispatch({ type: Types.SET_ITEM, item });
  };
};

export const clearItem = () => {
  return dispatch => {
    dispatch({ type: Types.CLEAR_ITEM });
  };
};

export const setRouteName = routeName => {
  return dispatch => {
    dispatch({ type: Types.ROUTE_NAME, routeName });
  };
};

export const setItemToBasket = song => {
  return dispatch => {
    dispatch({ type: Types.SET_ITEM_TO_BASKET, song });
  };
};

export const removeItemToBasket = () => {
  return dispatch => {
    dispatch({ type: Types.REMOVE_ITEM_FROM_BASKET });
  };
};

export const removeItemfromBasket = item => {
  return dispatch => {
    dispatch({ type: Types.REMOVE_ITEM_TO_BASKET, item });
  };
};

export const addItemToBasket = item => {
  return dispatch => {
    dispatch({ type: Types.ADD_ITEM_TO_BASKET, item });
  };
};

export const navigateToPlayer = () => {
  return dispatch => {
    NavigationService.navigate("Player");
  };
};

export const calculateTotal = () => {
  return (dispatch, getState) => {
    const { basket } = getState().app;
    let newPrice = 0;
    if (basket.length > 0) {
      basket.forEach(el => {
        newPrice += el.price;
      });
      dispatch({ type: Types.CALCULATE_TOTAL, newPrice });
    } else {
      dispatch({ type: Types.CALCULATE_TOTAL, newPrice });
    }
  };
};

export const buyProduct = () => {
  return (dispatch, getState) => {
    var myHeaders = new Headers();
myHeaders.append("Authorization", "Bearer eyJhbGciOiJSUzI1NiJ9.eyJyb2xlcyI6WyJST0xFX1VTRVIiXSwidXNlcm5hbWUiOiJoYW1kb3VuIiwiaWF0IjoxNTc5NjE3MTA4LCJleHAiOjE1Nzk2MjA3MDh9.Uh3X9DIocvbdLqwLOU3vonkCvkcTMHF4o6fnJK0nVkIK0JfxYlMRYu2mndLpEDnaWkRLzmW3mL4Z7Oo2-I_XfxpeusKHNcXkYiR2Dnmr8wUmeyFRse-NwSC_f9_q0Y1IOvCyOiv1Wn0lSvc_3r0dHKjesOebD0QMSql49j0w3ODjfrTIz-9_a5pad5MBlsAFEo-VuUZ_6pb1-fVvDpgqM4Rhg1qsDUKOPCpy-dxguZYqR3CuVU7bjLDfx5x2qAOrzLfzUur1iaQg-GoMtTwIyJaNMWgk-8fNPUSWuW8Skae6gOgKfD0SPP8sDTDmp539iAsEngxxdSxoQleztwaj1lWCO64LN38pcIZMKS6-r-TBLBQq9LbMxv__4FbzxCS-AzcqcASvDIoOOo_QZQCtbpHx0mNHPrt1FEhagKaSJcomhsziBqFyiqMDOdM1gIOxbvWIPKx-7RHU9hKgmA14kJaSUKOamaUSlzfenQYcdZGMFNvrVmICyDwesT8AVc4yesSC754ViJczc6N6Fj4N-g99SPcj3al26Ca7AlMsas5MAd53ZNTaFVlWX_3Q-HwjqACgJ1ICjUahEjoPFvo_uMwUcRDtiaHnf6NmImZWcn37_xZwD3ShJz1vJtQRuZw99G4JJq6nFEn99j14Lklcb7iXwfr1AM1soT09xd6xXe4");
myHeaders.append("Content-Type", "multipart/form-data; boundary=--------------------------363621818632658501614475");

var formdata = new FormData();
formdata.append("name", "hmayda");
formdata.append("email", "oflcad@gmail.com");
formdata.append("phone", "58141956");
formdata.append("comment", "");
formdata.append("cart", "6,4");

var requestOptions = {
  method: 'POST',
  headers: myHeaders,
  body: formdata,
  redirect: 'follow'
};

fetch("https://www.abdelrahman-amr.com/web/app_dev.php/api/user/post/order", requestOptions)
  .then(response => response.text())
  .then(result => console.log(result))
  .catch(error => console.log('error', error));
  };
};

export const setImagePath = path => {
  return dispatch => {
    dispatch({ type: Types.SET_IMAGE_URI, path });
  };
};
