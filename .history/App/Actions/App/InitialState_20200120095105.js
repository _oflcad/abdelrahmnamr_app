/**
 * The initial values for the Application Container redux state.
 */
export const INITIAL_STATE = {
  isFirstTimer: true,
  language: 'en',
  latestImages: null,
  isLoading: false,
  notifications: [],
  news: [],
  sessions: [],
  blog: [],
  item: null,
  routeName: '',
  isItemModalOpen: false,
  itemToBasket: null,
  basket: [],
  total: 0,
  imageUri: null,
}
