import * as Types from "./Types";
import axios from "axios";
import ApiService from "../../Services/ApiService";
import NavigationService from "../../Services/NavigationService";
import reactotron from "reactotron-react-native";
import { FileSystem } from "react-native-unimodules";
var fs = require('fs');
export const setFirstTimer = language => {
  return dispatch => {
    dispatch({ type: Types.FIRST_TIMER, language });
  };
};

export const setDeviceLanguage = language => {
  return dispatch => {
    dispatch({ type: Types.SET_DEVICE_LANGUAGE, language });
  };
};

export const fetchLatestImages = () => {
  return dispatch => {
    // API CALL FOR LATEST 6 IMAGES
    dispatch({ type: Types.FETCH_LATEST_IMAGES });
  };
};

export const fetchLatestNews = () => {
  return dispatch => {
    dispatch({ type: Types.LOADINGS });
    try {
      axios
        .get(ApiService.newsUrl)
        .then(response => {
          dispatch({ type: Types.FETCH_LATEST_NEWS_SUCCESS });
        })
        .catch(error => {
          dispatch({ type: Types.FETCH_LATEST_NEWS_FAILURE });
        });
    } catch (error) {
      dispatch({ type: Types.FETCH_LATEST_NEWS_FAILURE });
    }
  };
};

export const fetchLatestSessions = () => {
  return dispatch => {
    dispatch({ type: Types.LOADINGS });
    try {
      axios
        .get(ApiService.newsUrl)
        .then(response => {
          dispatch({ type: Types.FETCH_LATEST_SESSIONS_SUCCESS });
        })
        .catch(error => {
          dispatch({ type: Types.FETCH_LATEST_SESSIONS_FAILURE });
        });
    } catch (error) {
      dispatch({ type: Types.FETCH_LATEST_SESSIONS_FAILURE });
    }
  };
};

export const fetchLatestBlog = () => {
  return dispatch => {
    dispatch({ type: Types.LOADINGS });
    try {
      axios
        .get(ApiService.newsUrl)
        .then(response => {
          dispatch({ type: Types.FETCH_LATEST_BLOG_SUCCESS, response });
        })
        .catch(error => {
          dispatch({ type: Types.FETCH_LATEST_BLOG_FAILURE, error });
        });
    } catch (error) {
      dispatch({ type: Types.FETCH_LATEST_BLOG_FAILURE });
    }
  };
};

export const setItem = item => {
  return dispatch => {
    dispatch({ type: Types.SET_ITEM, item });
  };
};

export const clearItem = () => {
  return dispatch => {
    dispatch({ type: Types.CLEAR_ITEM });
  };
};

export const setRouteName = routeName => {
  return dispatch => {
    dispatch({ type: Types.ROUTE_NAME, routeName });
  };
};

export const setItemToBasket = song => {
  return dispatch => {
    dispatch({ type: Types.SET_ITEM_TO_BASKET, song });
  };
};

export const removeItemToBasket = () => {
  return dispatch => {
    dispatch({ type: Types.REMOVE_ITEM_FROM_BASKET });
  };
};

export const removeItemfromBasket = item => {
  return dispatch => {
    dispatch({ type: Types.REMOVE_ITEM_TO_BASKET, item });
  };
};

export const addItemToBasket = item => {
  return dispatch => {
    dispatch({ type: Types.ADD_ITEM_TO_BASKET, item });
  };
};

export const navigateToPlayer = () => {
  return dispatch => {
    NavigationService.navigate("Player");
  };
};

export const calculateTotal = () => {
  return (dispatch, getState) => {
    const { basket } = getState().app;
    let newPrice = 0;
    if (basket.length > 0) {
      basket.forEach(el => {
        newPrice += el.price;
      });
      dispatch({ type: Types.CALCULATE_TOTAL, newPrice });
    } else {
      dispatch({ type: Types.CALCULATE_TOTAL, newPrice });
    }
  };
};

export const buyProduct = () => {
  return (dispatch, getState) => {
    const { basket, imageUri } = getState().app;
    const { user, token } = getState().auth;
    var formdata = new FormData();
    let cart1 = {
      prod1: ""
    };
    let cart2 = {
      prod1: "",
      prod2: ""
    };
    let cart3 = {
      prod1: "",
      prod2: "",
      prod3: ""
    };
    if (basket.length === 1) {
      cart1.prod1 = `${basket[0].id}`;
      formdata.append("cart", cart1);
    } else if (basket.length === 2) {
      cart2.prod1 = `${basket[0].id}`;
      cart2.prod2 = `${basket[1].id}`;
      formdata.append("cart", cart2);
    } else if (basket.length === 3) {
      cart3.prod1 = `${basket[0].id}`;
      cart3.prod2 = `${basket[1].id}`;
      cart3.prod3 = `${basket[2].id}`;
      formdata.append("cart", cart3);
    }
    var newFile = FileSystem.createReadStream(file.path);

    formdata.append("name", user.firstname);
    formdata.append("email", "koalastkrs@gmail.com");
    formdata.append("phone", user.phone);
    formdata.append("comment", "");

    formdata.append("proofpath", {
      uri: imageUri,
      type: 'image/jpeg',
    });
    fetch(ApiService.buyProducts,{
      method: "POST",
      headers: {
        'Authorization': "Bearer " + token,
        'Content-Type': 'multipart/form-data;' 
      },
      body: formdata
    })
    .then((response) => response.json())
    .then(responseJson => {
      reactotron.log("BUY PRODUCT SUCCESS", response);
      dispatch({ type: Types.BUY_PRODUCT_SUCCESS });
    })
    .catch(error => {
      console.log("ERROR BUY PRODUCT", error)
        reactotron.log("BUY PRODUCT FAILURE", error);
        dispatch({ type: Types.BUY_PRODUCT_FAILURE });
      });
  };
};

export const setImagePath = path => {
  return dispatch => {
    dispatch({ type: Types.SET_IMAGE_URI, path });
  };
};
