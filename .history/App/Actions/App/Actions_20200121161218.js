import * as Types from "./Types";
import axios from "axios";
import ApiService from "../../Services/ApiService";
import NavigationService from "../../Services/NavigationService";
import reactotron from "reactotron-react-native";
import { FileSystem } from "react-native-unimodules";

export const setFirstTimer = language => {
  return dispatch => {
    dispatch({ type: Types.FIRST_TIMER, language });
  };
};

export const setDeviceLanguage = language => {
  return dispatch => {
    dispatch({ type: Types.SET_DEVICE_LANGUAGE, language });
  };
};

export const fetchLatestImages = () => {
  return dispatch => {
    // API CALL FOR LATEST 6 IMAGES
    dispatch({ type: Types.FETCH_LATEST_IMAGES });
  };
};

export const fetchLatestNews = () => {
  return dispatch => {
    dispatch({ type: Types.LOADINGS });
    try {
      axios
        .get(ApiService.newsUrl)
        .then(response => {
          dispatch({ type: Types.FETCH_LATEST_NEWS_SUCCESS });
        })
        .catch(error => {
          dispatch({ type: Types.FETCH_LATEST_NEWS_FAILURE });
        });
    } catch (error) {
      dispatch({ type: Types.FETCH_LATEST_NEWS_FAILURE });
    }
  };
};

export const fetchLatestSessions = () => {
  return dispatch => {
    dispatch({ type: Types.LOADINGS });
    try {
      axios
        .get(ApiService.newsUrl)
        .then(response => {
          dispatch({ type: Types.FETCH_LATEST_SESSIONS_SUCCESS });
        })
        .catch(error => {
          dispatch({ type: Types.FETCH_LATEST_SESSIONS_FAILURE });
        });
    } catch (error) {
      dispatch({ type: Types.FETCH_LATEST_SESSIONS_FAILURE });
    }
  };
};

export const fetchLatestBlog = () => {
  return dispatch => {
    dispatch({ type: Types.LOADINGS });
    try {
      axios
        .get(ApiService.newsUrl)
        .then(response => {
          dispatch({ type: Types.FETCH_LATEST_BLOG_SUCCESS, response });
        })
        .catch(error => {
          dispatch({ type: Types.FETCH_LATEST_BLOG_FAILURE, error });
        });
    } catch (error) {
      dispatch({ type: Types.FETCH_LATEST_BLOG_FAILURE });
    }
  };
};

export const setItem = item => {
  return dispatch => {
    dispatch({ type: Types.SET_ITEM, item });
  };
};

export const clearItem = () => {
  return dispatch => {
    dispatch({ type: Types.CLEAR_ITEM });
  };
};

export const setRouteName = routeName => {
  return dispatch => {
    dispatch({ type: Types.ROUTE_NAME, routeName });
  };
};

export const setItemToBasket = song => {
  return dispatch => {
    dispatch({ type: Types.SET_ITEM_TO_BASKET, song });
  };
};

export const removeItemToBasket = () => {
  return dispatch => {
    dispatch({ type: Types.REMOVE_ITEM_FROM_BASKET });
  };
};

export const removeItemfromBasket = item => {
  return dispatch => {
    dispatch({ type: Types.REMOVE_ITEM_TO_BASKET, item });
  };
};

export const addItemToBasket = item => {
  return dispatch => {
    dispatch({ type: Types.ADD_ITEM_TO_BASKET, item });
  };
};

export const navigateToPlayer = () => {
  return dispatch => {
    NavigationService.navigate("Player");
  };
};

export const calculateTotal = () => {
  return (dispatch, getState) => {
    const { basket } = getState().app;
    let newPrice = 0;
    if (basket.length > 0) {
      basket.forEach(el => {
        newPrice += el.price;
      });
      dispatch({ type: Types.CALCULATE_TOTAL, newPrice });
    } else {
      dispatch({ type: Types.CALCULATE_TOTAL, newPrice });
    }
  };
};

export const buyProduct = () => {
  return (dispatch, getState) => {
    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer eeyJhbGciOiJSUzI1NiJ9.eyJyb2xlcyI6WyJST0xFX1VTRVIiXSwidXNlcm5hbWUiOiJoYW1kb3VuIiwiaWF0IjoxNTc5NjE5NDMxLCJleHAiOjE1Nzk2MjMwMzF9.Ds1cBHsuKDLw_JzSUOW7oR8gyGvFncKBC7jSlCGBN6Dsm_i5bPOHuDVVNJCKf0D6hZ4RfVEizVVEamaCPKTqLTPB6PVT7jZ1TGJ4Qsg9XQ-LQFPrTqSFrQpcF6B91RLv2Ojy90xZ7uBGJ7nddYCNEaqiwi1akZM0JtOpwizmWtSqNekDW0MEV2Tn7sMShVMTjgNJ28cVzLYUaKiVhnM503PiIVkyP_eUtdn2dUvMPMNt9_2Ci0-5hxZcxHINc5F-yASPKctQpMCk2ZGRzC0w5dXPfeGUZcEQNlza-dLhxAdHjTi10FIWDrVeYBnMmHcGiJhVKt7DJUKFam-dcCa4qcmW4ObAaqPdZUt8NJ4rsHyQCieDbcC5ji9oAJmvHSRmlbs5MPu5kuWmucplhtLSAbnqvxKsEShwEXNPBam5YbKqqS76DlC4t4DXsxuszYsc391qVSoQ61XmtX-YAYTciwKy58Ju0uwCICQXryrRNt2YtQwDHrRAIs4VANDXKbjbjFknA7JYP5URFxTHZKD3vI51KZ1gZlkXvYp_iVOdFBbKWZ22zDQ8_i2sS8AEKnxTr5kQCa7CI32fALDwxaA-a38F07tcTTCNhCkJ2mMdCVNrWOXikRbg5pwLpjq5YoJVKbTSIqRG6FTZzhFIKAiQEwXxNjS0lzg-gf6duESK94c"
    );
    myHeaders.append(
      "Content-Type",
      "multipart/form-data; boundary=--------------------------363621818632658501614475"
    );

    var formdata = new FormData();
    formdata.append("name", "hmayda");
    formdata.append("email", "oflcad@gmail.com");
    formdata.append("phone", "58141956");
    formdata.append("comment", "");
    formdata.append("cart", "6,4");

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formdata,
      redirect: "follow"
    };

    axios({
      method: "POST",
      url: ApiService.buyProducts,
      headers: {
        Authorization: "Bearer eeyJhbGciOiJSUzI1NiJ9.eyJyb2xlcyI6WyJST0xFX1VTRVIiXSwidXNlcm5hbWUiOiJoYW1kb3VuIiwiaWF0IjoxNTc5NjE5NDMxLCJleHAiOjE1Nzk2MjMwMzF9.Ds1cBHsuKDLw_JzSUOW7oR8gyGvFncKBC7jSlCGBN6Dsm_i5bPOHuDVVNJCKf0D6hZ4RfVEizVVEamaCPKTqLTPB6PVT7jZ1TGJ4Qsg9XQ-LQFPrTqSFrQpcF6B91RLv2Ojy90xZ7uBGJ7nddYCNEaqiwi1akZM0JtOpwizmWtSqNekDW0MEV2Tn7sMShVMTjgNJ28cVzLYUaKiVhnM503PiIVkyP_eUtdn2dUvMPMNt9_2Ci0-5hxZcxHINc5F-yASPKctQpMCk2ZGRzC0w5dXPfeGUZcEQNlza-dLhxAdHjTi10FIWDrVeYBnMmHcGiJhVKt7DJUKFam-dcCa4qcmW4ObAaqPdZUt8NJ4rsHyQCieDbcC5ji9oAJmvHSRmlbs5MPu5kuWmucplhtLSAbnqvxKsEShwEXNPBam5YbKqqS76DlC4t4DXsxuszYsc391qVSoQ61XmtX-YAYTciwKy58Ju0uwCICQXryrRNt2YtQwDHrRAIs4VANDXKbjbjFknA7JYP5URFxTHZKD3vI51KZ1gZlkXvYp_iVOdFBbKWZ22zDQ8_i2sS8AEKnxTr5kQCa7CI32fALDwxaA-a38F07tcTTCNhCkJ2mMdCVNrWOXikRbg5pwLpjq5YoJVKbTSIqRG6FTZzhFIKAiQEwXxNjS0lzg-gf6duESK94c"
      },
      data: formdata
    })
      .then(response => {
        reactotron.log(response);
      })
      .catch(error => {
        reactotron.log("error", error);
      });
  };
};

export const setImagePath = path => {
  return dispatch => {
    dispatch({ type: Types.SET_IMAGE_URI, path });
  };
};
