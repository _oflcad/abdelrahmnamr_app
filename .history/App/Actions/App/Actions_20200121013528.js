import * as Types from "./Types";
import axios from "axios";
import ApiService from "../../Services/ApiService";
import NavigationService from "../../Services/NavigationService";
import reactotron from "reactotron-react-native";
import { FileSystem } from "react-native-unimodules";

export const setFirstTimer = language => {
  return dispatch => {
    dispatch({ type: Types.FIRST_TIMER, language });
  };
};

export const setDeviceLanguage = language => {
  return dispatch => {
    dispatch({ type: Types.SET_DEVICE_LANGUAGE, language });
  };
};

export const fetchLatestImages = () => {
  return dispatch => {
    // API CALL FOR LATEST 6 IMAGES
    dispatch({ type: Types.FETCH_LATEST_IMAGES });
  };
};

export const fetchLatestNews = () => {
  return dispatch => {
    dispatch({ type: Types.LOADINGS });
    try {
      axios
        .get(ApiService.newsUrl)
        .then(response => {
          dispatch({ type: Types.FETCH_LATEST_NEWS_SUCCESS });
        })
        .catch(error => {
          dispatch({ type: Types.FETCH_LATEST_NEWS_FAILURE });
        });
    } catch (error) {
      dispatch({ type: Types.FETCH_LATEST_NEWS_FAILURE });
    }
  };
};

export const fetchLatestSessions = () => {
  return dispatch => {
    dispatch({ type: Types.LOADINGS });
    try {
      axios
        .get(ApiService.newsUrl)
        .then(response => {
          dispatch({ type: Types.FETCH_LATEST_SESSIONS_SUCCESS });
        })
        .catch(error => {
          dispatch({ type: Types.FETCH_LATEST_SESSIONS_FAILURE });
        });
    } catch (error) {
      dispatch({ type: Types.FETCH_LATEST_SESSIONS_FAILURE });
    }
  };
};

export const fetchLatestBlog = () => {
  return dispatch => {
    dispatch({ type: Types.LOADINGS });
    try {
      axios
        .get(ApiService.newsUrl)
        .then(response => {
          dispatch({ type: Types.FETCH_LATEST_BLOG_SUCCESS, response });
        })
        .catch(error => {
          dispatch({ type: Types.FETCH_LATEST_BLOG_FAILURE, error });
        });
    } catch (error) {
      dispatch({ type: Types.FETCH_LATEST_BLOG_FAILURE });
    }
  };
};

export const setItem = item => {
  return dispatch => {
    dispatch({ type: Types.SET_ITEM, item });
  };
};

export const clearItem = () => {
  return dispatch => {
    dispatch({ type: Types.CLEAR_ITEM });
  };
};

export const setRouteName = routeName => {
  return dispatch => {
    dispatch({ type: Types.ROUTE_NAME, routeName });
  };
};

export const setItemToBasket = song => {
  return dispatch => {
    dispatch({ type: Types.SET_ITEM_TO_BASKET, song });
  };
};

export const removeItemToBasket = () => {
  return dispatch => {
    dispatch({ type: Types.REMOVE_ITEM_FROM_BASKET });
  };
};

export const removeItemfromBasket = item => {
  return dispatch => {
    dispatch({ type: Types.REMOVE_ITEM_TO_BASKET, item });
  };
};

export const addItemToBasket = item => {
  return dispatch => {
    dispatch({ type: Types.ADD_ITEM_TO_BASKET, item });
  };
};

export const navigateToPlayer = () => {
  return dispatch => {
    NavigationService.navigate("Player");
  };
};

export const calculateTotal = () => {
  return (dispatch, getState) => {
    const { basket } = getState().app;
    let newPrice = 0;
    if (basket.length > 0) {
      basket.forEach(el => {
        newPrice += el.price;
      });
      dispatch({ type: Types.CALCULATE_TOTAL, newPrice });
    } else {
      dispatch({ type: Types.CALCULATE_TOTAL, newPrice });
    }
  };
};

export const buyProduct = () => {
  return (dispatch, getState) => {
    var myHeaders = new Headers();
myHeaders.append("Authorization", "Bearer eyJhbGciOiJSUzI1NiJ9.eyJyb2xlcyI6WyJST0xFX1VTRVIiXSwidXNlcm5hbWUiOiJoYW1kb3VuIiwiaWF0IjoxNTc5NTYzMzIzLCJleHAiOjE1Nzk1NjY5MjN9.oeqCzJx86wo4xvd-AB_UfTWfBuDO87kD69NDCiFnRWlg9LUziBxQWA-llVV3elqll0RthoYCVfgUB8LfTA2yeR8tTcNWQCeQkH588cqdYW-0hAOXVPr_x70szdnFP9NVzaaS2ixze0oQtUhBqu1gxr7MLf_L1KqlpScRNRO2an4RNN2e6C-e60Sil9RowAClZAhy9y_C-ohp1x7KuDjjNJBbzUFE5xqwvoAueMdV3DUbb4El74WsjCrL_kvTpjgwUr4CYbOVHZkxw5EaXr2hMVj3SX4tZQUack5sotPBfCg9pJLhS-3z2IlBgKF8-JxQfZk-BD2pDACrTBqDrT-sfnWz-GFnoSeQyveGAr87ZTTXIhMFqgOjqJrb83pmgI6OZ1rmqcSSz_Jy7qAMDfO6KjfO8lWc_9V3b2qecWNW-22GE27pmoRSRjJb6Fqs3hczqYb9aghfF24kOkSDjEhx7moRDQUkD6tQe7NYyfhlWoR0AbaH_p2la0VSeMXoLPFzpVL5MQd_52NTS2k-mxSMUqgsGWixWzejNti-LHqjEVg_KkkiVGC4kDdZOo3xswfNbYgHDKXdBn9qELHQJQ_w89toJZyMBO-G_S3GllkbJ6WhwWPeF72jBPwZt5oY3GGulBwsVnkjKZWmz7U4Q0cdiLpT9L-NZ688VwRdi2enzZY");
myHeaders.append("X-Requested-With", "");
myHeaders.append("Content-Type", "multipart/form-data; boundary=--------------------------793906886744527352622454");

var formdata = new FormData();
formdata.append("name", "hmayda");
formdata.append("email", "oflcad@gmail.com");
formdata.append("phone", "58141956");
formdata.append("comment", "");
formdata.append("cart", {prod1: 6});

var requestOptions = {
  method: 'POST',
  headers: myHeaders,
  body: formdata,
  redirect: 'follow'
};

fetch("https://www.abdelrahman-amr.com/web/app_dev.php/api/user/post/order", requestOptions)
  .then(response => response.text())
  .then(result => console.log(result))
  .catch(error => console.log('error', error));
  };
};

export const setImagePath = path => {
  return dispatch => {
    dispatch({ type: Types.SET_IMAGE_URI, path });
  };
};
