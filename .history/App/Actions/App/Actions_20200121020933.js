import * as Types from "./Types";
import axios from "axios";
import ApiService from "../../Services/ApiService";
import NavigationService from "../../Services/NavigationService";
import reactotron from "reactotron-react-native";
import { FileSystem } from "react-native-unimodules";

export const setFirstTimer = language => {
  return dispatch => {
    dispatch({ type: Types.FIRST_TIMER, language });
  };
};

export const setDeviceLanguage = language => {
  return dispatch => {
    dispatch({ type: Types.SET_DEVICE_LANGUAGE, language });
  };
};

export const fetchLatestImages = () => {
  return dispatch => {
    // API CALL FOR LATEST 6 IMAGES
    dispatch({ type: Types.FETCH_LATEST_IMAGES });
  };
};

export const fetchLatestNews = () => {
  return dispatch => {
    dispatch({ type: Types.LOADINGS });
    try {
      axios
        .get(ApiService.newsUrl)
        .then(response => {
          dispatch({ type: Types.FETCH_LATEST_NEWS_SUCCESS });
        })
        .catch(error => {
          dispatch({ type: Types.FETCH_LATEST_NEWS_FAILURE });
        });
    } catch (error) {
      dispatch({ type: Types.FETCH_LATEST_NEWS_FAILURE });
    }
  };
};

export const fetchLatestSessions = () => {
  return dispatch => {
    dispatch({ type: Types.LOADINGS });
    try {
      axios
        .get(ApiService.newsUrl)
        .then(response => {
          dispatch({ type: Types.FETCH_LATEST_SESSIONS_SUCCESS });
        })
        .catch(error => {
          dispatch({ type: Types.FETCH_LATEST_SESSIONS_FAILURE });
        });
    } catch (error) {
      dispatch({ type: Types.FETCH_LATEST_SESSIONS_FAILURE });
    }
  };
};

export const fetchLatestBlog = () => {
  return dispatch => {
    dispatch({ type: Types.LOADINGS });
    try {
      axios
        .get(ApiService.newsUrl)
        .then(response => {
          dispatch({ type: Types.FETCH_LATEST_BLOG_SUCCESS, response });
        })
        .catch(error => {
          dispatch({ type: Types.FETCH_LATEST_BLOG_FAILURE, error });
        });
    } catch (error) {
      dispatch({ type: Types.FETCH_LATEST_BLOG_FAILURE });
    }
  };
};

export const setItem = item => {
  return dispatch => {
    dispatch({ type: Types.SET_ITEM, item });
  };
};

export const clearItem = () => {
  return dispatch => {
    dispatch({ type: Types.CLEAR_ITEM });
  };
};

export const setRouteName = routeName => {
  return dispatch => {
    dispatch({ type: Types.ROUTE_NAME, routeName });
  };
};

export const setItemToBasket = song => {
  return dispatch => {
    dispatch({ type: Types.SET_ITEM_TO_BASKET, song });
  };
};

export const removeItemToBasket = () => {
  return dispatch => {
    dispatch({ type: Types.REMOVE_ITEM_FROM_BASKET });
  };
};

export const removeItemfromBasket = item => {
  return dispatch => {
    dispatch({ type: Types.REMOVE_ITEM_TO_BASKET, item });
  };
};

export const addItemToBasket = item => {
  return dispatch => {
    dispatch({ type: Types.ADD_ITEM_TO_BASKET, item });
  };
};

export const navigateToPlayer = () => {
  return dispatch => {
    NavigationService.navigate("Player");
  };
};

export const calculateTotal = () => {
  return (dispatch, getState) => {
    const { basket } = getState().app;
    let newPrice = 0;
    if (basket.length > 0) {
      basket.forEach(el => {
        newPrice += el.price;
      });
      dispatch({ type: Types.CALCULATE_TOTAL, newPrice });
    } else {
      dispatch({ type: Types.CALCULATE_TOTAL, newPrice });
    }
  };
};

export const buyProduct = () => {
  return (dispatch, getState) => {
    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer eyJhbGciOiJSUzI1NiJ9.eyJyb2xlcyI6WyJST0xFX1VTRVIiXSwidXNlcm5hbWUiOiJvZmxjYWQiLCJpYXQiOjE1Nzk1NjcwMzIsImV4cCI6MTU3OTU3MDYzMn0.LZeU4zimL6o02vxnJd8h67gQ-lIcfvfbFIh5o_ag60Q542JuQAO3CEeRddRDKvktkJWQEl2bTHVH7Wl2Wgbi2_D5QMwU1HLipVTwFXgDQAv_tq--S-lsh6hpFRYadhCQnEjAIuxjvH8ryuqpQft10PmKXe3Q9USdrZ1dliFnyVSEf0SWxgFFEr4xllZBK3VmCBc03mvzjsotltj-S3sB7d27qLw9W-XJzLoLDStSD6G_VaCDyEbLpx8Ux6SMf7O5v7geKC2l0bsLLUcTWwLF0vul9Y6EUuM2IhX8ZZ7b-4JMhWmCQORFJUe2-Qioao2IIUxw6tqfrg4Z2Yt_2s_idUXVNL9ZkgunvHHKXiXFyiYLh44munYF6P0-2OEEIju-Zw6GZUbBk-yjyHqtxXU9RUVbZhyavJ0BTUGdk3LMP2oWMvkDqOmbhSpDqvJjN9ARrfOD7Yl8msffaoBMHSOoovmp6k4E_WNXd84LoJkUWR2qJzQ1opd1XI5YZClMfaLhK4JjS7gwQfDNG6U3MZUT8f3bHWSrQb0GB9Wp8ox-iENcKFDhXqTOkmjo8B0a43drF-JaJfQR1ZO99xW3HPFEXq2AaMhBvBsW8XjFt7iFszv2iLRc9DpfPanhCf_CYML_jb54F8qOWCd1eP3z9LmMj44E6ubIlty6zNz-DbQRttE"
    );
    myHeaders.append("X-Requested-With", "");
    myHeaders.append(
      "Content-Type",
      "multipart/form-data; boundary=--------------------------793906886744527352622454"
    );
    var formdata = new FormData();
    formdata.append("name", "hmayda");
    formdata.append("email", "oflcad@gmail.com");
    formdata.append("phone", "58141956");
    formdata.append("comment", "");
    formdata.append("cart", '{"prod1": "6"}');
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formdata,
      redirect: "follow"
    };

    fetch(
      "https://www.abdelrahman-amr.com/web/app_dev.php/api/user/post/order",
      requestOptions
    )
      .then(response => response.text())
      .then(result => reactotron.log(result))
      .catch(error => reactotron.log("error", error));
  };
};

export const setImagePath = path => {
  return dispatch => {
    dispatch({ type: Types.SET_IMAGE_URI, path });
  };
};
