import * as Types from "./Types";
import axios from "axios";
import ApiService from "../../Services/ApiService";
import NavigationService from '../../Services/NavigationService'
export const setFirstTimer = language => {
  return dispatch => {
    dispatch({ type: Types.FIRST_TIMER, language });
  };
};

export const setDeviceLanguage = language => {
  return dispatch => {
    dispatch({ type: Types.SET_DEVICE_LANGUAGE, language });
  };
};

export const fetchLatestImages = () => {
  return dispatch => {
    // API CALL FOR LATEST 6 IMAGES
    dispatch({ type: Types.FETCH_LATEST_IMAGES });
  };
};

export const fetchLatestNews = () => {
  return dispatch => {
    dispatch({ type: Types.LOADINGS });
    try {
      axios
        .get(ApiService.newsUrl)
        .then(response => {
          dispatch({ type: Types.FETCH_LATEST_NEWS_SUCCESS });
        })
        .catch(error => {
          dispatch({ type: Types.FETCH_LATEST_NEWS_FAILURE });
        });
    } catch (error) {
      dispatch({ type: Types.FETCH_LATEST_NEWS_FAILURE });
    }
  };
};

export const fetchLatestSessions = () => {
  return dispatch => {
    dispatch({ type: Types.LOADINGS });
    try {
      axios
        .get(ApiService.newsUrl)
        .then(response => {
          dispatch({ type: Types.FETCH_LATEST_SESSIONS_SUCCESS });
        })
        .catch(error => {
          dispatch({ type: Types.FETCH_LATEST_SESSIONS_FAILURE });
        });
    } catch (error) {
      dispatch({ type: Types.FETCH_LATEST_SESSIONS_FAILURE });
    }
  };
};

export const fetchLatestBlog = () => {
  return dispatch => {
    dispatch({ type: Types.LOADINGS });
    try {
      axios
        .get(ApiService.newsUrl)
        .then(response => {
          dispatch({ type: Types.FETCH_LATEST_BLOG_SUCCESS, response });
        })
        .catch(error => {
          dispatch({ type: Types.FETCH_LATEST_BLOG_FAILURE, error });
        });
    } catch (error) {
      dispatch({ type: Types.FETCH_LATEST_BLOG_FAILURE });
    }
  };
};

export const setItem = item => {
  return dispatch => {
    dispatch({ type: Types.SET_ITEM, item });
  };
};

export const clearItem = () => {
  return dispatch => {
    dispatch({ type: Types.CLEAR_ITEM });
  };
};

export const setRouteName = (routeName) => {
  return dispatch => {
    dispatch({type: Types.ROUTE_NAME , routeName})
  }
}

export const setItemToBasket = song => {
  return dispatch => {
    dispatch({type: Types.SET_ITEM_TO_BASKET, song})
  }
}

export const removeItemToBasket = () => {
  return dispatch => {
    dispatch({type: Types.REMOVE_ITEM_FROM_BASKET})
  }
}



export const removeItemfromBasket = item => {
  return dispatch => {
    dispatch({type: Types.REMOVE_ITEM_TO_BASKET, item})
  }
}

export const addItemToBasket = (item) => {
  return dispatch => {
    dispatch({type: Types.ADD_ITEM_TO_BASKET, item})
  }
}


export const navigateToPlayer = () => {
  return dispatch => {
    NavigationService.navigate('Player');
  }
}

export const calculateTotal = () => {
  return (dispatch, getState) => {
    const {basket} = getState().app;
    let newPrice = 0;
    if(basket.length > 0) {
      basket.forEach(el => {
        newPrice += el.price;
      });
      dispatch({type: Types.CALCULATE_TOTAL, newPrice})
    } else {

      dispatch({type: Types.CALCULATE_TOTAL, newPrice})

    }
    
  }
}

export const buyProduct = () => {
  return (dispatch, getState) => {
    const { basket } = getState().app;
    
  }
}