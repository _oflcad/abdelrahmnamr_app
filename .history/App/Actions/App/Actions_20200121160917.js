import * as Types from "./Types";
import axios from "axios";
import ApiService from "../../Services/ApiService";
import NavigationService from "../../Services/NavigationService";
import reactotron from "reactotron-react-native";
import { FileSystem } from "react-native-unimodules";

export const setFirstTimer = language => {
  return dispatch => {
    dispatch({ type: Types.FIRST_TIMER, language });
  };
};

export const setDeviceLanguage = language => {
  return dispatch => {
    dispatch({ type: Types.SET_DEVICE_LANGUAGE, language });
  };
};

export const fetchLatestImages = () => {
  return dispatch => {
    // API CALL FOR LATEST 6 IMAGES
    dispatch({ type: Types.FETCH_LATEST_IMAGES });
  };
};

export const fetchLatestNews = () => {
  return dispatch => {
    dispatch({ type: Types.LOADINGS });
    try {
      axios
        .get(ApiService.newsUrl)
        .then(response => {
          dispatch({ type: Types.FETCH_LATEST_NEWS_SUCCESS });
        })
        .catch(error => {
          dispatch({ type: Types.FETCH_LATEST_NEWS_FAILURE });
        });
    } catch (error) {
      dispatch({ type: Types.FETCH_LATEST_NEWS_FAILURE });
    }
  };
};

export const fetchLatestSessions = () => {
  return dispatch => {
    dispatch({ type: Types.LOADINGS });
    try {
      axios
        .get(ApiService.newsUrl)
        .then(response => {
          dispatch({ type: Types.FETCH_LATEST_SESSIONS_SUCCESS });
        })
        .catch(error => {
          dispatch({ type: Types.FETCH_LATEST_SESSIONS_FAILURE });
        });
    } catch (error) {
      dispatch({ type: Types.FETCH_LATEST_SESSIONS_FAILURE });
    }
  };
};

export const fetchLatestBlog = () => {
  return dispatch => {
    dispatch({ type: Types.LOADINGS });
    try {
      axios
        .get(ApiService.newsUrl)
        .then(response => {
          dispatch({ type: Types.FETCH_LATEST_BLOG_SUCCESS, response });
        })
        .catch(error => {
          dispatch({ type: Types.FETCH_LATEST_BLOG_FAILURE, error });
        });
    } catch (error) {
      dispatch({ type: Types.FETCH_LATEST_BLOG_FAILURE });
    }
  };
};

export const setItem = item => {
  return dispatch => {
    dispatch({ type: Types.SET_ITEM, item });
  };
};

export const clearItem = () => {
  return dispatch => {
    dispatch({ type: Types.CLEAR_ITEM });
  };
};

export const setRouteName = routeName => {
  return dispatch => {
    dispatch({ type: Types.ROUTE_NAME, routeName });
  };
};

export const setItemToBasket = song => {
  return dispatch => {
    dispatch({ type: Types.SET_ITEM_TO_BASKET, song });
  };
};

export const removeItemToBasket = () => {
  return dispatch => {
    dispatch({ type: Types.REMOVE_ITEM_FROM_BASKET });
  };
};

export const removeItemfromBasket = item => {
  return dispatch => {
    dispatch({ type: Types.REMOVE_ITEM_TO_BASKET, item });
  };
};

export const addItemToBasket = item => {
  return dispatch => {
    dispatch({ type: Types.ADD_ITEM_TO_BASKET, item });
  };
};

export const navigateToPlayer = () => {
  return dispatch => {
    NavigationService.navigate("Player");
  };
};

export const calculateTotal = () => {
  return (dispatch, getState) => {
    const { basket } = getState().app;
    let newPrice = 0;
    if (basket.length > 0) {
      basket.forEach(el => {
        newPrice += el.price;
      });
      dispatch({ type: Types.CALCULATE_TOTAL, newPrice });
    } else {
      dispatch({ type: Types.CALCULATE_TOTAL, newPrice });
    }
  };
};

export const buyProduct = () => {
  return (dispatch, getState) => {
    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer eyJhbGciOiJSUzI1NiJ9.eyJyb2xlcyI6WyJST0xFX1VTRVIiXSwidXNlcm5hbWUiOiJvZmxjYWQiLCJpYXQiOjE1Nzk2MTkzNDAsImV4cCI6MTU3OTYyMjk0MH0.XePTKa764rwIGJPpCrk3t7Ke129QNSaS6PkAScS6VLdfRXa7H0bQkT9n13jfnUhV-USuqehjxia3WG87vm89RYrct6rEWo74jZB-c1gTueV_800iNRUotIjfR3qepscOuCFj8baN1nWmaZXtkqFX-mkal6WdZVpMXVin_KAf6pFzq-fj-ga0em4hn42UCqO0OcXchnBcjo9k9ObqgmDrHMjlmS0UdTsl7EIzx7eG56OvCX5ZpLw15ihjbFw_gUCvdB5911mk7pM36kSLttL8h6ND0L03BpQPdQ46mbmYplIZfaKvsTrxjzOYhINX1QU8drLRNh5pX6NN47B2HNpgckDQFnglIEqFC_e7VjXeVa9ll5_82ipFSRVqCS-yHlazhcRXLtsicVinZipkhUFtIZLK5eE4PocBN6v_F13RyBXnSELovjQs8hk17jdLLfqKSITWFuMKTrRvBPTn5jxAXvMVzPJQwW_LHoxyrJU4TIRmxZsuUteijE427gAfV35f_nd9Q6ySwcST4HofvKdz5fjn9CJBzTEhAasHhoSn6O6r_QhZ-EbeDA8oXwuCr6BRIjWC1gB5Zhb3CLnFPMhLhpc7rPI_SyQ0goZ6xAC29L-IVwDWdjL5hozxFytIEpvB3J5TkeN1InSX51Gl48DixjCHsUlkHU8zlHaTvULMhNI"
    );
    myHeaders.append(
      "Content-Type",
      "multipart/form-data; boundary=--------------------------363621818632658501614475"
    );

    var formdata = new FormData();
    formdata.append("name", "hmayda");
    formdata.append("email", "oflcad@gmail.com");
    formdata.append("phone", "58141956");
    formdata.append("comment", "");
    formdata.append("cart", "6,4");

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formdata,
      redirect: "follow"
    };

    axios({
      method: "POST",
      url: ApiService.buyProducts,
      headers: myHeaders,
      data: formdata
    })
      .then(response => {
        console.log(response);
      })
      .catch(error => {
        console.log("error", error);
      });
  };
};

export const setImagePath = path => {
  return dispatch => {
    dispatch({ type: Types.SET_IMAGE_URI, path });
  };
};
