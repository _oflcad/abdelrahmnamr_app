import * as Types from "./Types";
import axios from "axios";
import ApiService from "../../Services/ApiService";
import NavigationService from "../../Services/NavigationService";
import reactotron from "reactotron-react-native";
import { FileSystem } from "react-native-unimodules";

export const setFirstTimer = language => {
  return dispatch => {
    dispatch({ type: Types.FIRST_TIMER, language });
  };
};

export const setDeviceLanguage = language => {
  return dispatch => {
    dispatch({ type: Types.SET_DEVICE_LANGUAGE, language });
  };
};

export const fetchLatestImages = () => {
  return dispatch => {
    // API CALL FOR LATEST 6 IMAGES
    dispatch({ type: Types.FETCH_LATEST_IMAGES });
  };
};

export const fetchLatestNews = () => {
  return dispatch => {
    dispatch({ type: Types.LOADINGS });
    try {
      axios
        .get(ApiService.newsUrl)
        .then(response => {
          dispatch({ type: Types.FETCH_LATEST_NEWS_SUCCESS });
        })
        .catch(error => {
          dispatch({ type: Types.FETCH_LATEST_NEWS_FAILURE });
        });
    } catch (error) {
      dispatch({ type: Types.FETCH_LATEST_NEWS_FAILURE });
    }
  };
};

export const fetchLatestSessions = () => {
  return dispatch => {
    dispatch({ type: Types.LOADINGS });
    try {
      axios
        .get(ApiService.newsUrl)
        .then(response => {
          dispatch({ type: Types.FETCH_LATEST_SESSIONS_SUCCESS });
        })
        .catch(error => {
          dispatch({ type: Types.FETCH_LATEST_SESSIONS_FAILURE });
        });
    } catch (error) {
      dispatch({ type: Types.FETCH_LATEST_SESSIONS_FAILURE });
    }
  };
};

export const fetchLatestBlog = () => {
  return dispatch => {
    dispatch({ type: Types.LOADINGS });
    try {
      axios
        .get(ApiService.newsUrl)
        .then(response => {
          dispatch({ type: Types.FETCH_LATEST_BLOG_SUCCESS, response });
        })
        .catch(error => {
          dispatch({ type: Types.FETCH_LATEST_BLOG_FAILURE, error });
        });
    } catch (error) {
      dispatch({ type: Types.FETCH_LATEST_BLOG_FAILURE });
    }
  };
};

export const setItem = item => {
  return dispatch => {
    dispatch({ type: Types.SET_ITEM, item });
  };
};

export const clearItem = () => {
  return dispatch => {
    dispatch({ type: Types.CLEAR_ITEM });
  };
};

export const setRouteName = routeName => {
  return dispatch => {
    dispatch({ type: Types.ROUTE_NAME, routeName });
  };
};

export const setItemToBasket = song => {
  return dispatch => {
    dispatch({ type: Types.SET_ITEM_TO_BASKET, song });
  };
};

export const removeItemToBasket = () => {
  return dispatch => {
    dispatch({ type: Types.REMOVE_ITEM_FROM_BASKET });
  };
};

export const removeItemfromBasket = item => {
  return dispatch => {
    dispatch({ type: Types.REMOVE_ITEM_TO_BASKET, item });
  };
};

export const addItemToBasket = item => {
  return dispatch => {
    dispatch({ type: Types.ADD_ITEM_TO_BASKET, item });
  };
};

export const navigateToPlayer = () => {
  return dispatch => {
    NavigationService.navigate("Player");
  };
};

export const calculateTotal = () => {
  return (dispatch, getState) => {
    const { basket } = getState().app;
    let newPrice = 0;
    if (basket.length > 0) {
      basket.forEach(el => {
        newPrice += el.price;
      });
      dispatch({ type: Types.CALCULATE_TOTAL, newPrice });
    } else {
      dispatch({ type: Types.CALCULATE_TOTAL, newPrice });
    }
  };
};

export const buyProduct = () => {
  return (dispatch, getState) => {
    var myHeaders = new Headers();
    let cartArr = [6];
    myHeaders.append(
      "Authorization",
      "Bearer eyJhbGciOiJSUzI1NiJ9.eyJyb2xlcyI6WyJST0xFX1VTRVIiXSwidXNlcm5hbWUiOiJoYW1kb3VuIiwiaWF0IjoxNTc5NjE2NDExLCJleHAiOjE1Nzk2MjAwMTF9.sRomTQwRMyO26sPY5SLDdFe78SSe_jZZ0u1IFFt-yV6hsogqUsV2BomnWj6G0OwtB8KfRDrsLl9PZTiP6OZuCXjyfdn3I6YnhL67EEaO0qDY08ZsRIx7ckzu5tJ7-7aBKr15hPuUBO4-MKguAY44JB0ED5cZmlyu58ouYDPG4zLmobC94BeQQ_oq3pKIC8erUpJXkyD7iAOlv4rb7kw-9FYlBB6CHj80lGc-eyp9UsSEVGF5C-p4UBU95G5xrRRrBtIKjkUJ2mFaYiQ96bH_odzugCxu1vgAFVfWy0Rrz4xJBja3e8484QF5koeXwFQnH57Rm9HPKo5xueyaj5CN592EiYpXuKK1__AquJ9WszyZ3CeQSJTeZyxI7v0-KxGKhj6NYrpRoXvNS3_dRmQ7Nsm93X0PsKJoQXhub1SLiWS4sjQ6Gt07KYbhvaQmDydSTqPdhWifso9IriSyd2Pf9y-5wTPPwJQO8b00dUyFQ3DP3O7DceDCKR_dWa5gxFuyG5tNvocFKKIOhOP757BH128nV4uxSYUFZ4gjuvv51t9YSxYLMldJmdRsAjLdCRVKWVaEFfkbCfOXnGJx5Qmj13rPk1D2Hs0OXdHpJclGwfbhddjurxeMEe7hjDuFa4p4AAEyRnOvErsZEAytv6j_6MNNwAm1ngwqJGDZo5QW_t8"
    );
    myHeaders.append("X-Requested-With", "");
    myHeaders.append(
      "Content-Type",
      "multipart/form-data; boundary=--------------------------793906886744527352622454"
    );
    var formdata = new FormData();
    formdata.append("name", "hmayda");
    formdata.append("email", "oflcad@gmail.com");
    formdata.append("phone", "58141956");
    formdata.append("comment", "");
    console.log();
    formdata.append("cart", {
      prod1: 6
    });
    console.log(formdata);
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: formdata,
      redirect: "follow"
    };

    fetch(
      "https://www.abdelrahman-amr.com/web/app_dev.php/api/user/post/order",
      requestOptions
    )
      .then(response => response.text())
      .then(result => reactotron.log(result))
      .catch(error => reactotron.log("error", error));
  };
};

export const setImagePath = path => {
  return dispatch => {
    dispatch({ type: Types.SET_IMAGE_URI, path });
  };
};
