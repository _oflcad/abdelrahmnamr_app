import React from "react";
import { Text, View, ScrollView, SafeAreaView } from "react-native";
import { connect } from "react-redux";
import { ApplicationStyles, Helpers, Metrics, Words, Fonts } from "../../Theme";
import HeaderComponent from "../../Components/Header/HeaderComponent";
import LoginContainer from "../../Components/LoginContainer/LoginContainer";
import AccountSection from "../../Components/AccountSection/AccountSection";
import LanguageSection from "../../Components/LanguageSection/LanguageSection";
import SupportSection from "../../Components/SupportSection/SupportSection";
import SocialSection from "../../Components/SocialSection/SocialSection";
import Logout from "../../Components/Logout/Logout";
class SettingsScreen extends React.Component {
  render() {
    const { language } = this.props;
    return (
      <SafeAreaView style={[Helpers.fill]}>
        <HeaderComponent />
        <ScrollView
          showsVerticalScrollIndicator={false}
          bounces
          keyboardShouldPersistTaps={"always"}
          style={[Helpers.fill]}
          contentContainerStyle={[Metrics.scrollViewBottomMargin]}
        >
          <View style={[Helpers.center, Metrics.smallVerticalMargin]}>
            <LoginContainer />
          </View>
          <View style={[ApplicationStyles.settingsSectionTitleContainer, language === 'ar' ? Helpers.crossEnd : null]}>
            <Text style={[Fonts.h5, Metrics.smallHorizontalMargin, ]}>
              {language === "en"
                ? Words.en.settings.Profile
                : Words.ar.settings.Profile
              }
            </Text>
          </View>
          <View style={[Helpers.center, Metrics.smallVerticalMargin,]}>
            <AccountSection />
          </View>
          
          <View style={[ApplicationStyles.settingsSectionTitleContainer,language === 'ar' ? Helpers.crossEnd : null]}>
            <Text style={[Fonts.h5, Metrics.smallHorizontalMargin]}>
              {language === "en"
                ? Words.en.settings.Language
                : Words.ar.settings.Language}
            </Text>
          </View>
          <View style={[Helpers.center, Metrics.smallVerticalMargin]}>
            <LanguageSection />
          </View>
          <View style={[ApplicationStyles.settingsSectionTitleContainer,language === 'ar' ? Helpers.crossEnd : null]}>
            <Text style={[Fonts.h5, Metrics.smallHorizontalMargin]}>
              {language === "en"
                ? Words.en.settings.Support
                : Words.ar.settings.Support}
            </Text>
          </View>
          <View style={[Helpers.center, Metrics.smallVerticalMargin]}>
            <SupportSection />
          </View>
          <View style={[ApplicationStyles.settingsSectionTitleContainer,language === 'ar' ? Helpers.crossEnd : null]}>
            <Text style={[Fonts.h5, Metrics.smallHorizontalMargin]}>
              {language === "en"
                ? Words.en.settings.GetInTouch
                : Words.ar.settings.GetInTouch}
            </Text>
          </View>
          <View style={[Helpers.center, Metrics.smallVerticalMargin]}>
            <SocialSection />
          </View>
          <View style={[Helpers.center, Metrics.smallHorizontalMargin]}>
              <Logout />
          </View>
        </ScrollView>
        
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  language: state.auth.language,
});

export default connect(mapStateToProps, null)(SettingsScreen);
