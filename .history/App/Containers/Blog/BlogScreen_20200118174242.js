import React, { Component } from "react";
import { View, Text, ScrollView, SafeAreaView, FlatList } from "react-native";
import { connect } from "react-redux";
import { Helpers, Fonts, Metrics, Words, Data } from "../../Theme";
import NestedHeader from "../../Components/NestedHeader";
import ReusableCard from "../../Components/ReusableCard/ReusableCard";
import { fetchLatestBlog } from "../../Actions/App/Actions";
class BlogScreen extends Component {
  render() {
    const { language } = this.props;
    let title = language === "ar" ? Words.ar.nestedHeader.Blog : null;
    return (
      <SafeAreaView style={[Helpers.fill]}>
        <NestedHeader customTitle={title} />
        <View style={[language === "ar" ? Helpers.mainEnd : Helpers.mainStart]}>
          <Text
            style={[
              Fonts.h4,
              Metrics.mediumHorizontalMargin,
              Metrics.smallVerticalMargin
            ]}
          >
            {language === "en" ? Words.en.blog.blog : Words.ar.blog.blog}
          </Text>
        </View>
        <FlatList
          showsVerticalScrollIndicator={false}
          style={[Metrics.smallHorizontalMargin]}
          data={Data.blog}
          keyExtractor={item => "key" + item.id}
          renderItem={({ item }) => <ReusableCard item={item} />}
        />
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  blog: state.app.blog,
  language: state.app.language
});

const mapDispatchToProps = dispatch => {
  return {
    onDispatchFetchLatestBlog: () => dispatch(fetchLatestBlog())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BlogScreen);
