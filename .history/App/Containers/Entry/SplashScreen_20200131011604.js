import React, { Component } from "react";
import { View, Text, ImageBackground, ActivityIndicator } from "react-native";
import { connect } from "react-redux";
import { Helpers, Images, Metrics, Colors } from "../../Theme";
import NavigationService from "../../Services/NavigationService";

export class Entry extends Component {


    componentDidMount(){
        setTimeout(() => {
            NavigationService.navigate('App');
        }, 2200);
    }
  render() {
    return (
      <View style={[Helpers.fill]}>
        <ImageBackground
          source={Images.firstTimerBg}
          style={[Helpers.fullSize]}
        >
          <View
            style={[
              {
                bottom: 100,
                left: 0,
                position: "absolute",
                right: 0,
                backgroundColor: Colors.transparent
              },
              Metrics.mediumHorizontalMargin,
              Metrics.mediumVerticalMargin,
              Helpers.center
            ]}
          >
            <ActivityIndicator size={"large"} color={Colors.primary} />
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Entry);
