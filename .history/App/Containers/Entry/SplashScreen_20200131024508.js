import React, { Component } from "react";
import { View, Text, ImageBackground, ActivityIndicator } from "react-native";
import { connect } from "react-redux";
import { Helpers, Images, Metrics, Colors } from "../../Theme";
import NavigationService from "../../Services/NavigationService";

export class Entry extends Component {

  constructor(props) {
    super(props)
  
    this.state = {
       isLoading: false
    }
  }
  

    componentDidMount(){
      if(!this.props.isFirsTimer){
        setTimeout(() => {
          NavigationService.navigate('App')
        },2000)
      }
    }

    componentDidUpdate(prevProps) {
      if (prevProps.isFirstTimer !== this.props.isFirstTimer && !this.props.isFirsTimer) {
        this.setState({ isLoading: true });
        return true;
      }
    }


  _setLanguage = val => {
    this.setState({ isLoading: true });
    NavigationService.navigate('App')
  };
  render() {
    const {isFirsTimer } = this.props;
    const {isLoading} = this.state;
    return (
      <View style={[Helpers.fill]}>
        <ImageBackground
          source={Images.firstTimerBg}
          style={[Helpers.fullSize]}
        >
          <View
            style={[
              {
                bottom: 100,
                left: 0,
                position: "absolute",
                right: 0,
                backgroundColor: Colors.transparent
              },
              Metrics.mediumHorizontalMargin,
              Metrics.mediumVerticalMargin,
              Helpers.center
            ]}
          >
          {isLoading ? (
              <View
                style={[
                  Style.buttonWrapper,
                  Metrics.mediumHorizontalMargin,
                  Metrics.mediumVerticalMargin,
                  Helpers.center,
                  { backgroundColor: Colors.transparent }
                ]}
              >
                <ActivityIndicator size={"small"} color={Colors.primary} />
              </View>
            ) : (
              <View
                style={[
                  Style.buttonWrapper,
                  Helpers.mainCenter,
                  Helpers.mainSpaceBetween,
                  Helpers.row,
                  Metrics.mediumHorizontalMargin
                ]}
              >
                <TouchableOpacity
                  onPress={() => this._setLanguage("en")}
                  style={[Style.buttonContainer, Helpers.center]}
                >
                  <Text style={Fonts.normal}>{Words.english}</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this._setLanguage("ar")}
                  style={[Style.buttonContainer, Helpers.center]}
                >
                  <Text style={Fonts.normal}>{Words.arabic}</Text>
                </TouchableOpacity>
              </View>
            )}
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  isFirsTimer: state.app.isFirsTimer,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Entry);
