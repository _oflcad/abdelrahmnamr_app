import React, { Component } from "react";
import { View, Text, ImageBackground, ActivityIndicator, } from "react-native";
import { connect } from "react-redux";
import { Helpers, Images, Metrics, Colors, Fonts, Words } from "../../Theme";
import Style from "./Styles";
import { TouchableOpacity } from "react-native-gesture-handler";
import { setFirstTimer } from "../../Actions/App/Actions";
import { hideMiniPlayer } from "../../Actions/Media/Actions";
export class Entry extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false
    };
  }

  componentDidMount() {

    const {isFirsTimer} = this.props;
    
    if(!isFirsTimer) {
      this.setState({isLoading: true})
      this.props.onDispatchHideMiniPlayer();
      setTimeout(() => {
        this.props.navigation.navigate('App')
      },1800);
    }
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.isFirstTimer !== this.props.isFirstTimer &&
      !this.props.isFirsTimer
    ) {
      this.setState({ isLoading: true });
      this.props.onDispatchHideMiniPlayer();
      return true;
    }
  }

  _setLanguage = val => {
    this.setState({ isLoading: true });
    this.props.onDispatchsetFirstTimer(val);
    setTimeout(() => {
      this.props.navigation.navigate("App");
    },1800)
    
  };
  render() {
    const { isLoading } = this.state;
    return (
      <View style={[Helpers.fill]}>
        <ImageBackground
          source={Images.firstTimerBg}
          style={[Helpers.fullSize]}
        >
        
            {isLoading ? (
              <View
                style={[
                  Metrics.mediumHorizontalMargin,
                  Metrics.mediumVerticalMargin,
                  Helpers.center,
                  {
                    bottom: 100,
                    left: 0,
                    position: "absolute",
                    right: 0,
                    backgroundColor: Colors.transparent
                  }
                ]}
              >
                <ActivityIndicator size={"large"} color={Colors.white} />
              </View>
            ) : (
              <View
                style={[
                  {
                    bottom: 100,
                    left: 0,
                    position: "absolute",
                    right: 0,
                    backgroundColor: Colors.transparent
                  },
                  Helpers.mainCenter,
                  Helpers.mainSpaceBetween,
                  Helpers.row,
                  Metrics.mediumHorizontalMargin
                ]}
              >
                <TouchableOpacity
                  onPress={() => this._setLanguage("en")}
                  style={[Style.buttonContainer, Helpers.center]}
                >
                  <Text style={Fonts.normal}>{Words.english}</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this._setLanguage("ar")}
                  style={[Style.buttonContainer, Helpers.center]}
                >
                  <Text style={Fonts.normal}>{Words.arabic}</Text>
                </TouchableOpacity>
              </View>
            )}
         
        </ImageBackground>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  isFirsTimer: state.app.isFirsTimer
});

const mapDispatchToProps = dispatch => {
  return {
    onDispatchsetFirstTimer: val => dispatch(setFirstTimer(val)),
    onDispatchHideMiniPlayer : () => dispatch(hideMiniPlayer()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Entry);
