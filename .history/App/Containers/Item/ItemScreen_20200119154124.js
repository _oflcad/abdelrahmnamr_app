import React, { Component } from "react";
import { View, Text, Image, ScrollView, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import NestedHeader from "../../Components/NestedHeader";
import Styles from "./Styles";
import { Helpers, Fonts, Metrics, Words, Colors, Images } from "../../Theme";
import { addItemToBasket } from "../../Actions/App/Actions";
import Toast from 'react-native-easy-toast'

class ItemScreen extends Component {

  

  _handleAddToBasket = () => {
    const {basket, itemToBasket} = this.props;
    if(basket.indexOf(itemToBasket) === -1){
      this.props.onDispatchAddItemToBasket(itemToBasket)
    }

    {
      this.props.language === "en" ?
      this.refs.toast_basket.show(Words.en.audioAddedToBasket)
      :
      this.refs.toast_basket.show(Words.ar.audioAddedToBasket);
    }
    setTimeout(() => {
      this.props.navigation.navigate('Home')
    },500)
  };


  render() {
    const { itemToBasket, language } = this.props;
    let title = language === 'ar' ? Words.ar.nestedHeader.Audio : null; 
    return (
      <View style={[Helpers.fill]}>
        <NestedHeader customTitle={title}/>
        <ScrollView showsVerticalScrollIndicator={false} bounces >
          <View style={[Helpers.center, Metrics.smallVerticalMargin]}>
            <Text style={[Fonts.h4]}>{itemToBasket.name}</Text>
          </View>
          <View style={[Helpers.crossCenter]}>
            <Image
              source={{ uri: itemToBasket.image }}
              style={[
                Styles.image,
                Metrics.horizontalMargin,
                Metrics.smallVerticalMargin
              ]}
            />

            <Text
              style={[
                Fonts.normal,
                Metrics.horizontalMargin,
                Metrics.smallVerticalMargin,
                { textAlign: "center" }
              ]}
            >
              {itemToBasket.description}
            </Text>
          </View>
          <View
            style={[Helpers.crossCenter, ]}
          >
            <TouchableOpacity
              onPress={this._handleAddToBasket}
              style={Styles.addButton}
            >
              <Text style={[Fonts.normal, { color: Colors.white }]}>
                {language === "en"
                  ? Words.en.addToBasket
                  : Words.ar.addToBasket}
              </Text>
              <Text style={[Fonts.newPrice, { color: Colors.white }]}>
                {itemToBasket.price} $
              </Text>
              <Image resizeMode={"contain"} resizeMode={"contain"}source={Images.cart} style={{width: 25, height: 25}} />
            </TouchableOpacity>
          </View>
          <View style={{height: 150}} />
        </ScrollView>
        <Toast ref="toast_basket"/>
        
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    itemToBasket: state.app.itemToBasket,
    language: state.app.language,
    basket: state.app.basket,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onDispatchAddItemToBasket: song => dispatch(addItemToBasket(song))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ItemScreen);
