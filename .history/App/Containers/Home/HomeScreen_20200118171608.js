import React from "react";
import { View, Text, ScrollView, SafeAreaView } from "react-native";
import { connect } from "react-redux";
import { Helpers, Fonts, Words, Metrics } from "../../Theme";
import HeaderComponent from "../../Components/Header/HeaderComponent";
import ImageCarousel from "../../Components/ImageCarousel/ImageCarousel";
import SectionCarousel from "../../Components/SectionCarousel/SectionCarousel";
import SoundScroller from "../../Components/SoundScroller/SoundScroller";
import Player from "../../Components/Player/Player";
class HomeScreen extends React.Component {
  render() {
    const { language } = this.props;
    return (
      <SafeAreaView style={[Helpers.fill, Helpers.column, Metrics.crossCenter]}>
        <HeaderComponent />
        
          <ImageCarousel />
          <SectionCarousel />
          <View style={[language === 'ar' ? Helpers.rowReverse : null]}>
            <Text
              style={[
                Fonts.h4,
                Metrics.mediumHorizontalMargin,
                Metrics.smallVerticalMargin
              ]}
            >
              {language === 'en' ? Words.en.homeTitle : Words.ar.homeTitle}
            </Text>
          </View>
          <SoundScroller />
          
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  language: state.app.language
});

export default connect(mapStateToProps, null)(HomeScreen);
