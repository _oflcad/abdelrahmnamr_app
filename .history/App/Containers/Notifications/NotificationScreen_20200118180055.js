import React from "react";
import { Text, View, SafeAreaView, Image } from "react-native";
import { connect } from "react-redux";
import { setFirstTimer } from "../../Actions/App/Actions";
import { Fonts, Helpers, Images, Metrics, Words } from "../../Theme";
import NestedHeader from "../../Components/NestedHeader";
import Styles from "./Styles";
import LottieView from "lottie-react-native";

class NotificationScreen extends React.Component {
  render() {
    const { notifications, language } = this.props;
    let title = language === "ar" ? Words.ar.nestedHeader.News : null;
    return (
      <SafeAreaView style={Helpers.fill}>
        <NestedHeader customTitle={title} />
        {notifications.length > 0 ? (
          <View style={[Helpers.center]}>
            <Text> we do have notifications </Text>
          </View>
        ) : (
          <View style={[Helpers.center, { flex: 1 }]}>
            <Image
              style={Styles.animationLottie}
              source={Images.noNotification}
            />
          </View>
        )}
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => {
  return {
    notifications: state.app.notifications,
    language: state.app.language
  };
};

const mapDispatchToProps = dispatch => ({
  setFirstTimer: val => dispatch(setFirstTimer(val))
});

export default connect(mapStateToProps, mapDispatchToProps)(NotificationScreen);
