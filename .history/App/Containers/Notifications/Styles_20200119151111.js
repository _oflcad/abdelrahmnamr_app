import { StyleSheet } from 'react-native'
import { Helpers, Metrics, Fonts, Colors } from '../../Theme'

export default StyleSheet.create({
  animationLottie: {
    backgroundColor: Colors.white,
    width: Metrics.width / 2,
    height: Metrics.width / 2
  },
})
