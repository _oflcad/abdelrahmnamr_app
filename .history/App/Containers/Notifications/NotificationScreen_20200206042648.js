import React from "react";
import { Text, View, SafeAreaView, Image } from "react-native";
import { connect } from "react-redux";
import { setFirstTimer } from "../../Actions/App/Actions";
import { Fonts, Helpers, Images, Metrics, Words } from "../../Theme";
import NestedHeader from "../../Components/NestedHeader";
import Styles from "./Styles";

class NotificationScreen extends React.Component {
  constructor(props) {
    super(props)
  
    this.state = {
       data : [],
    }
  }

  componentDidMount(){
    this.setState({data: this.props.noNotification});
  }
  
  render() {
    const { language } = this.props;
    let title = language === "ar" ? Words.ar.nestedHeader.Notification : null;
    return (
      <SafeAreaView style={Helpers.fill}>
        <NestedHeader customTitle={title} />
        {this.state.data.length > 0 ? (
          <View style={[Helpers.center]}>
            <Text> we do have notifications </Text>
          </View>
        ) : (
          <View style={[Helpers.center, { flex: 1 }]}>
            <Image
              style={Styles.animationLottie}
              source={Images.noNotification}
            />
          </View>
        )}
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => {
  return {
    notifications: state.app.notifications,
    language: state.auth.language
  };
};

const mapDispatchToProps = dispatch => ({
  setFirstTimer: val => dispatch(setFirstTimer(val))
});

export default connect(mapStateToProps, mapDispatchToProps)(NotificationScreen);
