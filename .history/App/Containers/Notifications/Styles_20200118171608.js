import { StyleSheet } from 'react-native'
import { Helpers, Metrics, Fonts, Colors } from '../../Theme'

export default StyleSheet.create({
  animationLottie: {
    backgroundColor: Colors.white,
    width: Metrics.width,
    height: Metrics.width
  },
})
