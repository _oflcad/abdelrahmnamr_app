import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  ScrollView,
  SafeAreaView,
  TouchableOpacity
} from "react-native";

import { connect } from "react-redux";
import Styles from "./Styles";
import { Helpers, Metrics, Fonts, Colors, Words } from "../../Theme";
import NestedHeader from "../../Components/NestedHeader";
import moment from "moment";
import { Ionicons } from "@expo/vector-icons";
class ReusableItemScreen extends Component {
  _handleShareItem = () => {
    const { item } = this.props;
    //this.props.onDispatchShareItem(item)
  };

  render() {
    const { item } = this.props;
    let title = " ";
    return (
      <SafeAreaView style={[Helpers.fill, Metrics.smallHorizontalMargin]}>
        <NestedHeader customTitle={title} />
        <ScrollView bounces showsVerticalScrollIndicator={false}>
          <View style={[Helpers.mainStart, Metrics.smallHorizontalMargin]}>
            <Text style={[Fonts.h2, Metrics.smallVerticalMargin]}>
              {item.title}
            </Text>
          </View>
          <View>
            <Image source={{ uri: item.imageUrl }} style={Styles.image} />
          </View>
          <View style={[Helpers.crossEnd, Metrics.horizontalMargin]}>
            <Text
              style={[
                Fonts.h5,
                Metrics.smallVerticalMargin,
                { textAlign: "left" }
              ]}
            >
              {item.description}
            </Text>
          </View>
          <View
            style={[
              Helpers.rowReverse,
              Helpers.crossCenter,
              Helpers.mainSpaceBetween,
              Metrics.smallHorizontalMargin,
              Metrics.verticalMargin
            ]}
          >
            <View
              style={[
                Helpers.row,
                Helpers.mainEnd,
                Helpers.crossCenter,
                Metrics.horizontalMargin,
                Metrics.verticalMargin
              ]}
            >
              <Text style={[Fonts.subTitle, { fontSize: 14 },Metrics.tinyHorizontalMargin]}>
                {moment(item.createdAt).fromNow()}
              </Text>
              <Ionicons name="ios-calendar" size={18} color={Colors.grey150} />
            </View>
            <TouchableOpacity onPress={this._handleShareItem}>
              <Ionicons name="ios-share" size={25} color={Colors.grey200} />
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  item: state.app.item,
  language: state.app.language
});

export default connect(mapStateToProps, null)(ReusableItemScreen);
