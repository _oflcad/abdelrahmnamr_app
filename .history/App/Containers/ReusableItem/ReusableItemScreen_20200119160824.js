import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  ScrollView,
  SafeAreaView,
  TouchableOpacity
} from "react-native";

import { connect } from "react-redux";
import Styles from "./Styles";
import { Helpers, Metrics, Fonts, Colors, Words, Images } from "../../Theme";
import NestedHeader from "../../Components/NestedHeader";
import moment from "moment";

class ReusableItemScreen extends Component {
  _handleShareItem = () => {
    const { item } = this.props;
    //this.props.onDispatchShareItem(item)
  };

  render() {
    const { item } = this.props;
    let title = " ";
    return (
      <SafeAreaView style={[Helpers.fill, Metrics.smallHorizontalMargin]}>
        <NestedHeader customTitle={title} />
        <ScrollView bounces showsVerticalScrollIndicator={false}>
          <View style={[Helpers.mainStart, Metrics.smallHorizontalMargin]}>
            <Text style={[Fonts.h2, Metrics.smallVerticalMargin]}>
              {item.title}
            </Text>
          </View>
          <View>
            <Image resizeMode={"contain"} resizeMode={"contain"}source={{ uri: item.imageUrl }} style={Styles.image} />
          </View>
          <View style={[Helpers.crossEnd, Metrics.horizontalMargin]}>
            <Text
              style={[
                Fonts.h5,
                Metrics.smallVerticalMargin,
                { textAlign: "left" }
              ]}
            >
              {item.description}
            </Text>
          </View>
          <View
            style={[
              Helpers.rowReverse,
              Helpers.crossCenter,
              Helpers.mainSpaceBetween,
              Metrics.smallHorizontalMargin,
              Metrics.verticalMargin
            ]}
          >
            <View
              style={[
                Helpers.row,
                Helpers.mainEnd,
                Helpers.crossCenter,
                Metrics.horizontalMargin,
                Metrics.verticalMargin
              ]}
            >
              <Text style={[Fonts.subTitle, { fontSize: 14 },Metrics.tinyHorizontalMargin]}>
                {moment(item.createdAt).fromNow()}
              </Text>
              <Image resizeMode={"contain"} resizeMode={"contain"}source={Images.calendar} style={Styles.icon} />
            </View>
            <View />
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  item: state.app.item,
  language: state.app.language
});

export default connect(mapStateToProps, null)(ReusableItemScreen);
