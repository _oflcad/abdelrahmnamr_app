import { StyleSheet } from "react-native";
import { Helpers, Metrics, Fonts, Colors } from "../../Theme";

export default StyleSheet.create({
    image: {
        ...Metrics.verticalMargin,
        width: Metrics.width - 25,
        height: Metrics.width - 25,
        borderRadius: 10,
    }
});
