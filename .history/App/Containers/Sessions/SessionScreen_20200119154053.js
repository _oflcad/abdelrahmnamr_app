import React, { Component } from "react";
import { View, Text, FlatList, SafeAreaView, Image } from "react-native";
import NestedHeader from "../../Components/NestedHeader";
import { connect } from "react-redux";
import { Helpers, Fonts, Metrics, Words, Data, Images } from "../../Theme";
import ReusableCard from "../../Components/ReusableCard/ReusableCard";
import { fetchLatestSessions } from "../../Actions/App/Actions";
class SessionScreen extends Component {
  render() {
    return (
      <SafeAreaView style={[Helpers.fill]}>
        <NestedHeader />
        <Text
          style={[
            Fonts.h4,
            Metrics.mediumHorizontalMargin,
            Metrics.smallVerticalMargin
          ]}
        >
          {Words.en.session.session}
        </Text>
        <FlatList
          showsVerticalScrollIndicator={false}
          style={[Metrics.smallHorizontalMargin]}
          data={Data.Sessions}
          ListEmptyComponent={() => (
            <View style={[Helpers.fill, Helpers.center]}>
            <Image resizeMode={"contain"}source={Images.notFound}  style={[Metrics.verticalMargin,{width: Metrics.width / 2, height: Metrics.width / 2}]}/>
              <Text style={[Fonts.h4]}>{Words.en.news.noData}</Text>
            </View>
          )}
          renderItem={({ item }) => (
            <ReusableCard
              item={item}
              keyExtractor={item => "key" + item.title}
            />
          )}
        />
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  sessions: state.app.sessions
});

const mapDispatchToProps = dispatch => {
  return {
    onDispatchFetchSessions: () => dispatch(fetchLatestSessions())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SessionScreen);
