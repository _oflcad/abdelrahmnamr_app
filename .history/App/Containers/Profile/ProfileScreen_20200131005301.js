import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  SafeAreaView,
  Image,
  ScrollView,
  Linking
} from "react-native";

import { connect } from "react-redux";
import {
  Helpers,
  Fonts,
  Colors,
  Metrics,
  Words,
  Images,
  CountryCodes
} from "../../Theme";
import NestedHeader from "../../Components/NestedHeader";
import Styles from "./Styles";

import { TouchableOpacity } from "react-native-gesture-handler";
//import { Linking } from "expo";
import RNPickerSelect from "react-native-picker-select";
class ProfileScreen extends Component {
  state = {
    username: "Omar Farouk Lakhdhar",
    fullName: "ID:123245747",
    email: "omar@oflcad.me",
    values: CountryCodes,
    country: null,
    profileUrl: null
  };

  _handleResetPassword = () => {
    Linking.openURL("https://abdelrahman-amr.com/resetting/request");
  };

  _handleProfileUpdate = () => {
    console.log("Handle Save Pofile Update");
  };

  componentDidMount() {
    this.handleCountryCode();
  }

  handleCountryCode = () => {
    const { country } = this.props.user;
  };

  render() {
    const { user, language } = this.props;
    let title = language === "ar" ? Words.ar.nestedHeader.Profile : null;
    return (
      <SafeAreaView style={[Helpers.fill, Helpers.column]}>
        <NestedHeader  customTitle={title} />
        <ScrollView bounces contentContainerStyle={{bottoMargin: 60}} >
        <View style={[Helpers.crossCenter]}>
        <View
              style={[
                Helpers.crossEnd,
                Helpers.mainEnd,
                Styles.addPictureContainer
              ]}
            >
              <Image
                source={Images.userShape}
                style={Styles.profileUrl}
              />
          </View>

          <Text
            style={[
              Fonts.buttonText,
              Metrics.smallMarginTop,
              Metrics.smallMarginBottom,
              { color: Colors.primary }
            ]}
          >
            {user.username}
          </Text>
        </View>
        <View style={[Helpers.mainSpaceAround, Metrics.marginTop]}>
          <View style={[language === 'en' ? Helpers.mainStart : Helpers.mainEnd, Metrics.horizontalMargin]}>
            <Text style={[Fonts.label, Metrics.smallMarginTop]}>
              {language === "en" ? Words.en.profile.fullName : Words.ar.profile.fullName}
            </Text>
            <TextInput
              style={[Styles.textInput, language=== "ar" ? {textAlign: "right"} : null]}
              onChange={value => this.setState({ fullName: value })}
              placeholder={user.firstname + " " + user.lastname}
              placeholderTextColor={Colors.grey150}
              underlineColorAndroid={Colors.transparent}
            />
          </View>
          <View style={[language === 'en' ? Helpers.mainStart : Helpers.mainEnd, Metrics.horizontalMargin]}>
            <Text style={[Fonts.label, Metrics.smallMarginTop]}>
              {
                language === "en" ?
                Words.en.profile.email
                :Words.ar.profile.email
              }
            </Text>
            <TextInput
              style={[Styles.textInput, language === "ar" ? { textAlign: "right",}: null ]}
              placeholder={user.email}
              onChange={value => this.setState({ email: value })}
              placeholderTextColor={Colors.grey150}
              underlineColorAndroid={Colors.transparent}
            />
          </View>
          <View style={[language === 'en' ? Helpers.mainStart : Helpers.mainEnd, Metrics.horizontalMargin]}>
            <Text style={[Fonts.label, Metrics.marginTop]}>
              {
                language === "en" ? 
                Words.en.profile.dialCode:
                Words.ar.profile.dialCode
              }
            </Text>
            <View
              style={[
                language === "en" ? Helpers.row : Helpers.rowReverse,
                Helpers.mainSpaceBetween,
                Metrics.smallVerticalMargin
              ]}
            >
              <RNPickerSelect
                placeholder={{ label: "Select a country...", value: null }}
                items={this.state.values}
                onValueChange={val => {
                  this.setState({ country: val });
                }}
                style={Styles.pickerSelectStyles}
                value={this.state.country}
              />
              <TextInput
                style={[Styles.phoneNumberTextInput, language === "ar" ? { textAlign: "right",}: null]}
                placeholder={user.phone}
                keyboardType={"number-pad"}
                returnKeyType="done"
                placeholderTextColor={Colors.grey150}
                underlineColorAndroid={Colors.transparent}
              />
            </View>
            <View style={{height: Metrics.width / 3}} />
            <View style={[language === 'en' ? Helpers.mainStart : Helpers.mainEnd, Metrics.horizontalMargin]}>
             
            </View>
          </View>
        </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  language: state.app.language,
  user: state.auth.user,
  isAuthenticated: state.auth.isAuthenticated
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);
