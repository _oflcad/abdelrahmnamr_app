import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  SafeAreaView,
  ActivityIndicator,
  ScrollView
} from "react-native";
import Styles from "./Styles";
import { connect } from "react-redux";
import { registerUser } from "../../Actions/Auth/Actions";
import {
  Helpers,
  Metrics,
  Words,
  Fonts,
  Colors,
  CountryCodes,
  Images
} from "../../Theme";
import NestedHeader from "../../Components/NestedHeader";
import NavigationService from "../../Services/NavigationService";
import _ from "lodash";
import RNPickerSelect from "react-native-picker-select";

class SignUpcreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      confirmpassword: "",
      name: "",
      lastname: "",
      country: null,
      phone: null,
      error: null,
      isLoading: false,
    };
  }

  _handleEmail = val => {
    this.setState({ email: val });
  };

  _handlePassword = val => {
    this.setState({ password: val });
  };

  _handleSecondPassword = val => {
    this.setState({ confirmpassword: val });
  };

  _handleName = val => {
    this.setState({ name: val });
  };

  _handleLastName = val => {
    this.setState({ lastname: val });
  };

  _handleCountry = val => {
    this.setState({ country: val });
  };

  _handlePhoneNumber = val => {
    this.setState({ phone: val });
  };

  _handleSignIn = () => {
    this.setState({
      email: "",
      password: "",
      confirmpassword: "",
      name: "",
      lastname: "",
      username: "",
      country: null,
      phone: null,
      error: null,
      isLoading: false
    });
    NavigationService.navigate("SignIn");
  };

  _handleRegisterUser = () => {
    const {
      email,
      password,
      confirmpassword,
      name,
      lastname,
      country,
      phone,
      username
    } = this.state;
    const { language } = this.props;
    console.log(this.state);
    this.setState({ isLoading: true, error: null });
    if (
      !_.isEmpty(username) &&
      !_.isEmpty(email) &&
      !_.isEmpty(password) &&
      !_.isEmpty(confirmpassword) &&
      _.isEqual(password, confirmpassword) &&
      !_.isEmpty(name) &&
      !_.isEmpty(lastname) &&
      !_.isEmpty(country) &&
      !_.isEmpty(phone)
    ) {
      setTimeout(() => {
        this.setState({ isLoading: true, error: null });
        this.props.onDispatchRegisterUser(this.state);
      }, 2500);
      NavigationService.navigate('SignIn')
    } else {
      language === "en"
        ? this.setState({ error: Words.en.signUp.error, isLoading: false })
        : this.setState({ error: Words.ar.signUp.error, isLoading: false });
    }
  };
  render() {
    const { isLoading, error } = this.state;
    const { language, confirmEmail } = this.props;
    if (confirmEmail) {
      return (
        <View style={[Helpers.fill, Helpers.center]}>
          <Image
            source={Images.confirmEmail}
            style={{ width: 180, height: 180 }}
          />
          <Text>
            {language === "en"
              ? Words.en.registerSuccess
              : Words.ar.registerSuccess}
          </Text>
        </View>
      );
    }
    return (
      <SafeAreaView style={[Helpers.fill]}>
        <NestedHeader />
        <ScrollView showsHorizontalScrollIndicator={false} bounces>
          <View
            style={[
              language === "ar" ? [Helpers.crossEnd, Metrics.marginTop] : null
            ]}
          >
            <Text
              style={[
                Fonts.h5,
                Metrics.mediumHorizontalMargin,
                Metrics.smallVerticalMargin,
                language === "ar" ? { textAlign: "right" } : null
              ]}
            >
              {language === "en"
                ? Words.en.signUp.welcomeMsg
                : Words.ar.signUp.welcomeMsg}
            </Text>
          </View>

          <View
            style={[
              Helpers.column,
              language === "en" ? Helpers.crossStart : Helpers.crossEnd,
              Metrics.tinyVerticalMargin,
              Metrics.smallHorizontalMargin
            ]}
          >
            <Text style={[Fonts.normal]}>
              {language === "en"
                ? Words.en.signUp.username
                : Words.ar.signUp.username}
            </Text>
            <TextInput
              style={[
                Styles.input,
                language === "ar" ? { textAlign: "right" } : null
              ]}
              onChangeText={val => this.setState({ username: val })}
              autoCapitalize={"none"}
              placeholder={
                language === "en"
                  ? Words.en.signUp.username
                  : Words.ar.signUp.username
              }
              placeholderTextColor={Colors.grey150}
              underlineColorAndroid={Colors.transparent}
            />
          </View>

          <View
            style={[
              Helpers.column,
              language === "en" ? Helpers.crossStart : Helpers.crossEnd,
              Metrics.tinyVerticalMargin,
              Metrics.smallHorizontalMargin
            ]}
          >
            <Text style={[Fonts.normal]}>
              {language === "en"
                ? Words.en.signUp.email
                : Words.ar.signUp.email}
            </Text>
            <TextInput
              style={[
                Styles.input,
                language === "ar" ? { textAlign: "right" } : null
              ]}
              onChangeText={val => this.setState({ email: val })}
              autoCapitalize={"none"}
              placeholder={
                language === "en"
                  ? Words.en.signUp.email
                  : Words.ar.signUp.email
              }
              placeholderTextColor={Colors.grey150}
              underlineColorAndroid={Colors.transparent}
            />
          </View>
          <View
            style={[
              Helpers.column,
              language === "en" ? Helpers.crossStart : Helpers.crossEnd,
              Metrics.tinyVerticalMargin,
              Metrics.smallHorizontalMargin
            ]}
          >
            <Text style={[Fonts.normal]}>
              {language === "en" ? Words.en.signUp.name : Words.ar.signUp.name}
            </Text>
            <TextInput
              style={[
                Styles.input,
                language === "ar" ? { textAlign: "right" } : null
              ]}
              onChangeText={val => this.setState({ name: val })}
              autoCapitalize={"none"}
              placeholder={
                language === "en" ? Words.en.signUp.name : Words.ar.signUp.name
              }
              placeholderTextColor={Colors.grey150}
              underlineColorAndroid={Colors.transparent}
            />
          </View>
          <View
            style={[
              Helpers.column,
              language === "en" ? Helpers.crossStart : Helpers.crossEnd,
              Metrics.tinyVerticalMargin,
              Metrics.smallHorizontalMargin
            ]}
          >
            <Text style={[Fonts.normal]}>
              {language === "en"
                ? Words.en.signUp.lastName
                : Words.ar.signUp.lastName}
            </Text>
            <TextInput
              style={[
                Styles.input,
                language === "ar" ? { textAlign: "right" } : null
              ]}
              onChangeText={val => this.setState({ lastname: val })}
              autoCapitalize={"none"}
              placeholder={
                language === "en" ? Words.en.signUp.name : Words.ar.signUp.name
              }
              placeholderTextColor={Colors.grey150}
              underlineColorAndroid={Colors.transparent}
            />
          </View>
          <View
            style={[
              Helpers.column,
              language === "en" ? Helpers.crossStart : Helpers.crossEnd,
              ,
              Metrics.verticalMargin,
              Metrics.smallHorizontalMargin
            ]}
          >
            <Text style={[Fonts.h5]}>
              {language === "en"
                ? Words.en.signUp.password
                : Words.ar.signUp.password}
            </Text>
            <TextInput
              style={[
                Styles.input,
                language === "ar" ? { textAlign: "right" } : null
              ]}
              secureTextEntry
              autoCapitalize={"none"}
              onChangeText={val => this.setState({ password: val })}
              placeholder={
                language === "en"
                  ? Words.en.signIn.password
                  : Words.ar.signIn.password
              }
              placeholderTextColor={Colors.grey150}
              underlineColorAndroid={Colors.transparent}
            />
          </View>
          <View
            style={[
              Helpers.column,
              language === "en" ? Helpers.crossStart : Helpers.crossEnd,
              ,
              Metrics.verticalMargin,
              Metrics.smallHorizontalMargin
            ]}
          >
            <Text style={[Fonts.h5]}>
              {language === "en"
                ? Words.en.signUp.password
                : Words.ar.signUp.password}
            </Text>
            <TextInput
              style={[
                Styles.input,
                language === "ar" ? { textAlign: "right" } : null
              ]}
              secureTextEntry
              autoCapitalize={"none"}
              onChangeText={val => this.setState({ confirmpassword: val })}
              placeholder={
                language === "en"
                  ? Words.en.signUp.password
                  : Words.ar.signUp.password
              }
              placeholderTextColor={Colors.grey150}
              underlineColorAndroid={Colors.transparent}
            />
          </View>
          <View
            style={[
              Helpers.column,
              language === "en" ? Helpers.crossStart : Helpers.crossEnd,
              ,
              Metrics.verticalMargin,
              Metrics.smallHorizontalMargin
            ]}
          >
            <Text style={[Fonts.h5]}>
              {language === "en"
                ? Words.en.signUp.country
                : Words.ar.signUp.country}
            </Text>
            <RNPickerSelect
              placeholder={{ label: "Select a country...", value: null }}
              items={CountryCodes}
              onValueChange={val => {
                this.setState({ country: val });
              }}
              style={Styles.pickerSelectStyles}
              value={this.state.country}
            />
          </View>
          <View
            style={[
              Helpers.column,
              language === "en" ? Helpers.crossStart : Helpers.crossEnd,
              ,
              Metrics.verticalMargin,
              Metrics.smallHorizontalMargin
            ]}
          >
            <Text style={[Fonts.h5]}>
              {language === "en"
                ? Words.en.signUp.phone
                : Words.ar.signUp.phone}
            </Text>
            <TextInput
              style={[
                Styles.input,
                language === "ar" ? { textAlign: "right" } : null
              ]}
              keyboardType={"phone-pad"}
              returnKeyType="done"
              onChangeText={val => this.setState({ phone: val })}
              placeholder={
                language === "en"
                  ? Words.en.signUp.phone
                  : Words.ar.signUp.phone
              }
              placeholderTextColor={Colors.grey150}
              underlineColorAndroid={Colors.transparent}
            />
          </View>
          {error && (
            <View style={[Helpers.center, Metrics.verticalMargin]}>
              <Text style={Styles.errorText}>{error}</Text>
            </View>
          )}
          <View style={[Helpers.center, Metrics.verticalMargin]}>
            <TouchableOpacity
              style={Styles.buttonContainer}
              onPress={this._handleRegisterUser}
            >
              {isLoading ? (
                <ActivityIndicator size={"small"} color={Colors.white} />
              ) : (
                <Text style={[Fonts.normal, { color: Colors.white }]}>
                  {language === "en"
                    ? Words.en.signUp.buttonText
                    : Words.ar.signUp.buttonText}
                </Text>
              )}
            </TouchableOpacity>
          </View>

          <View style={[Helpers.center, Metrics.verticalMargin]}>
            <TouchableOpacity onPress={this._handleSignIn}>
              <Text
                style={[
                  Fonts.normal,
                  { color: Colors.grey200, textDecorationLine: "underline" }
                ]}
              >
                {language === "en"
                  ? Words.en.signIn.buttonText
                  : Words.ar.signIn.buttonText}
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{ height: 150 }} />
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  language: state.app.language,
  isLoading: state.auth.isLoading,
  confirmEmail: state.auth.confirmEmail
});

const mapDispatchToProps = dispatch => {
  return {
    onDispatchRegisterUser: user => dispatch(registerUser(user))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUpcreen);
