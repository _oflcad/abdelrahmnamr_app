import React from "react";
import { Text, View, TouchableOpacity, FlatList, Image } from "react-native";
import { connect } from "react-redux";
import { setFirstTimer, calculateTotal } from "../../Actions/App/Actions";
import { Fonts, Helpers, Images, Metrics, Words, Colors } from "../../Theme";
import NestedHeader from "../../Components/NestedHeader";
import Styles from "./Styles";
import BasketItemContainer from "../../Components/BasketItemContainer/BasketItemContainer";
import * as ImagePicker from "expo-image-picker";
import {Permissions, Constants } from 'react-native-unimodules'

class BasketScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      paymentReceit: null,
      error: ""
    };
  }

  componentDidMount() {
    this.getPermissionAsync();
    this._handleTotalPrice();
  }

  componentDidUpdate(prevProps) {
    if ((prevProps.basket !== this.props.basket) && (prevProps.total !== this.props.total)) {
      this._handleTotalPrice();
      return true;
    }
  }

  _handlePayment = () => {
    const { isAuthenticated, language } = this.props;
    if (isAuthenticated) {
      if (this.state.paymentReceit) {
        //TO DO DISPATCH BUY ACTION
      } else {
        let msg =
          language === "en" ? Words.en.checkOut.msg : Words.ar.checkOut.msg;
        this.setState({ error: msg });
      }
    } else {
      this.props.navigation.navigate("SignIn");
    }
  };

  _handleImagePicker = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1
    });

    console.log(result);

    if (!result.cancelled) {
      this.setState({ paymentReceit: result.uri, error: null });
    }
  };
  _renderItem = ({ item }) => {
    return <BasketItemContainer item={item} />;
  };

  getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== "granted") {
        alert("Sorry, we need camera roll permissions to make this work!");
      }
    }
  };

  _handleTotalPrice = () => {
    this.props.onDipsatchCalculateTotal();
  };

  render() {
    const { basket, language, total } = this.props;
    const { paymentReceit, error } = this.state;
    let title = language === "ar" ? Words.ar.nestedHeader.News : null;
    return (
      <View style={[Helpers.fill]}>
        <NestedHeader customTitle={title} />
        {basket.length > 0 ? (
          <View style={[Helpers.crossCenter, { flex: 2 }]}>
            <FlatList
              data={basket}
              showsVerticalScrollIndicator={false}
              contentContainerStyle={[
                Helpers.mainCenter,
                Metrics.tinyVerticalMargin,
                { paddingBottom: 60 }
              ]}
              contentInset={{ top: 0, bottom: 20, left: 0, right: 0 }}
              contentInsetAdjustmentBehavior="automatic"
              renderItem={this._renderItem}
              keyExtractor={(item, index) => "key" + item.name}
            />
            <View
              style={[
                Helpers.center,
                Styles.imagePickerContainer,
                Metrics.horizontalMargin,
                Metrics.verticalMargin
              ]}
            >
              {paymentReceit ? (
                <Image
                  source={{ uri: paymentReceit }}
                  style={{ width: 150, height: 150, borderRadius: 10 }}
                />
              ) : (
                <TouchableOpacity onPress={this._handleImagePicker}>
                  <Image
                    source={Images.addPhoto}
                    style={Styles.icon}
                  />
                </TouchableOpacity>
              )}
              <View style={[Helpers.center, Helpers.verticalMargin]}>
                {error ? <Text style={[Fonts.error]}>{error}</Text> : null}
              </View>
            </View>
          </View>
        ) : (
          <View style={[Helpers.mainEnd, Helpers.crossCenter, Metrics.verticalMargin, { flex: 1 }]}>
            <Image
              style={Styles.animationLottie}
              source={Images.cart}
            />
            <Text style={[Fonts.h5]}>
              {language === "en" ? Words.en.emptyBasket : Words.ar.emptyBasket}
            </Text>
          </View>
        )}

        <View
          style={[
            Styles.footer,
            Helpers.row,
            Metrics.verticalMargin,
            Metrics.horizontalMargin,
            Helpers.mainSpaceAround,
          ]}
        >
          <View style={Styles.textWrapper}>
            <Text style={[Fonts.h4, Fonts.center]}>Total : </Text>
            <Text style={[Fonts.h4, Fonts.center]}>{total} $</Text>
          </View>
          <TouchableOpacity
            onPress={this._handlePayment}
            style={Styles.buttonContainer}
          >
            <Text style={[Fonts.normal, { color: Colors.white }]}>
              Check Out
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    basket: state.app.basket,
    total: state.app.total,
    language: state.app.language,
    isAuthenticated: state.auth.isAuthenticated,
  };
};

const mapDispatchToProps = dispatch => ({
  setFirstTimer: val => dispatch(setFirstTimer(val)),
  onDipsatchCalculateTotal: () => dispatch(calculateTotal())
});

export default connect(mapStateToProps, mapDispatchToProps)(BasketScreen);
