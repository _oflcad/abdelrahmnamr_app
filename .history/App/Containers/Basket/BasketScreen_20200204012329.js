import React from "react";
import { Text, View, TouchableOpacity, FlatList, Image, ActivityIndicator, Modal } from "react-native";
import { connect } from "react-redux";
import {
  buyProduct,
  calculateTotal,
  setImagePath
} from "../../Actions/App/Actions";
import { Fonts, Helpers, Images, Metrics, Words, Colors } from "../../Theme";
import NestedHeader from "../../Components/NestedHeader";
import Styles from "./Styles";
import BasketItemContainer from "../../Components/BasketItemContainer/BasketItemContainer";
import * as ImagePicker from "expo-image-picker";
import { Permissions, Constants } from "react-native-unimodules";
import reactotron from "reactotron-react-native";
import NavigationService from "../../Services/NavigationService";
import Toast from "react-native-easy-toast";
class BasketScreen extends React.Component {
  constructor(props) {
    super(props);
    this.formdata = new FormData();
    this.state = {
      paymentReceit: null,
      error: "",
      loading: false,
    };
  }

  componentDidMount() {
    this.getPermissionAsync();
    this._handleTotalPrice();
  }

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.imageUri && prevProps.imageUri !== this.props.imageUri){
      this.props.language === "en" ? 
        this.refs.toast.show(Words.en.purshased)
      :
      this.refs.toast.show(Words.ar.purshased);
      return true;
    }
    if (
      prevProps.basket.length !== this.props.basket.length &&
      prevProps.total !== this.props.total
    ) {
      this._handleTotalPrice();
      this.setState({loading: false})
      return true;
    }
  }

  _handlePayment = () => {
    const { isAuthenticated, language, token } = this.props;
    if (isAuthenticated) {
      this.setState({loading: true})
      if (this.state.paymentReceit) {
        //TO DO DISPATCH BUY ACTION
        this.props.onDispatchBuyProduct();
      } else {
        let msg =
          language === "en" ? Words.en.checkOut.msg : Words.ar.checkOut.msg;
        this.setState({ error: msg, loading:false });
      }
    } else {
      this.props.navigation.navigate("SignIn");
    }
  };

  _handleImagePicker = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: false,
      aspect: [4, 3],
      quality: 1
    });

    reactotron.log("IMAGE RESULT", result);
    //Dispatch Image URI function
    this.props.onDispatchSetImagePath(result.uri);
    if (!result.cancelled) {
      this.setState({ paymentReceit: result.uri, error: null });
    }
  };
  _renderItem = ({ item }) => {
    return <BasketItemContainer item={item} />;
  };

  getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== "granted") {
        alert("Sorry, we need camera roll permissions to make this work!");
      }
    }
  };

  _handleTotalPrice = () => {
    this.props.onDipsatchCalculateTotal();
  };

  render() {
    const { basket, language, total } = this.props;
    const { paymentReceit, error, loading } = this.state;
    let title = language === "ar" ? Words.ar.nestedHeader.Basket : null;
    return (
      <View style={[Helpers.fill]}>
        <NestedHeader customTitle={title} />
        {basket.length > 0 ? (
          <View style={[Helpers.crossCenter, { flex: 2 }]}>
            <FlatList
              data={basket}
              showsVerticalScrollIndicator={false}
              contentContainerStyle={[
                Helpers.mainCenter,
                Metrics.tinyVerticalMargin,
                { paddingBottom: 60 }
              ]}
              contentInset={{ top: 0, bottom: 20, left: 0, right: 0 }}
              contentInsetAdjustmentBehavior="automatic"
              renderItem={this._renderItem}
              keyExtractor={(item, index) => "key" + item.name}
            />
            <View
              style={[
                Helpers.center,
                Styles.imagePickerContainer,
                Metrics.horizontalMargin,
                Metrics.verticalMargin
              ]}
            >
              {paymentReceit ? (
                <View style={{ width: 200, height: 200, ...Helpers.center }}>
                  <Image
                    source={{ uri: paymentReceit }}
                    style={{ width: 150, height: 150, borderRadius: 10 }}
                  />
                  <View
                    style={{
                      position: "absolute",
                      bottom: 5,
                      left: 0,
                      right: 0,
                      ...Helpers.center
                    }}
                  >
                    <TouchableOpacity onPress={this._handleImagePicker}>
                      <Image
                        source={Images.addPhoto}
                        style={Styles.smallIcon}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              ) : (
                <TouchableOpacity onPress={this._handleImagePicker}>
                  <Image source={Images.addPhoto} style={Styles.icon} />
                </TouchableOpacity>
              )}
              <View style={[Helpers.center, Helpers.verticalMargin]}>
                {error ? <Text style={[Fonts.error]}>{error}</Text> : null}
              </View>
            </View>
          </View>
        ) : (
          <View
            style={[
              Helpers.mainEnd,
              Helpers.crossCenter,
              Metrics.verticalMargin,
              { flex: 1 }
            ]}
          >
            <Image style={Styles.animationLottie} source={Images.cart} />
            <Text style={[Fonts.h5, { textAlign: "center" }, Metrics.verticalMargin]}>
              {language === "en" ? Words.en.emptyBasket : Words.ar.emptyBasket}
            </Text>
          </View>
        )}

        <View
          style={[
            Styles.footer,
            Helpers.row,
            Metrics.verticalMargin,
            Metrics.horizontalMargin,
            Helpers.mainSpaceAround
          ]}
        >
          <View style={Styles.textWrapper}>
            <Text style={[Fonts.h4, Fonts.center]}>Total : </Text>
            <Text style={[Fonts.h4, Fonts.center]}>{total} $</Text>
          </View>
         {
           loading ? <ActivityIndicator color={Colors.primary} size={"large"} /> 
           :
           <TouchableOpacity
            onPress={this._handlePayment}
            style={Styles.buttonContainer}
          >
            <Text style={[Fonts.normal, { color: Colors.white }]}>
              Check Out
            </Text>
          </TouchableOpacity>
         }
        </View>
        <Toast 
        style={{backgroundColor: Colors.success }}
          ref="toast"
          position='center'
          fadeOutDuration={3000}
          opacity={1}
          textStyle={{color:Colors.white, fontSize: 30}}
           />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    basket: state.app.basket,
    total: state.app.total,
    language: state.auth.language,
    isAuthenticated: state.auth.isAuthenticated,
    showBoughtModal: state.app.showBoughtModal,
    imageUri: state.app.imageUri
  };
};

const mapDispatchToProps = dispatch => ({
  onDipsatchCalculateTotal: () => dispatch(calculateTotal()),
  onDispatchBuyProduct: () => dispatch(buyProduct()),
  onDispatchSetImagePath: val => dispatch(setImagePath(val))
});

export default connect(mapStateToProps, mapDispatchToProps)(BasketScreen);
