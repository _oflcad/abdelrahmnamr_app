import { StyleSheet } from "react-native";
import { Helpers, Metrics, Fonts, Colors } from "../../Theme";

export default StyleSheet.create({
  buttonContainer: {
    ...Helpers.center,
    backgroundColor: Colors.primary,
    height: 40,
    width: 100,
    borderRadius: 10
  },
  buttonWrapper: {
    bottom: 100,
    left: 0,
    position: "absolute",
    right: 0
  },
  error: {
    ...Fonts.normal,
    color: Colors.error,
    marginBottom: Metrics.tiny,
    textAlign: "center"
  },
  logoContainer: {
    ...Helpers.fullWidth,
    height: 300,
    marginBottom: 25
  },
  result: {
    ...Fonts.normal,
    marginBottom: Metrics.tiny,
    textAlign: "center"
  },
  text: {
    ...Fonts.normal,
    marginBottom: Metrics.tiny,
    textAlign: "center"
  },
  animationLottie: {
    backgroundColor: Colors.white,
    width: Metrics.width / 2,
    height: Metrics.width / 2
  },
  imagePickerContainer: {
    ...Helpers.center,
    width: Metrics.width,
  },
  footer: {
    ...Helpers.crossEnd,
    ...Helpers.row,
    ...Helpers.mainSpaceAround,
    marginBottom: 60,
    flex:1,

  },
  textWrapper: {
    ...Helpers.row,
    ...Helpers.center,
    width: Metrics.width / 2 ,
    height: 40,
    borderRadius: 10,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: Colors.black    
  },
  icon: {
    width: 50,
    height: 50,
  }
});
