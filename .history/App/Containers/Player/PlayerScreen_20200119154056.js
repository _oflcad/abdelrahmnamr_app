import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Slider,
  ScrollView,
  NativeEventEmitter,
  DeviceEventEmitter
} from "react-native";
import { connect } from "react-redux";
import { Helpers, Metrics, Fonts, Colors, Images } from "../../Theme";

import Styles from "./Styles";
import PlayerHeader from "../../Components/PlayerHeader/PlayerHeader";
class PlayerScreen extends Component {
  constructor(props) {
    super(props);
    this.eventEmitter = new NativeEventEmitter();
    this._handleNext = this._handleNext.bind(this);
    this._handlePlayPause = this._handlePlayPause.bind(this);
    this._handlePrevious = this._handlePrevious.bind(this);
    this.state = {
      time: "00:00"
    };
  }

  componentDidMount() {
    this.listenerT = DeviceEventEmitter.addListener("MILLIS", data => {});
  }

  _handlePlayPause = () => {
    this.eventEmitter.emit("PLAYPAUSE", "it fucking works!!!");
  };

  _handleNext = () => {
    this.eventEmitter.emit("NEXTSONG", "it fucking works!!!");
  };

  _handlePrevious = () => {
    this.eventEmitter.emit("PREVIOUSSONG", "it fucking works!!!");
  };

  _handleStop = () => {
    this.eventEmitter.emit("STOPSONG", "it is working");
  };

  render() {
    const { isPlaying, songToPlay, audio } = this.props;
    const { time } = this.state;
    return (
      <View style={[Helpers.fill, Helpers.column]}>
        <PlayerHeader />
        <ScrollView showsVerticalScrollIndicator={false} bounces>
          <View style={[Helpers.center]}>
            <Image
              source={{ uri: songToPlay.image }}
              style={[
                Metrics.verticalMargin,
                Metrics.horizontalMargin,
                {
                  width: Metrics.width - 100,
                  height: Metrics.width - 100,
                  borderRadius: 15
                }
              ]}
            />
          </View>
          <View style={Styles.timeContainer}>
            {isPlaying && <Text style={Styles.time}>{time}</Text>}
          </View>
          <View style={Styles.textPlayerContainer}>
            <Text style={[Styles.title, { fontSize: 24 }]}>
              {audio ? audio.title : songToPlay.name}
            </Text>
            <Text style={[Styles.author, { fontSize: 24 }]}>
              عبد الرحمن عمرو
            </Text>
          </View>
          <View style={Styles.sliderContainer}>
            <Slider
              style={Styles.slider}
              step={1}
              minimumValue={0}
              maximumValue={532}
              value={140}
            />
          </View>

          <View style={Styles.songIconsPlayerContainer}>
            <TouchableOpacity onPress={() => this._handlePrevious()}>
              <Image resizeMode={"contain"}source={Images.rewind} style={Styles.sidesIcon} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this._handlePlayPause()}>
              {isPlaying ? (
                <Image resizeMode={"contain"}source={Images.pause} style={Styles.MidIcon} />
              ) : (
                <Image resizeMode={"contain"}source={Images.play} style={Styles.MidIcon} />
              )}
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this._handleStop()}>
              <Image resizeMode={"contain"}source={Images.stop} style={Styles.sidesIcon} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this._handleNext()}>
              <Image resizeMode={"contain"}source={Images.forward} style={Styles.sidesIcon} />
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  isPlaying: state.media.isPlaying,
  audio: state.media.audio,
  songToPlay: state.media.songToPlay
});

const mapDispatchToProps = dispatch => {
  return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(PlayerScreen);
