import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
  ActivityIndicator
} from "react-native";
import NavigationService from "../../Services/NavigationService";
import AppNavigator from "../../Navigators/AppNavigator";
import { connect } from "react-redux";
import { Helpers, Images, Metrics, Fonts, Words, Colors } from "../../Theme";
import Style from "./Styles";
import { setFirstTimer } from "../../Actions/App/Actions";
import { fetchAllAudios, hideMiniPlayer } from "../../Actions/Media/Actions";
import Player from "../../Components/Player/Player";

class RootScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      
    };
  }

  componentDidMount() {
    const { isFirstTimer } = this.props.app;
    this.props.onDispatchFetchAudios();
    if (!isFirstTimer) {
        this.props.navigation.navigate("Splash");
    }
  }

  componentDidUpdate(prevProps){
    if(prevProps.isFirstTimer !== this.props.isFirstTimer) {
      this.setState({isLoading: true})
      return true;
    }
  }


  _setLanguage = val => {
    this.setState({ isLoading: true });
    this.props.navigation.navigate("Splash");
  };

  render() {
    const { isFirstTimer } = this.props.app;
    const { isLoading } = this.state;
    if (isFirstTimer) {
      return (
        <View style={Helpers.fill}>
          <ImageBackground
            source={Images.firstTimerBg}
            style={[Helpers.fullSize]}
          >
            {isLoading || !isFirstTimer? (
              <View
                style={[
                  Style.buttonWrapper,
                  Metrics.mediumHorizontalMargin,
                  Metrics.mediumVerticalMargin,
                  Helpers.center,
                  { backgroundColor: Colors.transparent }
                ]}
              >
                <ActivityIndicator size={"small"} color={Colors.primary} />
              </View>
            ) : (
              <View
                style={[
                  Style.buttonWrapper,
                  Helpers.mainCenter,
                  Helpers.mainSpaceBetween,
                  Helpers.row,
                  Metrics.mediumHorizontalMargin
                ]}
              >
                <TouchableOpacity
                  onPress={() => this._setLanguage("en")}
                  style={[Style.buttonContainer, Helpers.center]}
                >
                  <Text style={Fonts.normal}>{Words.english}</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this._setLanguage("ar")}
                  style={[Style.buttonContainer, Helpers.center]}
                >
                  <Text style={Fonts.normal}>{Words.arabic}</Text>
                </TouchableOpacity>
              </View>
            )}
          </ImageBackground>
        </View>
      );
    }
    return (
      <View style={Helpers.fill}>
        <AppNavigator
          // Initialize the NavigationService (see https://reactnavigation.org/docs/en/navigating-without-navigation-prop.html)
          ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
        <Player />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  app: state.app
});

const mapDispatchToProps = dispatch => {
  return {
    setFirstTimer: val => dispatch(setFirstTimer(val)),
    onDispatchFetchAudios: () => dispatch(fetchAllAudios()),
    onDispatchHideMiniPlayer: () => dispatch(hideMiniPlayer())
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(RootScreen);
