import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
  ActivityIndicator
} from "react-native";
import NavigationService from "../../Services/NavigationService";
import AppNavigator from "../../Navigators/AppNavigator";
import { connect } from "react-redux";
import { Helpers, Images, Metrics, Fonts, Words, Colors } from "../../Theme";

import { setFirstTimer } from "../../Actions/App/Actions";
import { fetchAllAudios, hideMiniPlayer } from "../../Actions/Media/Actions";
import Player from "../../Components/Player/Player";
import PlayerDownload from "../../Components/PlayerDownload/PlayerDownload";
class RootScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false
    };
  }

  componentDidMount() {
    this.props.onDispatchFetchAudios();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isFirstTimer !== this.props.isFirstTimer) {
      this.setState({ isLoading: true });
      return true;
    }
  }

  _setLanguage = val => {
    this.setState({ isLoading: true });
  };

  render() {
    return (
      <View style={Helpers.fill}>
        <AppNavigator
          // Initialize the NavigationService (see https://reactnavigation.org/docs/en/navigating-without-navigation-prop.html)
          ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
        <Player />
      
      </View>
    );
  }
}

const mapStateToProps = state => ({
  app: state.app
});

const mapDispatchToProps = dispatch => {
  return {
    setFirstTimer: val => dispatch(setFirstTimer(val)),
    onDispatchFetchAudios: () => dispatch(fetchAllAudios()),
    onDispatchHideMiniPlayer: () => dispatch(hideMiniPlayer())
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(RootScreen);
