import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  SafeAreaView,
  ActivityIndicator,
  ScrollView,
  Linking
} from "react-native";

import { connect } from "react-redux";
import NestedHeader from "../../Components/NestedHeader";
import { Helpers, Fonts, Metrics, Words, Colors } from "../../Theme";
import Styles from "./Styles";
import {
  authenticateUser,
  getUserInformation
} from "../../Actions/Auth/Actions";
import { getUserAudios } from "../../Actions/Media/Actions";
import NavigationService from "../../Services/NavigationService";
import Toast from "react-native-easy-toast";
import ApiService from "../../Services/ApiService";
class SignIncreen extends Component {
  state = {
    email: "",
    password: "",
    error: null,
    isLoading: false
  };

  _handleEmail = email => {
    this.setState({ email });
  };

  _handlePassword = password => {
    this.setState({ password });
  };

  _handleAuthentication = async () => {
    const { email, password } = this.state;
    const { language } = this.props;
    if (email && email !== "" && password && password !== "") {
      this.setState({ error: null, isLoading: true });
      this.props.onDispatchAuthenticateUser(email, password);
      setTimeout(() => {
        this.props.onDispatchFetchUserAudio();
      }, 1000);
      setTimeout(() => {
        let msg =
          language === "en"
            ? Words.en.signIn.welcomeMsg
            : Words.ar.signIn.welcomeMsg;
        this.refs.toast.show(msg, 1500, () => {
          NavigationService.navigate("Profile");
        });
      }, 2500);
    } else {
      this.setState({
        isLoading: false,
        error:
          this.props.language === "en"
            ? Words.en.auth.authErrorMsg
            : Words.ar.auth.authErrorMsg
      });
    }
  };

  _handleReset = () => {
    Linking.openURL(ApiService.resetPassword);
  };

  _handleSignUp = () => {
    this.setState({ error: null, isLoading: false, email: "", password: "" });
    NavigationService.navigate("SignUp");
  };

  render() {
    const { isLoading } = this.state;
    const { language } = this.props;
    return (
      <SafeAreaView style={[Helpers.fill]}>
        <NestedHeader />
        <ScrollView bounces showsHorizontalScrollIndicator={false}>
          <View
            style={[
              language === "ar" ? [Helpers.crossEnd, Metrics.marginTop] : null
            ]}
          >
            <Text
              style={[
                Fonts.h3,
                Metrics.mediumHorizontalMargin,
                Metrics.smallVerticalMargin,
                language === "ar" ? { textAlign: "right" } : null
              ]}
            >
              {language === "en"
                ? Words.en.signIn.welcomeMsg
                : Words.ar.signIn.welcomeMsg}
            </Text>
          </View>
          <View style={{ height: 30 }} />
          <View
            style={[
              Helpers.column,
              language === "en" ? Helpers.crossStart : Helpers.crossEnd,
              Metrics.verticalMargin,
              Metrics.horizontalMargin
            ]}
          >
            <Text style={[Fonts.h5]}>
              {language === "en"
                ? Words.en.signIn.email
                : Words.ar.signIn.email}
            </Text>
            <TextInput
              style={[
                Styles.input,
                language === "ar" ? { textAlign: "right" } : null
              ]}
              onChangeText={val => this._handleEmail(val)}
              autoCapitalize={"none"}
              placeholder={
                language === "en"
                  ? Words.en.signIn.email
                  : Words.ar.signIn.email
              }
              placeholderTextColor={Colors.grey150}
              underlineColorAndroid={Colors.transparent}
            />
          </View>
          <View
            style={[
              Helpers.column,
              language === "en" ? Helpers.crossStart : Helpers.crossEnd,
              ,
              Metrics.verticalMargin,
              Metrics.horizontalMargin
            ]}
          >
            <Text style={[Fonts.h5]}>
              {language === "en"
                ? Words.en.signIn.password
                : Words.ar.signIn.password}
            </Text>
            <TextInput
              style={[
                Styles.input,
                language === "ar" ? { textAlign: "right" } : null
              ]}
              secureTextEntry
              onChangeText={val => this._handlePassword(val)}
              placeholder={
                language === "en"
                  ? Words.en.signIn.password
                  : Words.ar.signIn.password
              }
              placeholderTextColor={Colors.grey150}
              underlineColorAndroid={Colors.transparent}
            />
          </View>
          <View
            style={[
              Metrics.verticalMargin,
              Metrics.horizontalMargin,
              language === "en" ? Helpers.crossEnd : null
            ]}
          >
            <TouchableOpacity onPress={this._handleReset}>
              <Text style={[Fonts.subTitle]}>
                {language === "en"
                  ? Words.en.auth.forgotPassword
                  : Words.ar.auth.forgotPassword}
              </Text>
            </TouchableOpacity>
          </View>
          <View style={[Helpers.center, Metrics.verticalMargin]}>
            <TouchableOpacity
              style={Styles.buttonContainer}
              onPress={this._handleAuthentication}
            >
              {isLoading ? (
                <ActivityIndicator size={"small"} color={Colors.white} />
              ) : (
                <Text
                  style={[
                    language === "en" ? Fonts.h3 : Fonts.normal,
                    { color: Colors.white }
                  ]}
                >
                  {language === "en"
                    ? Words.en.signIn.buttonText
                    : Words.ar.signIn.buttonText}
                </Text>
              )}
            </TouchableOpacity>
          </View>
          <View style={[Helpers.center, Metrics.verticalMargin]}>
            <TouchableOpacity onPress={this._handleSignUp}>
              <Text
                style={[
                  Fonts.normal,
                  { color: Colors.grey200, textDecorationLine: "underline" }
                ]}
              >
                {language === "en"
                  ? Words.en.signUp.buttonText
                  : Words.ar.signUp.buttonText}
              </Text>
            </TouchableOpacity>
          </View>

          <View style={{ height: 150 }} />
        </ScrollView>
        <Toast ref="toast" />
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  language: state.app.language,
  isLoading: state.auth.isLoading,
  token: state.auth.token,
  user: state.auth.user
});

const mapDispatchToProps = dispatch => {
  return {
    onDispatchAuthenticateUser: (email, password) =>
      dispatch(authenticateUser(email, password)),
    onDispatchGetUserInfo: () => dispatch(getUserInformation()),
    onDispatchFetchUserAudio: () => dispatch(getUserAudios())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignIncreen);
