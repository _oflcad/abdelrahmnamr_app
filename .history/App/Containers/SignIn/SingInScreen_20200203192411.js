import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  SafeAreaView,
  ActivityIndicator,
  ScrollView,
  Linking
} from "react-native";

import { connect } from "react-redux";
import NestedHeader from "../../Components/NestedHeader";
import { Helpers, Fonts, Metrics, Words, Colors } from "../../Theme";
import Styles from "./Styles";
import {
  authenticateUser,
  getUserInformation
} from "../../Actions/Auth/Actions";
import { getUserAudios } from "../../Actions/Media/Actions";
import NavigationService from "../../Services/NavigationService";
import Toast from "react-native-easy-toast";
import ApiService from "../../Services/ApiService";
import _ from "lodash";
const AUTH_ERROR_MSG = "Request failed with status code 401";
class SignIncreen extends Component {
  state = {
    email: "",
    password: "",
    error: null,
    isLoading: false
  };


  componentDidUpdate(prevProps){
    if(prevProps.isAuthenticated !== this.props.isAuthenticated) {
      this.setState({ error: null, isLoading: true });
      setTimeout(() => {
        NavigationService.navigate("Profile");
      }, 2500);
    }
  }

  _handleEmail = email => {
    this.setState({ email: email, error: null });
  };

  _handlePassword = password => {
    this.setState({ password: password, error: null });
  };

  _handleAuthentication = () => {
    const { email, password, error } = this.state;
    const { language, authError, isAuthenticated } = this.props;
    if(email && email !== "" && password && password !== ""){
      this.props.onDispatchAuthenticateUser(email, password);
      this.setState({isLoading: true });
        this.props.onDispatchFetchUserAudio();
    } else if(authError) {
      setTimeout(() => {
        this.setState({
          isLoading: false,
          error:
            language === "en"
              ? Words.en.auth.authErrorMsg
              : Words.ar.auth.authErrorMsg
        });
      }, 1500);
    }
  };

  _handleReset = () => {
    Linking.openURL(ApiService.resetPassword);
  };

  _handleSignUp = () => {
    this.setState({ error: null, isLoading: false, email: "", password: "" });
    NavigationService.navigate("SignUp");
  };

  render() {
    const { isLoading, error } = this.state;
    const { language } = this.props;
    return (
      <SafeAreaView style={[Helpers.fill]}>
        <NestedHeader />
        <ScrollView bounces showsHorizontalScrollIndicator={false}>
          <View
            style={[
              language === "ar" ? [Helpers.crossEnd, Metrics.marginTop] : null
            ]}
          >
            <Text
              style={[
                Fonts.h3,
                Metrics.mediumHorizontalMargin,
                Metrics.smallVerticalMargin,
                language === "ar" ? { textAlign: "right" } : null
              ]}
            >
              {language === "en"
                ? Words.en.signIn.welcomeMsg
                : Words.ar.signIn.welcomeMsg}
            </Text>
          </View>
          <View style={{ height: 30 }} />
          <View
            style={[
              Helpers.column,
              language === "en" ? Helpers.crossStart : Helpers.crossEnd,
              Metrics.verticalMargin,
              Metrics.horizontalMargin
            ]}
          >
            <Text style={[Fonts.h5]}>
              {language === "en"
                ? Words.en.signIn.email
                : Words.ar.signIn.email}
            </Text>
            <TextInput
              returnKeyType="next"
              style={[
                Styles.input,
                language === "ar" ? { textAlign: "right" } : null
              ]}
              onChangeText={val => this._handleEmail(val)}
              autoCapitalize={"none"}
              placeholder={
                language === "en"
                  ? Words.en.signIn.email
                  : Words.ar.signIn.email
              }
              placeholderTextColor={Colors.grey150}
              underlineColorAndroid={Colors.transparent}
              onSubmitEditing={() => this.passwordInput.focus()}
            />
          </View>
          <View
            style={[
              Helpers.column,
              language === "en" ? Helpers.crossStart : Helpers.crossEnd,
              ,
              Metrics.verticalMargin,
              Metrics.horizontalMargin
            ]}
          >
            <Text style={[Fonts.h5]}>
              {language === "en"
                ? Words.en.signIn.password
                : Words.ar.signIn.password}
            </Text>
            <TextInput
              ref={input => {
                this.passwordInput = input;
              }}
              style={[
                Styles.input,
                language === "ar" ? { textAlign: "right" } : null
              ]}
              secureTextEntry
              onChangeText={val => this._handlePassword(val)}
              placeholder={
                language === "en"
                  ? Words.en.signIn.password
                  : Words.ar.signIn.password
              }
              placeholderTextColor={Colors.grey150}
              underlineColorAndroid={Colors.transparent}
              returnKeyType="done"
            />
          </View>
          <View
            style={[
              Metrics.verticalMargin,
              Metrics.horizontalMargin,
              Helpers.center
            ]}
          >
            {error && <Text style={[Fonts.error]}>{error}</Text>}
          </View>
          <View
            style={[
              Metrics.verticalMargin,
              Metrics.horizontalMargin,
              language === "en" ? Helpers.crossEnd : null
            ]}
          >
            <TouchableOpacity onPress={this._handleReset}>
              <Text style={[Fonts.subTitle]}>
                {language === "en"
                  ? Words.en.auth.forgotPassword
                  : Words.ar.auth.forgotPassword}
              </Text>
            </TouchableOpacity>
          </View>
          <View style={[Helpers.center, Metrics.verticalMargin]}>
            <TouchableOpacity
              style={Styles.buttonContainer}
              onPress={this._handleAuthentication}
            >
              {isLoading ? (
                <ActivityIndicator size={"small"} color={Colors.white} />
              ) : (
                <Text
                  style={[
                    language === "en" ? Fonts.h3 : Fonts.normal,
                    { color: Colors.white }
                  ]}
                >
                  {language === "en"
                    ? Words.en.signIn.buttonText
                    : Words.ar.signIn.buttonText}
                </Text>
              )}
            </TouchableOpacity>
          </View>
          <View style={[Helpers.center, Metrics.verticalMargin]}>
            <TouchableOpacity onPress={this._handleSignUp}>
              <Text
                style={[
                  Fonts.normal,
                  { color: Colors.grey200, textDecorationLine: "underline" }
                ]}
              >
                {language === "en"
                  ? Words.en.signUp.buttonText
                  : Words.ar.signUp.buttonText}
              </Text>
            </TouchableOpacity>
          </View>

          <View style={{ height: 150 }} />
        </ScrollView>
        <Toast ref="toast" />
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  language: state.auth.language,
  isLoading: state.auth.isLoading,
  token: state.auth.token,
  user: state.auth.user,
  authError: state.auth.authError,
  isAuthenticated: state.auth.isAuthenticated
});

const mapDispatchToProps = dispatch => {
  return {
    onDispatchAuthenticateUser: (email, password) =>
      dispatch(authenticateUser(email, password)),
    onDispatchGetUserInfo: () => dispatch(getUserInformation()),
    onDispatchFetchUserAudio: () => dispatch(getUserAudios())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignIncreen);
