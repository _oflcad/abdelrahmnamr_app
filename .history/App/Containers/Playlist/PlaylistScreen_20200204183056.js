import React from "react";
import { View, FlatList, SafeAreaView, ActivityIndicator } from "react-native";
import { connect } from "react-redux";
import { ApplicationStyles, Helpers, Metrics, Words, Colors } from "../../Theme";
import HeaderComponent from "../../Components/Header/HeaderComponent";
import AddPlaylist from "../../Components/AddPlaylist/AddPlaylist";
import PlaylistHolder from "../../Components/PlaylistHolder/PlaylistHolder";
import Styles from "./Styles";
import EmptyPlaylist from "../../Components/EmptyPlaylist/EmptyPlaylist";
import NewPlaylistModal from "../../Components/NewPlaylistModal/NewPlaylistModal";
import Toast from "react-native-easy-toast";
import _ from "lodash";
import reactotron from "reactotron-react-native";
import { withNavigation } from 'react-navigation';
import { PropTypes } from 'prop-types';

const DURATION = 500;

class PlaylistScreen extends React.Component {

  constructor(props) {
    super(props)
    
    this.state = {
       data: [],
    }
  }
  

  componentDidMount() {
    const {navigation} = this.props;
    this.fetchingData();
    this.focusListener = navigation.addListener('didFocus', () => {
      reactotron.log("our user playlist list inside didFocus ", this.props.userPlaylists);
      this.fetchingData();
    })
  }
  
  componentWillUnmount() {
    this.focusListener.remove();
  }

  fetchingData = () => {
    const { userPlaylists } = this.props;
    this.setState({isLoading: true});
    setTimeout(() => {
      this.setState({data: userPlaylists, isLoading: false});
    })
  }


  componentDidUpdate(prevProps, prevState) {
    reactotron.log("we should update",!_.isEqual(this.props.userPlaylists, this.state.data))

    if(!_.isEqual(this.props.userPlaylists.length, this.state.data.length)){
      this.updatePlaylists(this.props.userPlaylists);
      this.refs.toast.show(Words.en.newPlaylistAdded, DURATION);
      return true;
    }


  }

  updatePlaylists = (data) =>{
    this.setState((prevState) => {
      if(_.isEqual(prevState.data,data)){
        return {data: data}
      }
    });
  }


  render() {
    const { userPlaylists } = this.props;
    const { isLoading} = this.state;
    return (
      <SafeAreaView style={[Helpers.fill]}>
        <HeaderComponent />
        <AddPlaylist />
        <NewPlaylistModal />
        <View style={[ApplicationStyles.dividerHorizontal]} />
        <FlatList
          showsVerticalScrollIndicator={false}
          contentContainerStyle={[
            Helpers.mainCenter,
            Metrics.tinyVerticalMargin,
            { paddingBottom: 60 }
          ]}
          contentInset={{ top: 0, bottom: 20, left: 0, right: 0 }}
          contentInsetAdjustmentBehavior="automatic"
          data={userPlaylists}
          style={Styles.list}
          extraData={this.props}
          renderItem={({item}) => isLoading ? <ActivityIndicator size={"large"} color={Colors.grey200} /> : <PlaylistHolder item={item} />}
          ListEmptyComponent={() => <EmptyPlaylist />}
          keyExtractor={(item, index) => "key" + item.createdAt}
        />
        <Toast ref="toast" />
      </SafeAreaView>
    );
  }
}

PlaylistScreen.propTypes = {
  userPlaylists: PropTypes.array.isRequired,
  PlaylistHolder: PropTypes.elementType,
  EmptyPlaylist: PropTypes.elementType,
}

const mapStateToProps = state => ({
  userPlaylists: state.media.userPlaylists,
  playlistToDisplay: state.media.playlistToDisplay,
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, null)(withNavigation(PlaylistScreen));
