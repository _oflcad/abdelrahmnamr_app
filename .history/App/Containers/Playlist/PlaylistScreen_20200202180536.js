import React from "react";
import { View, FlatList, SafeAreaView } from "react-native";
import { connect } from "react-redux";
import { ApplicationStyles, Helpers, Metrics, Words } from "../../Theme";
import HeaderComponent from "../../Components/Header/HeaderComponent";
import AddPlaylist from "../../Components/AddPlaylist/AddPlaylist";
import PlaylistHolder from "../../Components/PlaylistHolder/PlaylistHolder";
import Styles from "./Styles";
import EmptyPlaylist from "../../Components/EmptyPlaylist/EmptyPlaylist";
import NewPlaylistModal from "../../Components/NewPlaylistModal/NewPlaylistModal";
import Toast from "react-native-easy-toast";
import _ from "lodash";
import reactotron from "reactotron-react-native";
const DURATION = 500;

class PlaylistScreen extends React.Component {
  _renderItem = ({ item }) => {
    return <PlaylistHolder item={item} />;
  };

  componentDidUpdate(prevProps) {
    reactotron.log("we should update",!_.isEqual(prevProps.userPlaylists, this.props.userPlaylists))
    if (!_.isEqual(prevProps.userPlaylists, this.props.userPlaylists)) {
      this.refs.toast.show(Words.en.newPlaylistAdded, DURATION);
      return true;
    }
    
  }

  render() {
    const { userPlaylists } = this.props;
    return (
      <SafeAreaView style={[Helpers.fill]}>
        <HeaderComponent />
        <AddPlaylist />
        <NewPlaylistModal />
        <View style={[ApplicationStyles.dividerHorizontal]} />
        <FlatList
          showsVerticalScrollIndicator={false}
          contentContainerStyle={[
            Helpers.mainCenter,
            Metrics.tinyVerticalMargin,
            { paddingBottom: 60 }
          ]}
          contentInset={{ top: 0, bottom: 20, left: 0, right: 0 }}
          contentInsetAdjustmentBehavior="automatic"
          data={userPlaylists}
          style={Styles.list}
          renderItem={this._renderItem}
          ListEmptyComponent={() => <EmptyPlaylist />}
          keyExtractor={(item, index) => "key" + item.id}
        />

        <Toast ref="toast" />
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  userPlaylists: state.media.userPlaylists,
  playlistToDisplay: state.media.playlistToDisplay,
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, null)(PlaylistScreen);
