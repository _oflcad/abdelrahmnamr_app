import React from "react";
import { View, FlatList, SafeAreaView } from "react-native";
import { connect } from "react-redux";
import { ApplicationStyles, Helpers, Metrics, Words } from "../../Theme";
import HeaderComponent from "../../Components/Header/HeaderComponent";
import AddPlaylist from "../../Components/AddPlaylist/AddPlaylist";
import PlaylistHolder from "../../Components/PlaylistHolder/PlaylistHolder";
import Styles from "./Styles";
import EmptyPlaylist from "../../Components/EmptyPlaylist/EmptyPlaylist";
import NewPlaylistModal from "../../Components/NewPlaylistModal/NewPlaylistModal";
import Toast from "react-native-easy-toast";
import _ from "lodash";
import reactotron from "reactotron-react-native";
const DURATION = 500;

class PlaylistScreen extends React.Component {

  constructor(props) {
    super(props)
  
    this.state = {
       data: [],
    }
  }
  

  componentDidMount() {
    this.setState({data: this.props.userPlaylists})
  }

  _renderItem = ({ item }) => {
    return <PlaylistHolder item={item} />;
  };



  componentDidUpdate(prevProps, prevState) {
    reactotron.log("we should update",!_.isEqual(prevProps.userPlaylists, this.state.data))

    if(!_.isEqual(prevProps.userPlaylists.length, this.props.userPlaylists.length)){
      this.updatePlaylists(this.props.userPlaylists);
      this.refs.toast.show(Words.en.newPlaylistAdded, DURATION);
    }

    _.some(this.props.userPlaylists, (el ,index)=> {
      reactotron.log("THIS PROPS",el.downloaded);
      reactotron.log("PREVPROPS PROPS", prevProps.userPlaylists[index].downloaded);
      reactotron.log("we should update",!_.isEqual(el.downloaded, prevProps.userPlaylists[index].downloaded))
      if(!_.isEqual(el.downloaded, prevProps.userPlaylists[index].downloaded)){
        this.updatePlaylists(this.props.userPlaylists);
      }
      
    })


  }

  updatePlaylists = (data) =>{
    this.setState({data});
  }


  render() {
    const { userPlaylists } = this.props;
    const {data} = this.state;
    return (
      <SafeAreaView style={[Helpers.fill]}>
        <HeaderComponent />
        <AddPlaylist />
        <NewPlaylistModal />
        <View style={[ApplicationStyles.dividerHorizontal]} />
        <FlatList
          showsVerticalScrollIndicator={false}
          contentContainerStyle={[
            Helpers.mainCenter,
            Metrics.tinyVerticalMargin,
            { paddingBottom: 60 }
          ]}
          contentInset={{ top: 0, bottom: 20, left: 0, right: 0 }}
          contentInsetAdjustmentBehavior="automatic"
          data={data}
          style={Styles.list}
          extraData={this.props}
          renderItem={this._renderItem}
          ListEmptyComponent={() => <EmptyPlaylist />}
          keyExtractor={(item, index) => "key" + item.id}
          
        />

        <Toast ref="toast" />
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  userPlaylists: state.media.userPlaylists,
  playlistToDisplay: state.media.playlistToDisplay,
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, null)(PlaylistScreen);
