import React from "react";
import {
  Platform,
  Text,
  View,
  Button,
  ActivityIndicator,
  Image,
  FlatList,
  SafeAreaView
} from "react-native";
import { connect } from "react-redux";
import { Helpers, Metrics, Words, Fonts } from "../../Theme";
import HeaderComponent from "../../Components/Header/HeaderComponent";
import PlaylistHolder from "../../Components/PlaylistHolder/PlaylistHolder";
import EmptyPlaylist from "../../Components/EmptyPlaylist/EmptyPlaylist";
class HomeScreen extends React.Component {
  _renderItem = ({ item }) => {
    return <PlaylistHolder item={item} />;
  };

  render() {
    const { userPlaylists, language } = this.props;
    let filtered = userPlaylists.filter(el => el.downloaded )
    return (
      <SafeAreaView style={[Helpers.fill]}>
        <HeaderComponent />
        <View style={[language === "ar" ? Helpers.crossEnd: null]}>
        <Text
          style={[
            Fonts.h4,
            Metrics.mediumHorizontalMargin,
            Metrics.smallVerticalMargin
          ]}
        >
          {language === 'en' ? Words.en.downloads.downloads : Words.ar.downloads.downloads}
        </Text>
        </View>
        <FlatList
          data={filtered}
          style={[Metrics.smallVerticalMargin]}
          contentContainerStyle={[Helpers.mainCenter,  { paddingBottom: 60 }]}
          renderItem={this._renderItem}
          keyExtractor={index => "key" + index}
          ListEmptyComponent={<EmptyPlaylist downloads={true} />}
        />
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  app: state.app,
  userPlaylists: state.media.userPlaylists,
  language: state.app.language
});


export default connect(mapStateToProps, null)(HomeScreen);
