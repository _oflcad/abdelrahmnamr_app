import React from "react";
import {
  Text,
  View,
  FlatList,
  SafeAreaView
} from "react-native";
import { connect } from "react-redux";
import { Helpers, Metrics, Words, Fonts } from "../../Theme";
import HeaderComponent from "../../Components/Header/HeaderComponent";
import PlaylistHolder from "../../Components/PlaylistHolder/PlaylistHolder";
import EmptyPlaylist from "../../Components/EmptyPlaylist/EmptyPlaylist";
import reactotron from "reactotron-react-native";
class HomeScreen extends React.Component {
  _renderItem = ({ item }) => {
    return <PlaylistHolder item={item} />;
  };

  render() {
    const { userPlaylists, language } = this.props;
    reactotron.log('LIBRARY !!', this.props.userPlaylists);
    //let filtered = userPlaylists.filter(el => el.downloaded )
    return (
      <SafeAreaView style={[Helpers.fill]}>
        <HeaderComponent />
        <View style={[language === "ar" ? Helpers.crossEnd: null]}>
        <Text
          style={[
            Fonts.h4,
            Metrics.mediumHorizontalMargin,
            Metrics.smallVerticalMargin
          ]}
        >
          {language === 'en' ? Words.en.downloads.downloads : Words.ar.downloads.downloads}
        </Text>
        </View>
        
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  app: state.app,
  userPlaylists: state.media.userPlaylists,
  language: state.auth.language
});


export default connect(mapStateToProps, null)(HomeScreen);
