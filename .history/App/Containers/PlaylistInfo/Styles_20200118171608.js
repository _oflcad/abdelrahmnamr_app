import { StyleSheet } from "react-native";
import { Metrics, Fonts, Colors, Helpers } from "../../Theme";

export default StyleSheet.create({
  wrapper: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    ...Metrics.smallHorizontalMargin,
    flexDirection: "column",
    backgroundColor: Colors.white
  },
  container: {
    width: "100%",
    height: "100%",
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  thumbnail: {
    width: Metrics.height / 4,
    height: Metrics.height / 4,
    borderRadius: 10,
  },
  input: {
    width: Metrics.width - 100,
    height: 50,
    color: Colors.black,
    textAlign: "center",
    borderRadius: 10,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: Colors.grey200,
  },
  topContainer: {

  },
  error: {
    ...Fonts.normal,
    color: Colors.error,
    marginBottom: Metrics.tiny,
    textAlign: "center"
  },
  buttonContainer: {
    width: Metrics.width - 180,
    height: 50,
    borderRadius: 20,
    backgroundColor: Colors.black,
    ...Helpers.center
  },
  buttonText: {
    ...Fonts.h4,
    color: Colors.white
  },
  background: {
    width: '100%',
    height: '100%',
  },
  toastContainer: {
    backgroundColor: Colors.grey100,
    ...Helpers.center
  },
  toastText: {
    ...Fonts.buttonText
  }
});
