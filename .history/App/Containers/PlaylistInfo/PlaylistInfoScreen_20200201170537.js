import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  SafeAreaView
} from "react-native";
import NestedHeader from "../../Components/NestedHeader";
import Styles from "./Styles";
import {
  Helpers,
  Images,
  Metrics,
  Fonts,
  Words,
  Colors,
  ApplicationStyles
} from "../../Theme";
import { connect } from "react-redux";
import InfoModal from "../../Components/InfoModal/InfoModal";
import EmptyPlaylist from "../../Components/EmptyPlaylist/EmptyPlaylist";
import {
  openAddSongsModal,
  downloadPlaylist
} from "../../Actions/Media/Actions";
import AddSongsModal from "../../Components/AddSongsModal/AddSongsModal";
import moment from "moment";
import { Switch } from "react-native-switch";
import PlaySongContainer from "../../Components/PlaySongContainer/PlaySongContainer";
import NavigationService from "../../Services/NavigationService";
import Toast from 'react-native-easy-toast'
class PlaylistInfoScreen extends Component {
  _renderItem = ({ item }) => {
    return <PlaySongContainer item={item} />;
  };

  _handleAddSongs = () => {
    const { isAuthenticated } = this.props;
    if (isAuthenticated) {
      this.props.onDispatchOpenModal();
    } else {
      NavigationService.navigate("SignIn");
    }
  };

  _handleToggleDownload = () => {
    const { playlistToDisplay } = this.props.media;
    this.props.onDispatchTogglePlaylist(playlistToDisplay);
  };

  render() {
    const {
      title,
      createdAt,
      songs,
      downloaded
    } = this.props.media.playlistToDisplay;
    const { language } = this.props;
    return (
      <SafeAreaView style={[Helpers.fill, Metrics.tinyVerticalMargin]}>
        <NestedHeader customTitle={title} />

        <View style={[Helpers.center, Metrics.mediumVerticalMargin]}>
          <Image resizeMode={"contain"} source={Images.thumbnail} style={Styles.thumbnail} />
          <View style={[Helpers.row, Helpers.mainSpaceAround, Helpers.center]}>
            <Text
              style={[
                Metrics.tinyVerticalMargin,
                Metrics.smallHorizontalMargin,
                Fonts.h2,
                { color: Colors.black }
              ]}
            >
              {title}
            </Text>
            <View />
          </View>
        </View>
        <View style={[Helpers.row]}>
          <View
            style={[
              Helpers.column,
              Metrics.smallHorizontalMargin,
              Helpers.mainSpaceAround,
            ]}
          >
            <Text style={[Fonts.subTitle]}>{songs.length} audios</Text>
            <Text style={[Fonts.label, { color: Colors.grey150 }]}>
              {moment(createdAt).fromNow()}
            </Text>
          </View>
          <View
            style={[
              Helpers.fill,
              Helpers.crossCenter,
              Metrics.smallHorizontalMargin,
            ]}
          >
            <Text>Download</Text>
          </View>
        </View>
        <View style={[ApplicationStyles.dividerHorizontal]} />
        <View style={[Metrics.tinyHorizontalMargin]}>
          <TouchableOpacity
            onPress={this._handleAddSongs}
            style={[
              language === "en" ? Helpers.row : Helpers.rowReverse,
              Helpers.mainStart,
              Helpers.crossCenter,
              Metrics.tinyHorizontalMargin
            ]}
          >
            <View
              style={[
                Helpers.center,
                Metrics.tinyHorizontalMargin,
                { marginTop: 4 }
              ]}
            >
              <Image resizeMode={"contain"} source={Images.addCircle} style={Styles.icon} />
            </View>
            <Text style={[Fonts.subTitle, Metrics.tinyHorizontalMargin]}>
              {language === "en" ? Words.en.addAudio : Words.ar.addAudio}
            </Text>
          </TouchableOpacity>
        </View>
        <View style={[ApplicationStyles.dividerHorizontal, Helpers.center]} />
        <FlatList
          showsVerticalScrollIndicator={false}
          contentContainerStyle={[
            Helpers.mainCenter,
            Metrics.tinyVerticalMargin,
            { paddingBottom: 60 }
          ]}
          contentInset={{ top: 0, bottom: 20, left: 0, right: 0 }}
          contentInsetAdjustmentBehavior="automatic"
          data={songs}
          renderItem={this._renderItem}
          keyExtractor={(item, index) => "key" + index}
          ListEmptyComponent={() => <EmptyPlaylist title={"songs"} />}
        />
        <InfoModal />
        <AddSongsModal />
        <Toast
          ref="toast"
          style={Styles.toastContainer}
          position={"bottom"}
          opacity={0.8}
          textStyle={Styles.toastText}
        />
        
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => {
  return {
    media: state.media,
    language: state.auth.language,
    isAuthenticated: state.auth.isAuthenticated
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onDispatchOpenModal: () => dispatch(openAddSongsModal()),
    onDispatchTogglePlaylist: playlist => dispatch(downloadPlaylist(playlist))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PlaylistInfoScreen);
