import * as Types from "../../Actions/App/Types";
import { INITIAL_STATE } from "../../Actions/App/InitialState";

module.exports = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case Types.BUY_PRODUCT_SUCCESS:
      return {
        ...state,
        imageUri: "",
        basket: []
      }
    case Types.BUY_PRODUCT_FAILURE:
      return {
        ...state,
      }
    case Types.SET_IMAGE_URI:
      return {
        ...state,
        imageUri: action.path
      }
    case Types.CALCULATE_TOTAL: 
      return {
        ...state,
        total: action.newPrice,
      }
    case Types.FIRST_TIMER:
      return {
        ...state,
        isFirstTimer: false,
        language: action.language
      };
    case Types.SET_DEVICE_LANGUAGE:
      return {
        ...state,
        language: action.language
      };
    case Types.LOADINGS:
      return {
        ...state,
        isLoading: true
      };
    case Types.FETCH_LATEST_NEWS_SUCCESS:
      return {
        ...state,
        isLoading: false
      };
    case Types.FETCH_LATEST_NEWS_FAILURE:
      return {
        ...state,
        isLoading: false
      };
    case Types.FETCH_LATEST_SESSIONS_SUCCESS:
    case Types.FETCH_LATEST_SESSIONS_FAILURE:
    case Types.FETCH_LATEST_BLOG_SUCCESS:
    case Types.FETCH_LATEST_BLOG_FAILURE:
    case Types.SET_ITEM:
      return {
        ...state,
        item: action.item
      }
    case Types.CLEAR_ITEM:
      return {
        ...state,
        item: null
      }
    case Types.ROUTE_NAME: 
      return {
        ...state,
        routeName: action.routeName
      }
    case Types.SET_ITEM_TO_BASKET: 
    return {
      ...state,
      itemToBasket: action.song,
    }
    case Types.REMOVE_ITEM_FROM_BASKET:
      return {
        ...state,
        itemToBasket: null,
      }  
    case Types.ADD_ITEM_TO_BASKET:
      return {
        ...state,
        basket: state.basket.concat(action.item),
        isItemModalOpen: false
      }
      case Types.REMOVE_ITEM_TO_BASKET:
        let removedArray = state.basket.filter(el => el !== action.item)
        return {
          ...state,
          basket: removedArray
        }
    default:
      return state;
  }
};
