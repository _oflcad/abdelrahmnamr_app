import * as Types from "../../Actions/Auth/Types";

import { INITIAL_STATE } from "../../Actions/Auth/InitialState";
import { Words } from "../../Theme";

module.exports = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case Types.LOADING:
      return {
        ...state,
        isLoading: true
      };
    case Types.AUTHENTICATE_USER_SUCCESS:
      return {
        ...state,
        isLoading: false,
        token: action.token,
        isAuthenticated: true
      };
    case Types.AUTHENTICATE_USER_FAILURE:
      return {
        ...state,
        isLoading: false,
        authError: "Please check Credentials"
      };
    case Types.GET_USER_SUCCESS:
      return {
        ...state,
        isLoading: false,
        user: action.user
      };
    case Types.GET_USER_FAILURE:
      return {
        ...state,
        isLoading: false
      };
    case Types.REGISTER_USER_SUCCESS:
      return {
        ...state,
        isLoading: false,
        email: action.email,
        confirmEmail: true,
      };
    case Types.LOGOUT_USER:
      return {
        ...state,
        isAuthenticated: false,
        token: null,
        isLoading: false,
        user: null
      };
    default:
      return state;
  }
};
