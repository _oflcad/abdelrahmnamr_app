import * as Types from "../../Actions/Media/Types";
import {
  addSongToPlaylist,
  removeSongFromPlaylist,
  updateUserPlaylist,
  updatedDownloadedUserPlaylist
} from "../../Services/ReduxService";
import { INITIAL_STATE } from "../../Actions/Media/InitialState";
import Reactotron from "reactotron-react-native";
module.exports = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case Types.SET_AUDIO_TITLE:
      return {
        ...state,
        audio: action.audio,
      }
    case Types.TOGGLE_FULL_SIZE: 
      return {
        ...state,
        isFullPlayerShow: !state.isFullPlayerShow
      }
    case Types.SHOW_MINI_PLAYER:
      return {
        ...state,
        isPlayerShowen: true
      }
    case Types.HIDE_MINI_PLAYER:  
      return {
        ...state,
        isPlayerShowen: false,
      }
    case Types.SET_PLAYLIST:
      return {
        ...state,
        playlistToDisplay: action.playlist,
        selectedSongs: [],
      };
    case Types.OPEN_PLAYLIST_MODAL_VISIBLE:
      return {
        ...state,
        isAddPlaylistModalVisible: true
      };
    case Types.CLOSE_PLAYLIST_MODAL_VISIBLE:
      return {
        ...state,
        isAddPlaylistModalVisible: false
      };
    case Types.CHANGE_PLAYLIST_TITLE:
      let edited = state.playlistToDisplay;
      edited.title = action.title;
      return {
        ...state,
        playlistToDisplay: edited,
        isAddPlaylistModalVisible: false
      };
    case Types.CREATE_PLAYLIST:
      let copy = state.userPlaylists.slice();
      copy.push(action.playlist);
      return {
        ...state,
        userPlaylists: [
          ...state.userPlaylists
        ]
      };
    case Types.OPEN_PLAYLISTINFO_MODAL_VISIBLE:
      return {
        ...state,
        isAddPlaylistInfoModalVisible: true
      };
    case Types.CLOSE_PLAYLISTINFO_MODAL_VISIBLE:
      return {
        ...state,
        isAddPlaylistInfoModalVisible: false
      };
    case Types.DELETE_PLAYLIST:
      let playlists = state.userPlaylists.filter(
        playlist => playlist !== action.playlist
      );
      return {
        ...state,
        userPlaylists: playlists
      };
    case Types.OPEN_ADD_SONGS_MODAL_VISIBLE:
      return {
        ...state,
        isAddSongsToPlaylistModalVisible: true
      };
    case Types.CLOSE_ADD_SONGS_MODAL_VISIBLE:
      return {
        ...state,
        isAddSongsToPlaylistModalVisible: false
      };
    case Types.ADD_SONG_TO_PLAYLIST:
      {
        var selectedSongsUpdated = addSongToPlaylist(
          action.song,
          state.selectedSongs
        );
        var updatedUserPlaylist = updateUserPlaylist(
          state.playlistToDisplay,
          state.userPlaylists,
          selectedSongsUpdated
        );
      }
      return {
        ...state,
        userPlaylists: updatedUserPlaylist,
        selectedSongs: selectedSongsUpdated
      };
    case Types.REMOVE_SONG_FROM_PLAYLIST:
      {
        var updatedSelectedSongs = removeSongFromPlaylist(
          action.song,
          state.selectedSongs
        );
       
        var temp2 = updateUserPlaylist(
          state.playlistToDisplay,
          state.userPlaylists,
          updatedSelectedSongs
        );
        Reactotron.log(
          "REMOVE state values redux:",
          state.playlistToDisplay,
          state.userPlaylists,
          temp2
        );
      }
      return {
        ...state,
        userPlaylists: temp2,
        selectedSongs: updatedSelectedSongs
      };
    case Types.HANDLE_AUTOPLAY:
      return {
        ...state,
        isAutoPlay: !state.isAutoPlay
      };
    case Types.TOGGLE_DOWNLOAD_PLAYLIST:
      return {
        ...state,
        userPlaylists: action.userPlaylistCopy,
        playlistToDisplay: action.downloadedPlaylist
      };
    case Types.UPDATE_DOWNLOADAUDIO_ARRAY:
      let newPlaylistToDisplay = action.downloadedPlaylist
      newPlaylistToDisplay.downloadedAudios = action.downloadedAudioArray;
      let temp5 = updatedDownloadedUserPlaylist(newPlaylistToDisplay, action.newUserPlaylists)
    return {
      ...state,
      playlistToDisplay: newPlaylistToDisplay,
      userPlaylists: temp5,
    }  
    case Types.LOADING:
      return {
        ...state,
        isLoading: true
      };
    case Types.FETCH_ALL_AUDIOS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        latestAudios: action.audios 
      };
    case Types.FETCH_ALL_AUDIOS_FAILURE:
      return {
        ...state,
        isLoading: false
      };
    case Types.GET_USER_AUDIO_SUCCESS:
      return {
        ...state,
        userAudios: action.userAudios
      }
    case Types.GET_USER_AUDIO_FAILURE:  
      return {
        ...state,
        userAudios: []
      }
    case Types.SET_SONG_AUDIOS:
      return {
        ...state,
        audiosToPlay:action.audioArray,
        nbrParts: action.nbrParts,
        indexToPlay: action.index,
      }
    case Types.SET_SONG_AUDIOS_FROM_DOWNLOAD: 
    return {
      ...state,
      audiosToPlay: action.audioArray,
    }
    case Types.PLAY_SONG_FROM_DOWNLOAD:
      return {
        ...state,
        isPlayingDownload: true,
        isPlaying: false,
        isPlayerShowenDownload: true,
        isPlayerShowen: false,
        songToPlay: action.song,
      }
    case Types.PLAY_SONG: 
      return {
        ...state,
        isPlaying: true,
        isPlayingDownload: false,
        isPlayerShowen: true,
        isPlayerShowenDownload: false,
        songToPlay: action.song,
      }
    case Types.PAUSE_SONG:
      return {
        ...state,
        isPlaying: false,
      }  
    case Types.RESUME_SONG:
      return {
        ...state,
        isPlaying: true,
      }
      case Types.STOP_SONG:
        return {
          ...state,
          isPlaying: false,
        }    
    case Types.PAUSE_SONG_FROM_DOWNLOAD:
      return {
        ...state,
        isPlayingDownload: false,
      }  
    case Types.RESUME_SONG_FROM_DOWNLOAD:
      return {
        ...state,
        isPlayingDownload: true,
      }
      case Types.STOP_SONG_FROM_DOWNLOAD:
        return {
          ...state,
          isPlayingDownload: false,
        }    
    case Types.SET_SONG_INDEX:
    return {
      ...state,
      indexToPlay: action.index,
    }
    case Types.LOGOUT_USER:
      return {
        ...state,
        isPlayerShowen: false,
        isPlaying: false,
        songToPlay: null, 
        userAudios: []
      }
    default:
      return state;
  }
};
