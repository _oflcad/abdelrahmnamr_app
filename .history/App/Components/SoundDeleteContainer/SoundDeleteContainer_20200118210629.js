import React from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import { connect } from "react-redux";
import {
  removeItemfromBasket,
  calculateTotal
} from "../../Actions/App/Actions";
import { Helpers, Metrics, Fonts, Colors, Images } from "../../Theme";
import Styles from "./Styles";
const SoundDeleteContainer = props => {
  const { price } = props.item;
  const { language } = props.language;
  const _handleRemoveItem = () => {
    props.onDispatchRemoveAudioFromBasket(props.item);
    props.onDipsatchCalculateTotal();
  };
  if (language === "en") {
    return (
      <View style={[Helpers.row, Helpers.crossCenter]}>
        <Text style={[Fonts.newPrice]}>{price} $ </Text>
        <TouchableOpacity
          onPress={_handleRemoveItem}
          style={Styles.deleteButton}
        >
          <Image style={{ width: 25, height: 25 }} source={Images.trashBin} />
        </TouchableOpacity>
      </View>
    );
  }
  return (
    <View style={[Helpers.row, Helpers.crossCenter]}>
      <TouchableOpacity onPress={_handleRemoveItem} style={Styles.deleteButton}>
        <Image style={{ width: 25, height: 25 }} source={Images.trashBin} />
      </TouchableOpacity>
      <Text style={[Fonts.newPrice]}>{price} $ </Text>
    </View>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    onDispatchRemoveAudioFromBasket: song =>
      dispatch(removeItemfromBasket(song)),
    onDipsatchCalculateTotal: () => dispatch(calculateTotal())
  };
};

export default connect(null, mapDispatchToProps)(SoundDeleteContainer);
