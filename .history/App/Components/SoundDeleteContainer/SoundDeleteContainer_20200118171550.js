import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { removeItemfromBasket, calculateTotal } from "../../Actions/App/Actions";
import { Helpers, Metrics, Fonts, Colors } from "../../Theme";
import { Ionicons } from "@expo/vector-icons";
import Styles from "./Styles";
const SoundDeleteContainer = props => {
  const { price } = props.item;
  const { language } = props.language;
  const _handleRemoveItem = () => {
    props.onDispatchRemoveAudioFromBasket(props.item);
    props.onDipsatchCalculateTotal()
  };
  if (language === "en") {
    return (
      <View style={[Helpers.row, Helpers.crossCenter]}>
        <Text style={[Fonts.newPrice]}>{price} $ </Text>
        <TouchableOpacity
          onPress={_handleRemoveItem}
          style={Styles.deleteButton}
        >
          <Ionicons name="ios-trash" size={25} color={Colors.error} />
        </TouchableOpacity>
      </View>
    );
  }
  return (
    <View style={[Helpers.row, Helpers.crossCenter]}>
      <TouchableOpacity onPress={_handleRemoveItem} style={Styles.deleteButton}>
        <Ionicons name="ios-trash" size={25} color={Colors.error} />
      </TouchableOpacity>
      <Text style={[Fonts.newPrice]}>{price} $ </Text>
    </View>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    onDispatchRemoveAudioFromBasket: song =>
      dispatch(removeItemfromBasket(song)),
      onDipsatchCalculateTotal: () => dispatch(calculateTotal())
  };
};

export default connect(null, mapDispatchToProps)(SoundDeleteContainer);
