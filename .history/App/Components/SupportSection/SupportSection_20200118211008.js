import React from "react";
import { View, Text, TouchableOpacity, Image, Linking } from "react-native";
import Styles from "./Styles";
import { Words, Helpers, Metrics, Fonts, Colors, Images } from "../../Theme";
import { Ionicons } from "@expo/vector-icons";
import { connect } from "react-redux";
const SupportSection = (props) => {

    const _handleContactUs = () => {
        Linking.openURL('mailTo: 	contact@abdelrahman-amr.com')
    }
    const _handleShareApp = () => {
        
    }
    const _handleRateApp = () => {
        
    }
    if(props.language === 'ar'){
      return (
        <View style={Styles.container}>
          <TouchableOpacity
            onPress={_handleContactUs}
            style={[
              Helpers.rowReverse,
              Helpers.mainSpaceBetween,
              Helpers.crossCenter,
              Metrics.tinyHorizontalMargin,
              Styles.buttonContainer,
              Metrics.tinyVerticalMargin,
            ]}
          >
          
            <Text style={[Fonts.normal,Metrics.tinyHorizontalMargin, { textAlign: "right" }]}>
              {Words.ar.SupportSection.contactUs}
            </Text>
            <View style={[Helpers.center, Metrics.tinyHorizontalMargin]}>
            <Image
              source={Images.rightArrow}
              style={{width: 25, height: 25}}
            />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={_handleShareApp}
            style={[
              Helpers.rowReverse,
              Helpers.mainSpaceBetween,
              Helpers.crossCenter,
              Metrics.tinyHorizontalMargin,
              Styles.buttonContainer,
              Metrics.tinyVerticalMargin,
            ]}
          >
            <Text style={[Fonts.normal,Metrics.tinyHorizontalMargin, { textAlign: "right" }]}>
              {Words.ar.SupportSection.shareTheApp}
            </Text>
            <View style={[Helpers.center, Metrics.tinyHorizontalMargin]}>
            <Image
              source={Images.leftArrow}
              style={{width: 25, height: 25}}
            />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={_handleRateApp}
            style={[
              Helpers.rowReverse,
              Helpers.mainSpaceBetween,
              Helpers.crossCenter,
              Metrics.tinyHorizontalMargin,
              Styles.buttonContainer,
              Metrics.tinyVerticalMargin,
            ,{
                borderBottomColor: Colors.white
            }]}
          >
            <Text style={[Fonts.normal,Metrics.tinyHorizontalMargin, { textAlign: "right" }]}>
              {Words.ar.SupportSection.rateTheApp}
            </Text>
            <View style={[Helpers.center, Metrics.tinyHorizontalMargin]}>
            <Image
              source={Images.leftArrow}
              style={{width: 25, height: 25}}
            />
            </View>
          </TouchableOpacity>
        </View>
      );
    }
  return (
    <View style={Styles.container}>
      <TouchableOpacity
        onPress={_handleContactUs}
        style={[
          Helpers.row,
          Helpers.mainSpaceBetween,
          Helpers.crossCenter,
          Metrics.tinyHorizontalMargin,
          Styles.buttonContainer,
          Metrics.tinyVerticalMargin,
        ]}
      >
      
        <Text style={[Fonts.normal,Metrics.tinyHorizontalMargin, { textAlign: "left" }]}>
          {Words.en.SupportSection.contactUs}
        </Text>
        <View style={[Helpers.center, Metrics.tinyHorizontalMargin]}>
        <Image
              source={Images.rightArrow}
              style={{width: 25, height: 25}}
            />
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={_handleShareApp}
        style={[
          Helpers.row,
          Helpers.mainSpaceBetween,
          Helpers.crossCenter,
          Metrics.tinyHorizontalMargin,
          Styles.buttonContainer,
          Metrics.tinyVerticalMargin,
        ]}
      >
        <Text style={[Fonts.normal,Metrics.tinyHorizontalMargin, { textAlign: "left" }]}>
          {Words.en.SupportSection.shareTheApp}
        </Text>
        <View style={[Helpers.center, Metrics.tinyHorizontalMargin]}>
        <Image
              source={Images.rightArrow}
              style={{width: 25, height: 25}}
            />
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={_handleRateApp}
        style={[
          Helpers.row,
          Helpers.mainSpaceBetween,
          Helpers.crossCenter,
          Metrics.tinyHorizontalMargin,
          Styles.buttonContainer,
          Metrics.tinyVerticalMargin,
        ,{
            borderBottomColor: Colors.white
        }]}
      >
        <Text style={[Fonts.normal,Metrics.tinyHorizontalMargin, { textAlign: "left" }]}>
          {Words.en.SupportSection.rateTheApp}
        </Text>
        <View style={[Helpers.center, Metrics.tinyHorizontalMargin]}>
        <Image
              source={Images.leftArrow}
              style={{width: 25, height: 25}}
            />
        </View>
      </TouchableOpacity>
    </View>
  );
};

const mapStateToProps = state => {
return{
  language: state.app.language,
}
}
export default connect(mapStateToProps,null)(SupportSection);
