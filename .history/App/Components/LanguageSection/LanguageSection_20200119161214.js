import React, { useState, useEffect } from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import Styles from "./Styles";
import { Helpers, Metrics, Fonts, Colors, Words, Images } from "../../Theme";
import { Ionicons } from "@expo/vector-icons";
import { connect } from "react-redux";
import { setDeviceLanguage } from "../../Actions/App/Actions";
const LanguageSection = props => {
  const [lang, setLang] = useState("");
  let languages = {
    en: "en",
    ar: "ar"
  };
  useEffect(() => {
    setLang(props.language);
  });

  const _handleChangeLanguage = language => {
    setLang(language);
    props.onDispatchSetDeviceLanguage(language);
  };
  if (props.language === "en") {
    return (
      <View style={[Styles.container]}>
        <TouchableOpacity
          onPress={() => _handleChangeLanguage(languages.en)}
          style={[
            Helpers.row,
            Helpers.mainSpaceBetween,
            Helpers.crossCenter,
            Styles.buttonContainer,
            Metrics.tinyHorizontalMargin,
            Metrics.tinyVerticalMargin
          ]}
        >
          <Text
            style={[
              Fonts.normal,
              Metrics.tinyHorizontalMargin,
              { textAlign: "left" }
            ]}
          >
            {Words.en.languageSection.english}
          </Text>
          <View style={[Helpers.center, Metrics.tinyHorizontalMargin]}>
            {props.language === languages.en ? (
              <Image resizeMode={"contain"} resizeMode={"contain"} source={Images.chosen} style={Styles.icon} />
            ) : (
              <Image resizeMode={"contain"} resizeMode={"contain"} source={Images.checkMarkEmpty} style={Styles.icon} />
            )}
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => _handleChangeLanguage(languages.ar)}
          style={[
            Helpers.row,
            Helpers.mainSpaceBetween,
            Helpers.crossCenter,
            Styles.buttonContainer,
            Metrics.tinyVerticalMargin,
            Metrics.tinyHorizontalMargin,
            {
              borderBottomColor: Colors.white
            }
          ]}
        >
          <Text
            style={[
              Fonts.normal,
              Metrics.tinyHorizontalMargin,
              { textAlign: "left" }
            ]}
          >
            {Words.en.languageSection.arabic}
          </Text>
          <View style={[Helpers.center, Metrics.tinyHorizontalMargin]}>
            {props.language === languages.en ? (
              <Image resizeMode={"contain"} resizeMode={"contain"}source={Images.checkMark} style={Styles.icon} />
            ) : (
              <Image resizeMode={"contain"} resizeMode={"contain"}source={Images.checkMarkEmpty} style={Styles.icon} />
            )}
          </View>
        </TouchableOpacity>
      </View>
    );
  } else {
    return (
      <View style={[Styles.container]}>
        <TouchableOpacity
          onPress={() => _handleChangeLanguage(languages.en)}
          style={[
            Helpers.rowReverse,
            Helpers.mainSpaceBetween,
            Helpers.crossCenter,
            Styles.buttonContainer,
            Metrics.tinyHorizontalMargin,
            Metrics.tinyVerticalMargin
          ]}
        >
          <Text
            style={[
              Fonts.normal,
              Metrics.tinyHorizontalMargin,
              { textAlign: "left" }
            ]}
          >
            {Words.en.languageSection.english}
          </Text>
          <View style={[Helpers.center, Metrics.tinyHorizontalMargin]}>
            {props.language === languages.ar ? (
              <Image resizeMode={"contain"} resizeMode={"contain"}source={Images.checkMark} style={Styles.icon} />
            ) : (
              <Image resizeMode={"contain"} resizeMode={"contain"}source={Images.checkMarkEmpty} style={Styles.icon} />
            )}
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => _handleChangeLanguage(languages.ar)}
          style={[
            Helpers.rowReverse,
            Helpers.mainSpaceBetween,
            Helpers.crossCenter,
            Styles.buttonContainer,
            Metrics.tinyVerticalMargin,
            Metrics.tinyHorizontalMargin,
            {
              borderBottomColor: Colors.white
            }
          ]}
        >
          <Text
            style={[
              Fonts.normal,
              Metrics.tinyHorizontalMargin,
              { textAlign: "left" }
            ]}
          >
            {Words.en.languageSection.arabic}
          </Text>
          <View style={[Helpers.center, Metrics.tinyHorizontalMargin]}>
            {props.language === languages.ar ? (
              <Image resizeMode={"contain"} resizeMode={"contain"}source={Images.checkMark} style={Styles.icon} />
            ) : (
              <Image resizeMode={"contain"} resizeMode={"contain"}source={Images.checkMarkEmpty} style={Styles.icon} />
            )}
          </View>
        </TouchableOpacity>
      </View>
    );
  }
};

const mapStateToProps = state => {
  return {
    language: state.app.language
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onDispatchSetDeviceLanguage: language =>
      dispatch(setDeviceLanguage(language))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LanguageSection);
