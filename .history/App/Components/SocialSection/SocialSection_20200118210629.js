import React from "react";
import { View, Text, TouchableOpacity, Image, Linking } from "react-native";
import { Helpers, Metrics, Fonts, Colors, Words, Images } from "../../Theme";
import Styles from "./Styles";
import connect from "react-redux/lib/connect/connect";
const SocialSection = props => {
  const _handleFacebook = () => {
    Linking.openURL("https://www.facebook.com/Dr.Abdelrhmanamr/");
  };

  const _handleYoutube = () => {
    Linking.openURL("https://www.youtube.com/user/AbdElRahmanAmrNasrCi");
  };

  const _handleInstagram = () => {
    Linking.openURL("https://www.instagram.com/abdelrhman_amr23/");
  };

  if (props.language === 'en') {
    return (
      <View style={[Styles.container]}>
        <TouchableOpacity
          onPress={_handleFacebook}
          style={[
            Helpers.row,
            Helpers.mainSpaceBetween,
            Helpers.crossCenter,
            Styles.buttonContainer,
            Metrics.tinyVerticalMargin,
            Metrics.tinyHorizontalMargin
          ]}
        >
          <Text
            style={[
              Fonts.normal,
              Metrics.tinyHorizontalMargin,
              { textAlign: "left" }
            ]}
          >
            {Words.en.SocialSection.facebook}
          </Text>
          <View style={[Helpers.center, Metrics.tinyHorizontalMargin]}>
            <Image source={Images.facebookIcon} style={Styles.logo} />
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={_handleInstagram}
          style={[
            Helpers.row,
            Helpers.mainSpaceBetween,
            Helpers.crossCenter,
            Styles.buttonContainer,
            Metrics.tinyVerticalMargin,
            Metrics.tinyHorizontalMargin
          ]}
        >
          <Text
            style={[
              Fonts.normal,
              Metrics.tinyHorizontalMargin,
              { textAlign: "left" }
            ]}
          >
            {Words.en.SocialSection.instagram}
          </Text>
          <View style={[Helpers.center, Metrics.tinyHorizontalMargin]}>
            <Image source={Images.instagramIcon} style={Styles.logo} />
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={_handleYoutube}
          style={[
            Helpers.row,
            Helpers.mainSpaceBetween,
            Helpers.crossCenter,
            Styles.buttonContainer,
            Metrics.tinyVerticalMargin,
            Metrics.tinyHorizontalMargin,
            {
              borderBottomColor: Colors.white
            }
          ]}
        >
          <Text
            style={[
              Fonts.normal,
              Metrics.tinyHorizontalMargin,
              { textAlign: "left" }
            ]}
          >
            {Words.en.SocialSection.youtube}
          </Text>
          <View style={[Helpers.center, Metrics.tinyHorizontalMargin]}>
            <Image source={Images.youtubeIcon} style={Styles.logo} />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
  return (
    <View style={[Styles.container]}>
      <TouchableOpacity
        onPress={_handleFacebook}
        style={[
          Helpers.rowReverse,
          Helpers.mainSpaceBetween,
          Helpers.crossCenter,
          Styles.buttonContainer,
          Metrics.tinyVerticalMargin,
          Metrics.tinyHorizontalMargin
        ]}
      >
        <Text
          style={[
            Fonts.normal,
            Metrics.tinyHorizontalMargin,
            { textAlign: "right" }
          ]}
        >
          {Words.ar.SocialSection.facebook}
        </Text>
        <View style={[Helpers.center, Metrics.tinyHorizontalMargin]}>
          <Image source={Images.facebookIcon} style={Styles.logo} />
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={_handleInstagram}
        style={[
          Helpers.rowReverse,
          Helpers.mainSpaceBetween,
          Helpers.crossCenter,
          Styles.buttonContainer,
          Metrics.tinyVerticalMargin,
          Metrics.tinyHorizontalMargin
        ]}
      >
        <Text
          style={[
            Fonts.normal,
            Metrics.tinyHorizontalMargin,
            { textAlign: "right" }
          ]}
        >
          {Words.ar.SocialSection.instagram}
        </Text>
        <View style={[Helpers.center, Metrics.tinyHorizontalMargin]}>
          <Image source={Images.instagramIcon} style={Styles.logo} />
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={_handleYoutube}
        style={[
          Helpers.rowReverse,
          Helpers.mainSpaceBetween,
          Helpers.crossCenter,
          Styles.buttonContainer,
          Metrics.tinyVerticalMargin,
          Metrics.tinyHorizontalMargin,
          {
            borderBottomColor: Colors.white
          }
        ]}
      >
        <Text
          style={[
            Fonts.normal,
            Metrics.tinyHorizontalMargin,
            { textAlign: "right" }
          ]}
        >
          {Words.ar.SocialSection.youtube}
        </Text>
        <View style={[Helpers.center, Metrics.tinyHorizontalMargin]}>
          <Image source={Images.youtubeIcon} style={Styles.logo} />
        </View>
      </TouchableOpacity>
    </View>
  );
};

const mapStateToProps = state => {
  return {
    language: state.app.language
  };
};

export default connect(mapStateToProps, null)(SocialSection);
