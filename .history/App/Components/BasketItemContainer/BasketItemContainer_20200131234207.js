import React from "react";
import { View, Text, Image } from "react-native";
import Styles from "./Styles";
import { connect } from "react-redux";
import { Helpers, Metrics, Fonts, Colors } from "../../Theme";
import SoundDeleteContainer from "../SoundDeleteContainer/SoundDeleteContainer";
const BasketItemContainer = props => {
  const { image, name, delprice, price } = props.item;
  if (props.language === "en") {
    return (
      <View style={Styles.container}>
        <Image resizeMode={"contain"} resizeMode={"contain"}source={{ uri: image }} style={Styles.icon} />
        <View style={Styles.midContainer}>
          <Text style={Styles.title}>{name}</Text>
        </View>
        <View style={Styles.buttonContainer}>
          <SoundDeleteContainer item={props.item} language={props.language}/>
        </View>
      </View>
    );
  }
  return (
    <View style={[Styles.container, { ...Helpers.rowReverse }]}>
      <Image
        source={{ uri: image }}
        style={[
          Styles.icon,
          props.language === "ar"
            ? {
                borderTopRightRadius: 10,
                borderBottomRightRadius: 10,
                borderTopLeftRadius: 0,
                borderBottomLeftRadius: 0
              }
            : null
        ]}
      />
      <View
        style={[
          Styles.midContainer,
          props.language === "ar"
            ? [Helpers.crossEnd, Metrics.tinyHorizontalMargin]
            : null
        ]}
      >
        <Text
          style={[
            Styles.title,
            props.language === "ar" ? { textAlign: "right" } : null
          ]}
        >
          {name}
        </Text>
      </View>
      <View style={Styles.buttonContainer}>
        <SoundDeleteContainer item={props.item} language={props.language}/>
      </View>
    </View>
  );
};

const mapStateToProps = state => {
  return {
    language: state.auth.language
  };
};



export default connect(mapStateToProps, null)(BasketItemContainer);
