import { StyleSheet } from 'react-native'
import { Helpers, Metrics, Fonts, Colors } from '../../Theme'

export default StyleSheet.create({
  container: {
    ...Helpers.fullWidth,
    height: 50,
    flexDirection: 'row',
    backgroundColor: Colors.white,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  error: {
    ...Fonts.normal,
    color: Colors.error,
    marginBottom: Metrics.tiny,
    textAlign: 'center',
  },
  instructions: {
    ...Fonts.normal,
    fontStyle: 'italic',
    marginBottom: Metrics.tiny,
    textAlign: 'center',
  },
  logoContainer: {
    ...Helpers.fullWidth,
    height: 300,
    marginBottom: 25,
  },
  result: {
    ...Fonts.normal,
    marginBottom: Metrics.tiny,
    textAlign: 'center',
  },
  text: {
    ...Fonts.normal,
    marginBottom: Metrics.tiny,
    textAlign: 'center',
  },
  title: {
    ...Fonts.h4,
  },
  basketContainer: {
    ...Helpers.center,
    position: 'absolute',
    top: 0,
    left:15,
    borderRadius: 50,
    width: 18,
    height: 18,
    backgroundColor: Colors.primary,
  },
  basketNotificationText: {
    ...Fonts.small,
    color: Colors.white
  }
})
