import React, { useContext, useState, useEffect } from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import NavigationService from "../../Services/NavigationService";
import Style from "./Styles";
import { NavigationContext } from "react-navigation";

import { Colors, Metrics, Helpers, Words, Images } from "../../Theme";

import { connect } from "react-redux";
import { setRouteName } from "../../Actions/App/Actions";
const HeaderComponent = props => {
  const navigation = useContext(NavigationContext);
  const [title, setTitle] = useState(navigation.state.routeName);

  useEffect(() => {
    _helperArabic(title);
    props.onDispatchSetRouteName(title);
  });

  const _navigateToBasket = () => {
    props.onDispatchSetRouteName("Basket");
    NavigationService.navigate("Basket");
  };

  const _navigateToNotification = () => {
    props.onDispatchSetRouteName("Notification");
    NavigationService.navigate("Notification");
  };

  const _helperArabic = title => {
    if (props.language === "ar") {
      if (title === "Home") {
        setTitle(Words.ar.header.home);
        console.log("title", title);
      } else if (title === "Search") {
        setTitle(Words.ar.header.search);
        console.log("title", title);
      } else if (title === "Playlist") {
        setTitle(Words.ar.header.playlist);
      } else if (title === "Library") {
        setTitle(Words.ar.header.library);
      } else if (title === "Settings") {
        setTitle(Words.ar.header.settings);
      }
    } else {
      setTitle(navigation.state.routeName);
    }
  };
  if (props.language === "en") {
    return (
      <View style={[Style.container]}>
        <View style={[Metrics.horizontalMargin]}>
          <TouchableOpacity
            style={[Metrics.horizontalMargin]}
            onPress={_navigateToBasket}
          >
            <Image
              resizeMode={"contain"}
              resizeMode={"contain"}
              source={Images.cart}
              style={Style.icon}
            />
            {props.basket.length > 0 ? (
              <View style={Style.basketContainer}>
                <Text style={Style.basketNotificationText}>
                  {props.basket.length}
                </Text>
              </View>
            ) : null}
          </TouchableOpacity>
        </View>
        <View style={[Helpers.center, Metrics.smallHorizontalMargin]}>
          <Text style={Style.title}>{title}</Text>
        </View>
       <View/>
      </View>
    );
  }
  return (
    <View style={[Style.container, Helpers.rowReverse]}>
      <View>
        <TouchableOpacity
          style={[Metrics.horizontalMargin]}
          onPress={_navigateToBasket}
        >
          <Image
            resizeMode={"contain"}
            resizeMode={"contain"}
            source={Images.cart}
            style={Style.icon}
          />
          {props.basket.length > 0 ? (
            <View style={Style.basketContainer}>
              <Text style={Style.basketNotificationText}>
                {props.basket.length}
              </Text>
            </View>
          ) : null}
        </TouchableOpacity>
      </View>
      <View style={[, Metrics.smallHorizontalMargin]}>
        <Text style={Style.title}>{title}</Text>
      </View>
      <View style={[, Metrics.smallHorizontalMargin]} />
    </View>
  );
};

const mapStateToProps = state => {
  return {
    language: state.auth.language,
    basket: state.app.basket
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onDispatchSetRouteName: val => dispatch(setRouteName(val))
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(HeaderComponent);
