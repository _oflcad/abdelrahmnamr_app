import React from "react";
import { View, TouchableOpacity, Image } from "react-native";
import NavigationService from "../../Services/NavigationService";
import Styles from "./Styles";
import { Helpers, Metrics, Colors, Images } from "../../Theme";
import { showMiniPlayer } from "../../Actions/Media/Actions";
import { connect } from "react-redux";
const PlayerHeader = ({ onDispatchShowMiniPlayer, language }) => {
  const _navigateBack = () => {
    onDispatchShowMiniPlayer();
    NavigationService.navigate("Playlist");
  };

  const _navigateToNotification = () => {
    onDispatchShowMiniPlayer();
    NavigationService.navigate("Notification");
  };

  return (
    <View
      style={[
        Styles.container,
        Metrics.tinyVerticalMargin,
        language === "ar" ? { flexDirection: "row-reverse" } : null
      ]}
    >
      <View style={[Helpers.rowCross]}>
        <TouchableOpacity
          style={[Metrics.horizontalMargin]}
          onPress={_navigateBack}
        >
          <Image
            source={language === "en" ? Images.leftArrow : Images.rightArrow}
            style={{ width: 25, height: 25 }}
          />
        </TouchableOpacity>
      </View>
      <View />
      <View style={[Helpers.rowCross]}>
        <TouchableOpacity
          style={[Metrics.smallHorizontalMargin]}
          onPress={_navigateToNotification}
        >
          <Image
            source={Images.notification}
            style={{ width: 25, height: 25 }}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const mapStateToProps = state => {
  return {
    language: state.app.language
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onDispatchShowMiniPlayer: () => dispatch(showMiniPlayer())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PlayerHeader);
