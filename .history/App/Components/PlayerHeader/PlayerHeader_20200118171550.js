import React from "react";
import { View, TouchableOpacity } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import NavigationService from "../../Services/NavigationService";
import Styles from "./Styles";
import { Helpers, Metrics, Colors } from "../../Theme";
import { showMiniPlayer } from "../../Actions/Media/Actions";
import { connect } from "react-redux";
const PlayerHeader = ({ onDispatchShowMiniPlayer, language }) => {
  const _navigateBack = () => {
    onDispatchShowMiniPlayer();
    NavigationService.navigate("Playlist");
  };

  const _navigateToNotification = () => {
    onDispatchShowMiniPlayer();
    NavigationService.navigate("Notification");
  };

  return (
    <View
      style={[
        Styles.container,
        Metrics.tinyVerticalMargin,
        language === "ar" ? { flexDirection: "row-reverse" } : null
      ]}
    >
      <View style={[Helpers.rowCross]}>
        <TouchableOpacity
          style={[Metrics.horizontalMargin]}
          onPress={_navigateBack}
        >
          <Ionicons
            name={language=== "en" ? "ios-arrow-dropleft-circle" : "ios-arrow-dropright-circle" }
            size={25}
            color={Colors.black}
          />
        </TouchableOpacity>
      </View>
      <View />
      <View style={[Helpers.rowCross]}>
        <TouchableOpacity
          style={[Metrics.smallHorizontalMargin]}
          onPress={_navigateToNotification}
        >
          <Ionicons name="ios-notifications" size={25} color={Colors.black} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const mapStateToProps = state => {
  return {
    language: state.app.language
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onDispatchShowMiniPlayer: () => dispatch(showMiniPlayer())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PlayerHeader);
