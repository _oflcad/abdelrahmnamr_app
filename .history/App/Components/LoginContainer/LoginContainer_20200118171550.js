import React, { useState } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import Styles from "./Styles";
import { Ionicons } from "@expo/vector-icons";
import { Colors, Fonts, Words, Helpers } from "../../Theme";
import NavigationService from "../../Services/NavigationService";
import UUID from "uuid-js";

const LoginContainer = props => {
  const [empty, setEmpty] = useState(UUID.create(4));
  const _handleNavigation = () => {
    props.isAuthenticated
      ? NavigationService.navigate("Profile")
      : NavigationService.navigate("SignIn");
  };

  if (props.isAuthenticated) {
    if (props.language === "en" && props.user) {
      return (
        <TouchableOpacity style={Styles.container} onPress={_handleNavigation}>
          <View style={Styles.thumbnailContainer}>
            <Ionicons name="ios-contacts" size={30} color={Colors.black} />
          </View>
          <View style={Styles.midContainer}>
            {props.user !== null ? (
              <Text style={[Fonts.normal]}>{props.user.username} </Text>
            ) : (
              <Text style={[Fonts.normal]}>143148494</Text>
            )}

            {props.user !== null ? (
              <Text style={[Fonts.subTitle]}>
                {props.user.firstname + " " + props.user.lastname}
              </Text>
            ) : (
              <Text style={[Fonts.subTitle]}>143148494</Text>
            )}
          </View>
          <View style={Styles.thumbnailContainer}>
            <Ionicons
              name="ios-arrow-dropright-circle"
              size={30}
              color={Colors.black}
            />
          </View>
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          style={[Styles.container, { flexDirection: "row-reverse" }]}
          onPress={_handleNavigation}
        >
          <View style={Styles.thumbnailContainer}>
            <Ionicons name="ios-contacts" size={30} color={Colors.black} />
          </View>

          <View
            style={[
              { flex: 2 },
              Helpers.column,
              Helpers.crossEnd,
              Helpers.mainCenter
            ]}
          >
            {props.user !== null ? (
              <Text style={[Fonts.normal]}>{props.user.username}</Text>
            ) : (
              <Text style={[Fonts.normal]}>143148494</Text>
            )}

            {props.user !== null ? (
              <Text style={[Fonts.subTitle]}>
                {props.user.firstname + " " + props.user.lastname}
              </Text>
            ) : (
              <Text style={[Fonts.subTitle]}>143148494</Text>
            )}
          </View>
          <View style={Styles.thumbnailContainer}>
            <Ionicons
              name="ios-arrow-dropleft-circle"
              size={30}
              color={Colors.black}
            />
          </View>
        </TouchableOpacity>
      );
    }
  } else {
    if (props.language === "en") {
      return (
        <TouchableOpacity style={Styles.container} onPress={_handleNavigation}>
          <View style={Styles.thumbnailContainer}>
            <Ionicons name="ios-contacts" size={30} color={Colors.black} />
          </View>
          <View style={Styles.midContainer}>
            <Text style={[Fonts.normal]}>{Words.en.singInMessage}</Text>
            <Text style={[Fonts.subTitle]}>{Words.en.accesAccountMessage}</Text>
          </View>
          <View style={Styles.thumbnailContainer}>
            <Ionicons
              name="ios-arrow-dropright-circle"
              size={30}
              color={Colors.black}
            />
          </View>
        </TouchableOpacity>
      );
    }
    return (
      <TouchableOpacity
        style={[Styles.container, Helpers.rowReverse]}
        onPress={_handleNavigation}
      >
        <View style={Styles.thumbnailContainer}>
          <Ionicons name="ios-contacts" size={30} color={Colors.black} />
        </View>
        <View style={[Styles.midContainer, Helpers.crossEnd]}>
          <Text style={[Fonts.normal, { textAlign: "right", fontSize: 14 }]}>
            {Words.ar.singInMessage}
          </Text>
          <Text style={[Fonts.subTitle, { textAlign: "right" }]}>
            {Words.ar.accesAccountMessage}
          </Text>
        </View>
        <View style={Styles.thumbnailContainer}>
          <Ionicons
            name="ios-arrow-dropleft-circle"
            size={30}
            color={Colors.black}
          />
        </View>
      </TouchableOpacity>
    );
  }
};

const mapStateToProps = state => {
  return {
    language: state.app.language,
    isAuthenticated: state.auth.isAuthenticated,
    user: state.auth.user
  };
};
const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
