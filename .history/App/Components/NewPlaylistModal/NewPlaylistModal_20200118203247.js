import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Modal,
  KeyboardAvoidingView,
  Image
} from "react-native";
import Styles from "./Styles";
import {
  Helpers,
  Fonts,
  Words,
  Metrics,
  Colors,
  ApplicationStyles,
  Images
} from "../../Theme";
import { connect } from "react-redux";
import {
  createPlaylist,
  closePlaylistModal
} from "../../Actions/Media/Actions";
import UUID from "uuid-js";
import moment from "moment";
import { Ionicons } from "@expo/vector-icons";
import Reactotron from "reactotron-react-native";

const NewPlaylistModal = props => {
  const [title, setTitle] = useState("");
  const [error, setError] = useState(null);

  const _handleCreatePlaylist = () => {
    let playlist = {
      id: UUID.create(4),
      title: title,
      songs: [],
      downloadedSongs: [],
      createdAt: moment(),
      downloaded: false
    };
    if (title && title !== "") {
      setError(null);
      props.onDispatchCreatePlaylist(playlist);
      props.onDispatchCloseModal();
    } else {
      setError(Words.en.addPlaylistError);
    }
  };

  const _handleCloseModal = () => {
    props.onDispatchCloseModal();
    setError(null);
  };

  return (
    <KeyboardAvoidingView>
      <Modal
        style={Styles.wrapper}
        animationType={"slide"}
        transparent
        visible={props.isAddPlaylistModalVisible}
        hasBackdrop
        backdropColor={Colors.grey200}
        useNativeDriver
        onBackdropPress={props.onDispatchCloseModal}
      >
        <View style={Styles.container}>
          <TouchableOpacity
            activeOpacity={1}
            style={[ApplicationStyles.modalBackground]}
            onPress={_handleCloseModal}
          ></TouchableOpacity>
          <View style={[ApplicationStyles.playlistInfoModal]}>
            <View style={Styles.topContainer}>
              <Image source={Images.musicalNotes} style={Styles.icon} />
              <Text style={[Fonts.h2, Metrics.tinyVerticalMargin]}>
                {title}
              </Text>
              <Text style={[Fonts.subTitle]}>Playlist</Text>
            </View>

            <View style={Styles.bottomContainer}>
              <TextInput
                style={Styles.input}
                onChangeText={val => setTitle(val)}
                placeholder={
                  props.language === "en"
                    ? Words.en.addPlaylistPlaceholder
                    : Words.ar.addPlaylistPlaceholder
                }
                placeholderTextColor={Colors.grey150}
                underlineColorAndroid={Colors.transparent}
              />
              {error ? (
                <Text
                  style={[
                    Metrics.horizontalMargin,
                    Metrics.verticalMargin,
                    Styles.error
                  ]}
                >
                  {error}
                </Text>
              ) : null}

              <TouchableOpacity
                onPress={_handleCreatePlaylist}
                style={Styles.buttonContainer}
              >
                <Image source={Images.musicalNotes} style={Styles.smallIcon} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </KeyboardAvoidingView>
  );
};

const mapStateToProps = state => {
  return {
    isAddPlaylistModalVisible: state.media.isAddPlaylistModalVisible,
    language: state.app.language
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onDispatchCreatePlaylist: obj => dispatch(createPlaylist(obj)),
    onDispatchCloseModal: () => dispatch(closePlaylistModal())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NewPlaylistModal);
