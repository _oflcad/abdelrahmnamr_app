import { StyleSheet } from "react-native";
import { Metrics, Fonts, Colors, Helpers } from "../../Theme";

export default StyleSheet.create({
  wrapper: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    ...Metrics.smallHorizontalMargin,
    flexDirection: "column",
    backgroundColor: Colors.white
  },
  container: {
    width: "100%",
    height: "100%",
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  thumbnail: {
    width: 50,
    height: 50,
    borderRadius: 10,
  },
  topContainer: {
    width: "100%",
    height: "60%",
    flexDirection: "column",
    ...Helpers.center,
    backgroundColor: Colors.modalTop,
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10
  },
  bottomContainer: {
    ...Helpers.column,
    width: "100%",
    height: "50%",

    alignItems: "center",
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10
  },
  input: {
    ...Metrics.verticalMargin,
    width: Metrics.width - 100,
    height: 40,
    color: Colors.black,
    textAlign: "center",
    borderRadius: 10,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: Colors.grey200,
  },
  textContainer: {
    flex: 1,
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "space-around"
  },
  error: {
    ...Fonts.normal,
    color: Colors.error,
    marginBottom: Metrics.tiny,
    textAlign: "center"
  },
  buttonContainer: {
    ...Helpers.center,
    ...Metrics.verticalMargin,
    width: Metrics.width - 100,
    height: 40,
    backgroundColor: Colors.black,
    borderRadius: 10
  },
  buttonText: {
    ...Fonts.normal,
    textAlign: 'center',
    color: Colors.white
  },
  background: {
    width: '100%',
    height: '100%',
  },
  borderForButton: {
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: Colors.grey200
  },
  deleteContainer : {
    ...Helpers.column,
    width: "100%",
    height: "40%",
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10
  },
  deleteButtonContainer: {
    ...Helpers.row,
    flex:2,
    alignItems: "center",
    justifyContent: "space-around",
  },
  icon: {
    width: 40,
    height: 40,
  },
  smallIcon: {
    width: 25,
    height: 25,
  },
});
