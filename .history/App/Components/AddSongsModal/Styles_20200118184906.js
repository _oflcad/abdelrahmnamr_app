import { StyleSheet } from "react-native";
import { Metrics, Fonts, Colors, Helpers } from "../../Theme";
const MODAL_HEIGHT = Metrics.height - (Metrics.height / 3 )
export default StyleSheet.create({
  wrapper: {
    flex: 1,
    ...Metrics.smallHorizontalMargin,
    flexDirection: "column",
    backgroundColor: Colors.grey150,
    justifyContent: "center",
    alignItems: "center",
    ...Metrics.smallHorizontalMargin
  },
  container: {
    width: "100%",
    height: "100%",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 20
  },
  thumbnail: {
    width: 50,
    height: 50,
    borderRadius: 10,
  },
  input: {
    width: Metrics.width - 100,
    height: 50,
    color: Colors.black,
    textAlign: "center",
    borderRadius: 10,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: Colors.grey200,
  },
  error: {
    ...Fonts.normal,
    color: Colors.error,
    marginBottom: Metrics.tiny,
    textAlign: "center"
  },
  buttonContainer: {
    width: Metrics.width - 180,
    height: 50,
    borderRadius: 20,
    backgroundColor: Colors.black,
    ...Helpers.center
  },
  buttonText: {
    ...Fonts.h4,
    color: Colors.white
  },
  background: {
    width: '100%',
    height: '100%',
  },
  AddSongsModal: {
    position: 'absolute',
    top:0,
    right: 0,
    width: 40,
    height: 40,
    borderRadius: 50,
    ...Helpers.center
  },
  modalSize: {
    width: MODAL_HEIGHT,
    height: MODAL_HEIGHT
  }
});
