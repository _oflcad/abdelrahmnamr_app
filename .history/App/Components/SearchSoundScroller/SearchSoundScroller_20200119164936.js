import React, { Component } from "react";
import {
  View,
  Text,
  FlatList,
  ActivityIndicator,
  TouchableOpacity,
  Image
} from "react-native";
import Style from "./Styles";
import { connect } from "react-redux";
import {
  Helpers,
  Colors,
  Metrics,
  Fonts,
  Words,
  Images
} from "../../Theme";
import SongContainer from "../SongContainer/SongContainer";
import PlaySongContainer from "../PlaySongContainer/PlaySongContainer";
import { getUserAudios, fetchAllAudios } from "../../Actions/Media/Actions";
class SoundScroller extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isFetchingAllAudios: false,
      isFetchingUserAudios: false
    };
  }

  _renderItem = ({ item }) => {
    return <SongContainer item={item} />;
  };

  _renderItemBought = ({ item }) => {
    return <PlaySongContainer item={item} />;
  };

  _refreshData = () => {
    this.setState({ isFetchingUserAudios: true });
    setTimeout(() => {
      this.props.onDispatchFetchUserAudios();
      this.setState({ isFetchingUserAudios: false });
    }, 1500);
  };

  _renderRefresh = () => {
    return (
      <View style={Style.wrapper}>
        <TouchableOpacity
          onPress={() => this._refreshData()}
          style={[Style.buttonContainer, Metrics.verticalMargin]}
        >
          <Image resizeMode={"contain"} resizeMode={"contain"}source={Images.refresh} style={{width: 25, height: 25}} />
        </TouchableOpacity>
      </View>
    );
  };

  _refreshAllAudios = () => {
    this.setState({ isFetchingAllAudios: true });
    setTimeout(() => {
      this.props.onDispatchFetchAllAudios();
      this.setState({ isFetchingAllAudios: false });
    }, 1500);
  };

  render() {
    const { userAudios, language, latestAudios } = this.props;
    if (language === "en") {
      return (
        <View style={[Helpers.fill]}>
          <Text
            style={[
              Fonts.h5,
              Metrics.mediumHorizontalMargin,
              Metrics.smallVerticalMargin
            ]}
          >
            {Words.en.purshased}
          </Text>
          <FlatList
           
            showsVerticalScrollIndicator={false}
            style={[Metrics.verticalMargin]}
            contentContainerStyle={[
              Helpers.mainCenter,
              Metrics.tinyVerticalMargin,
              { paddingBottom: 60 }
            ]}
            contentInset={{ top: 0, bottom: 20, left: 0, right: 0 }}
            contentInsetAdjustmentBehavior="automatic"
            data={userAudios ? userAudios : [] }
            renderItem={this._renderItemBought}
            keyExtractor={(item, index) => "key" + item.name}
            ListEmptyComponent={this._renderRefresh}
          />

          <Text
            style={[
              Fonts.h5,
              Metrics.mediumHorizontalMargin,
              Metrics.smallVerticalMargin
            ]}
          >
            {Words.en.allAudios}
          </Text>

          <FlatList
           
            showsVerticalScrollIndicator={false}
            style={[Metrics.smallVerticalMargin]}
            contentContainerStyle={[
              Helpers.mainCenter,
              Metrics.tinyVerticalMargin,
              { paddingBottom: 60 }
            ]}
            data={latestAudios}
            renderItem={this._renderItem}
            contentInset={{ top: 0, bottom: 20, left: 0, right: 0 }}
            contentInsetAdjustmentBehavior="automatic"
            keyExtractor={(item, index) => "key" + item.name}
            ListEmptyComponent={
              <ActivityIndicator size={"large"} color={Colors.black} />
            }
          />
        </View>
      );
    }
    return (
      <View style={[Helpers.fill]}>
        <View style={[Helpers.crossEnd]}>
          <Text
            style={[
              Fonts.h5,
              Metrics.mediumHorizontalMargin,
              Metrics.smallVerticalMargin
            ]}
          >
            {Words.ar.purshased}
          </Text>
        </View>
        <FlatList
          
          showsVerticalScrollIndicator={false}
          style={[Metrics.smallVerticalMargin]}
          contentContainerStyle={[
            Helpers.mainCenter,
            Metrics.tinyVerticalMargin,
            { paddingBottom: 60 }
          ]}
          data={userAudios}
          contentInset={{ top: 0, bottom: 20, left: 0, right: 0 }}
          contentInsetAdjustmentBehavior="automatic"
          renderItem={this._renderItemBought}
          keyExtractor={(item, index) => "key" + item.name}
          ListEmptyComponent={this._renderRefresh}
          />
          
        <View />
        <View style={[Helpers.crossEnd]}>
          <Text
            style={[
              Fonts.h5,
              Metrics.mediumHorizontalMargin,
              Metrics.smallVerticalMargin
            ]}
          >
            {Words.ar.allAudios}
          </Text>
        </View>
        <FlatList
          
          showsVerticalScrollIndicator={false}
          style={[Metrics.smallVerticalMargin]}
          contentContainerStyle={[
            Helpers.mainCenter,
            Metrics.tinyVerticalMargin,
            { paddingBottom: 60 }
          ]}
          data={latestAudios}
          renderItem={this._renderItem}
          contentInset={{ top: 0, bottom: 20, left: 0, right: 0 }}
          contentInsetAdjustmentBehavior="automatic"
          keyExtractor={(item, index) => "key" + item.name}
          ListEmptyComponent={
            <ActivityIndicator size={"large"} color={Colors.black} />
          }
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  userAudios: state.media.userAudios,
  language: state.app.language,
  latestAudios: state.media.latestAudios
});

const mapDispatchToProps = dispatch => {
  return {
    onDispatchFetchUserAudios: () => dispatch(getUserAudios()),
    onDispatchFetchAllAudios: () => dispatch(fetchAllAudios())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SoundScroller);
