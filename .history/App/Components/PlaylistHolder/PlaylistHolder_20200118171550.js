import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { Helpers, Fonts, Metrics, Colors, Words } from "../../Theme";
import Styles from "./Styles";
import moment from "moment";
import NavigationService from "../../Services/NavigationService";
import { Ionicons } from "@expo/vector-icons";
import { connect } from "react-redux";
import { setPlaylist } from "../../Actions/Media/Actions";
const PlaylistHolder = props => {
  const { title, songs, createdAt, downloaded } = props.item;

  const _handleNav = () => {
    props.onDispatchSetItem(props.item);
    NavigationService.navigate("PlaylistInfo");
  };

  if (props.language === "en") {
    return (
      <TouchableOpacity style={Styles.container} onPress={_handleNav}>
        <View style={Styles.topContainer}>
          <Text style={[Fonts.h2]}>{title}</Text>
          {downloaded ? (
            <View
              style={[
                Helpers.center,
                Metrics.horizontalMargin,
                Metrics.tinyVerticalMargin
              ]}
            >
              <Ionicons
                name="ios-arrow-dropdown-circle"
                color={Colors.success}
                size={25}
              />
            </View>
          ) : (
            <View
              style={[
                Helpers.center,
                Metrics.horizontalMargin,
                Metrics.tinyVerticalMargin
              ]}
            >
              <Ionicons
                name="ios-arrow-dropdown-circle"
                color={Colors.grey100}
                size={25}
              />
            </View>
          )}
        </View>

        <View
          style={[
            Helpers.row,
            Helpers.mainStart,
            Helpers.crossCenter,
            Metrics.smallVerticalMargin
          ]}
        >
          <View
            style={[
              Helpers.row,
              Helpers.mainSpaceAround,
              Helpers.crossCenter,
              Metrics.horizontalMargin,
              Metrics.smallHorizontalPadding
            ]}
          >
            <Ionicons
              name="ios-arrow-dropright-circle"
              color={Colors.black}
              size={25}
            />
            <Text style={[Fonts.subTitle, Metrics.smallHorizontalMargin]}>
              {songs.length} audios
            </Text>
          </View>
          <View style={[Helpers.crossEnd, Metrics.horizontalMargin]}>
            <Text
              style={[
                Fonts.label,
                { color: Colors.grey150, textAlign: "right" }
              ]}
            >
              {moment(createdAt).fromNow()}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
  return (
    <TouchableOpacity style={Styles.container} onPress={_handleNav}>
      <View style={[Styles.topContainer, { flexDirection: "row-reverse" }]}>
        <Text style={[Fonts.h2]}>{title}</Text>
        {downloaded ? (
          <View
            style={[
              Helpers.center,
              Metrics.horizontalMargin,
              Metrics.tinyVerticalMargin
            ]}
          >
            <Ionicons
              name="ios-arrow-dropdown-circle"
              color={Colors.success}
              size={25}
            />
          </View>
        ) : (
          <View
            style={[
              Helpers.center,
              Metrics.horizontalMargin,
              Metrics.tinyVerticalMargin
            ]}
          >
            <Ionicons
              name="ios-arrow-dropdown-circle"
              color={Colors.grey100}
              size={25}
            />
          </View>
        )}
      </View>

      <View
        style={[
          Helpers.rowReverse,
          Helpers.mainStart,
          Helpers.crossCenter,
          Metrics.smallVerticalMargin
        ]}
      >
        <View
          style={[
            Helpers.row,
            Helpers.mainSpaceAround,
            Helpers.crossCenter,
            Metrics.horizontalMargin,
            Metrics.smallHorizontalPadding
          ]}
        >
          <Ionicons
            name="ios-arrow-dropleft-circle"
            color={Colors.black}
            size={25}
          />
          <Text style={[Fonts.subTitle, Metrics.smallHorizontalMargin]}>
            {songs.length} audios
          </Text>
        </View>
        <View
          style={[Helpers.crossEnd, Helpers.crossEnd, Metrics.horizontalMargin]}
        >
          <Text
            style={[Fonts.label, { color: Colors.grey150, textAlign: "right" }]}
          >
            {moment(createdAt).fromNow()}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const mapStateToProps = state => {
  return {
    language: state.app.language
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onDispatchSetItem: item => dispatch(setPlaylist(item))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PlaylistHolder);
