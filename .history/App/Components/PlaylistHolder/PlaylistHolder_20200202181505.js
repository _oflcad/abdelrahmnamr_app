import React from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import { Helpers, Fonts, Metrics, Colors, Words, Images } from "../../Theme";
import Styles from "./Styles";
import moment from "moment";
import NavigationService from "../../Services/NavigationService";
import { connect } from "react-redux";
import { setPlaylist } from "../../Actions/Media/Actions";
import _ from "lodash";
import reactotron from "reactotron-react-native";
class PlaylistHolder extends React.Component {

  constructor(props) {
    super(props)
  
    this._handleNav = this._handleNav.bind(this);
  }
  
  
  componentDidUpdate(prevProps) {
    reactotron.log('PROPS DOWNLOADED FROM PLAYLIST HOLDER', !_.isEqual(prevProps.item.downloaded, this.props.item.downloaded))
    if(!_.isEqual(prevProps.item.downloaded, this.props.item.downloaded)) {
      return true;
    }
  }

  _handleNav = () => {
    const {item} = this.props;
    this.props.onDispatchSetItem(item);
    NavigationService.navigate("PlaylistInfo");
  };

  render() {
    
    const { title, songs, createdAt, downloaded } = this.props.item;
    
      return (
        <TouchableOpacity style={Styles.container} onPress={this._handleNav}>
          <View style={Styles.topContainer}>
            <Text style={[Fonts.h2]}>{title}</Text>
            {downloaded ? (
              <View
                style={[
                  Helpers.center,
                  Metrics.horizontalMargin,
                  Metrics.tinyVerticalMargin
                ]}
              >
                <Image
                  source={Images.offlineAudio}
                  style={{ width: 25, height: 25 }}
                />
              </View>
            ) : (
              <View
                style={[
                  Helpers.center,
                  Metrics.horizontalMargin,
                  Metrics.tinyVerticalMargin
                ]}
              >
                <Image
                  source={Images.onlineAudio}
                  style={{ width: 25, height: 25 }}
                />
              </View>
            )}
          </View>
  
          <View
            style={[
              Helpers.row,
              Helpers.mainStart,
              Helpers.crossCenter,
              Metrics.smallVerticalMargin
            ]}
          >
            <View
              style={[
                Helpers.row,
                Helpers.mainSpaceAround,
                Helpers.crossCenter,
                Metrics.horizontalMargin,
                Metrics.smallHorizontalPadding
              ]}
            >
              <Image
                source={Images.rightArrow}
                style={{ width: 25, height: 25 }}
              />
              <Text style={[Fonts.subTitle, Metrics.smallHorizontalMargin]}>
                {songs.length} audios
              </Text>
            </View>
            <View style={[Helpers.crossEnd, Metrics.horizontalMargin]}>
              <Text
                style={[
                  Fonts.label,
                  { color: Colors.grey150, textAlign: "right" }
                ]}
              >
                {moment(createdAt).fromNow()}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      );
    }
}

const mapStateToProps = state => {
  return {
    language: state.auth.language
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onDispatchSetItem: item => dispatch(setPlaylist(item))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PlaylistHolder);
