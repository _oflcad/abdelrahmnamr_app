import React from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import { Helpers, Fonts, Metrics, Colors, Words, Images } from "../../Theme";
import Styles from "./Styles";
import moment from "moment";
import NavigationService from "../../Services/NavigationService";
import { connect } from "react-redux";
import { setPlaylist } from "../../Actions/Media/Actions";
const PlaylistHolder = props => {
  const { title, songs, createdAt, downloaded } = props.item;

  const _handleNav = () => {
    props.onDispatchSetItem(props.item);
    NavigationService.navigate("PlaylistInfo");
  };

  if (props.language === "en") {
    return (
      <TouchableOpacity style={Styles.container} onPress={_handleNav}>
        <View style={Styles.topContainer}>
          <Text style={[Fonts.h2]}>{title}</Text>
          {downloaded ? (
            <View
              style={[
                Helpers.center,
                Metrics.horizontalMargin,
                Metrics.tinyVerticalMargin
              ]}
            >
              <Image
                source={Images.offlineAudio}
                style={{ width: 25, height: 25 }}
              />
            </View>
          ) : (
            <View
              style={[
                Helpers.center,
                Metrics.horizontalMargin,
                Metrics.tinyVerticalMargin
              ]}
            >
              <Image
                source={Images.onlineAudio}
                style={{ width: 25, height: 25 }}
              />
            </View>
          )}
        </View>

        <View
          style={[
            Helpers.row,
            Helpers.mainStart,
            Helpers.crossCenter,
            Metrics.smallVerticalMargin
          ]}
        >
          <View
            style={[
              Helpers.row,
              Helpers.mainSpaceAround,
              Helpers.crossCenter,
              Metrics.horizontalMargin,
              Metrics.smallHorizontalPadding
            ]}
          >
            <Image
              source={Images.rightArrow}
              style={{ width: 25, height: 25 }}
            />
            <Text style={[Fonts.subTitle, Metrics.smallHorizontalMargin]}>
              {songs.length} audios
            </Text>
          </View>
          <View style={[Helpers.crossEnd, Metrics.horizontalMargin]}>
            <Text
              style={[
                Fonts.label,
                { color: Colors.grey150, textAlign: "right" }
              ]}
            >
              {moment(createdAt).fromNow()}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
  return (
    <TouchableOpacity style={Styles.container} onPress={_handleNav}>
      <View style={[Styles.topContainer, { flexDirection: "row-reverse" }]}>
        <Text style={[Fonts.h2]}>{title}</Text>
        {downloaded ? (
          <View
            style={[
              Helpers.center,
              Metrics.horizontalMargin,
              Metrics.tinyVerticalMargin
            ]}
          >
            <Image
              source={Images.offlineAudio}
              style={{ width: 25, height: 25 }}
            />
          </View>
        ) : (
          <View
            style={[
              Helpers.center,
              Metrics.horizontalMargin,
              Metrics.tinyVerticalMargin
            ]}
          >
            <Image
              source={Images.onlineAudio}
              style={{ width: 25, height: 25 }}
            />
          </View>
        )}
      </View>

      <View
        style={[
          Helpers.rowReverse,
          Helpers.mainStart,
          Helpers.crossCenter,
          Metrics.smallVerticalMargin
        ]}
      >
        <View
          style={[
            Helpers.row,
            Helpers.mainSpaceAround,
            Helpers.crossCenter,
            Metrics.horizontalMargin,
            Metrics.smallHorizontalPadding
          ]}
        >
          <Image resizeMode={"contain"}source={Images.leftArrow} style={{ width: 25, height: 25 }} />
          <Text style={[Fonts.subTitle, Metrics.smallHorizontalMargin]}>
            {songs.length} audios
          </Text>
        </View>
        <View
          style={[Helpers.crossEnd, Helpers.crossEnd, Metrics.horizontalMargin]}
        >
          <Text
            style={[Fonts.label, { color: Colors.grey150, textAlign: "right" }]}
          >
            {moment(createdAt).fromNow()}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const mapStateToProps = state => {
  return {
    language: state.app.language
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onDispatchSetItem: item => dispatch(setPlaylist(item))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PlaylistHolder);
