import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { logoutUser } from "../../Actions/Auth/Actions";
import { connect } from "react-redux";
import { Helpers, Metrics, Fonts, Words, Colors } from "../../Theme";
import Styles from "./Styles";
import { Ionicons, AntDesign } from "@expo/vector-icons";

const Logout = ({ onDispatchLogoutUser, language, isAuthenticated }) => {
  const _handleLogOutUser = () => {
    onDispatchLogoutUser();
  };
  if (isAuthenticated) {
    return (
      <View style={[Styles.container, Helpers.center]}>
        <TouchableOpacity
          onPress={_handleLogOutUser}
          style={[
            language === "en" ? Helpers.row : Helpers.rowReverse,
            Helpers.mainSpaceBetween,
            Helpers.crossCenter,
            Styles.buttonContainer,
            Metrics.tinyHorizontalMargin,
            Metrics.tinyVerticalMargin
          ]}
        >
          <Text
            style={[
              Fonts.normal,
              language === "en"
                ? { textAlign: "left" }
                : { textAlign: "right" },
              Metrics.tinyHorizontalMargin
            ]}
          >
            {language === "en" ? Words.en.logout : Words.ar.logout}
          </Text>
          <View style={[Metrics.tinyHorizontalMargin, Helpers.center]}>
            <Ionicons name="ios-log-out" size={25} color={Colors.black} />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
  return <View />;
};

const mapStateToProps = state => ({
  language: state.app.language,
  isAuthenticated: state.auth.isAuthenticated
});

const mapDispatchToProps = dispatch => {
  return {
    onDispatchLogoutUser: () => dispatch(logoutUser())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Logout);
