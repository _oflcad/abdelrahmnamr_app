import React from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import { logoutUser } from "../../Actions/Auth/Actions";
import { connect } from "react-redux";
import { Helpers, Metrics, Fonts, Words, Colors, Images } from "../../Theme";
import Styles from "./Styles";


const Logout = ({ onDispatchLogoutUser, language, isAuthenticated }) => {
  const _handleLogOutUser = () => {
    onDispatchLogoutUser();
  };
  if (isAuthenticated) {
    return (
      <View style={[Styles.container, Helpers.center]}>
        <TouchableOpacity
          onPress={_handleLogOutUser}
          style={[
            language === "en" ? Helpers.row : Helpers.rowReverse,
            Helpers.mainSpaceBetween,
            Helpers.crossCenter,
            Styles.buttonContainer,
            Metrics.tinyHorizontalMargin,
            Metrics.tinyVerticalMargin
          ]}
        >
          <Text
            style={[
              Fonts.normal,
              language === "en"
                ? { textAlign: "left" }
                : { textAlign: "right" },
              Metrics.tinyHorizontalMargin
            ]}
          >
            {language === "en" ? Words.en.logout : Words.ar.logout}
          </Text>
          <View style={[Metrics.tinyHorizontalMargin, Helpers.center]}>
            <Image source={Images.logout} style={Styles.icon} />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
  return <View />;
};

const mapStateToProps = state => ({
  language: state.app.language,
  isAuthenticated: state.auth.isAuthenticated
});

const mapDispatchToProps = dispatch => {
  return {
    onDispatchLogoutUser: () => dispatch(logoutUser())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Logout);
