import { StyleSheet } from "react-native";
import { Metrics, Fonts, Colors, Helpers } from "../../Theme";

export default StyleSheet.create({
  wrapper: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    ...Metrics.smallHorizontalMargin,
    flexDirection: "column",
    backgroundColor: Colors.white
  },
  container: {
    width: "100%",
    height: "100%",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 20
  },
  thumbnail: {
    width: 50,
    height: 50,
    borderRadius: 10
  },
  topContainer: {
    width: "100%",
    height: "60%",
    flexDirection: "column",
    ...Helpers.center,
    backgroundColor: Colors.modalTop,
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10
  },
  bottomContainer: {
    ...Helpers.row,
    width: "100%",
    height: "50%",
    justifyContent: "space-around",
    alignItems: "center",
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10
  },
  thumbnail: {
    flexGrow: 1
  },
  textContainer: {
    flex: 1,
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "space-around"
  },
  error: {
    ...Fonts.h5,
    color: Colors.black,
    textAlign: "center"
  },
  buttonContainer: {
    width: 120,
    height: 50,
    ...Helpers.row,
    ...Metrics.tinyVerticalPadding,
    ...Metrics.tinyHorizontalPadding,
    alignItems: "center",
    justifyContent: "space-around",
    borderRadius: 10
  },
  buttonText: {
    ...Fonts.h4,
    color: Colors.white
  },
  borderForButton: {
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: Colors.grey200
  },
  deleteContainer : {
    ...Helpers.column,
    width: "100%",
    height: "40%",
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10
  },
  deleteButtonContainer: {
    ...Helpers.row,
    flex:2,
    alignItems: "center",
    justifyContent: "space-around",
  }
});
