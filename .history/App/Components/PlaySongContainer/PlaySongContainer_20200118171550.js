import React, { useEffect } from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import { Colors, Helpers } from "../../Theme";
import Styles from "./Styles";
import { Ionicons } from "@expo/vector-icons";
import Reactotron from "reactotron-react-native";
import { startPlaying, playSongFromDownload } from "../../Actions/Media/Actions";
import { connect } from "react-redux";

const PlaySongContainer = ({ item, onDispatchPlaySong }) => {
  const { name, image, } =item;
  const _handlePlaySong = () => {
    
    onDispatchPlaySong(item)
    onDispatchSetPlaySongFromDownload(false)
    Reactotron.log("we sould be playing this", item);
  };
  return (
    <TouchableOpacity style={Styles.container} onPress={_handlePlaySong}>
      <Image source={{ uri: image }} style={Styles.icon} />
      <View style={Styles.midContainer}>
        <Text style={Styles.title}>{name}</Text>
      </View>
      <View style={Styles.buttonContainer}>
        <Ionicons size={25} color={Colors.black} name="ios-play-circle" />
      </View>
    </TouchableOpacity>
  );
};

const mapStateToProps = state => {
  return {
    language: state.app.language
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onDispatchPlaySong: song => dispatch(startPlaying(song)),
    onDispatchSetPlaySongFromDownload: val => dispatch(playSongFromDownload(val))
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(PlaySongContainer);
