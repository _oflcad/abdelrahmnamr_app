import React, { useContext, useEffect, useState } from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import Style from "./Styles";
import { NavigationContext } from "react-navigation";
import { Colors, Metrics, Helpers, Images } from "../../Theme";
import {
  openPlaylistInfoModal,
  closePlaylistInfoModal,
  showMiniPlayer
} from "../../Actions/Media/Actions";
import { setRouteName } from "../../Actions/App/Actions";
import NavigationService from "../../Services/NavigationService";
import { connect } from "react-redux";
const NestedHeaderComponent = ({
  customTitle,
  onDispatchOpenPlaylistInfoModal,
  language,
  onDispatchSetRouteName,
  onDispatchShowMiniPlayer
}) => {
  const [mainTitle, setMainTitle] = useState("");
  const navigation = useContext(NavigationContext);
  const title = navigation.state.routeName;
  let secondStyle = customTitle ? true : false;
  let authStyle = title === "SignIn" || title === "SignUp" ? true : false;

  const _navigateToNotification = () => {
    onDispatchSetRouteName("Notification");
    onDispatchShowMiniPlayer();
    NavigationService.navigate("Notification");
  };

  const _navigateBack = () => {
    if (title === "Player") {
      onDispatchShowMiniPlayer();
      onDispatchSetRouteName("Playlist");
      NavigationService.navigate("Playlist");
    } else if (title === "PlaylistInfo") {
      NavigationService.navigate("Playlist");
    }
     else if (
      title === "Profile" ||
      title === "SignIn" ||
      title === "SignUp"
    ) {
      onDispatchSetRouteName("Settings");
      NavigationService.navigate("Settings");
    } else {
      onDispatchSetRouteName("Home");
      NavigationService.navigateAndReset("Home");
    }
  };

  useEffect(() => {
    authStyle
      ? title === "SignIn"
        ? language === "en"
          ? setMainTitle("Sign In")
          : setMainTitle("تسجيل الدخول")
        : language === "en"
        ? setMainTitle("Sign Up")
        : setMainTitle("تسجيل")
      : secondStyle
      ? setMainTitle(customTitle)
      : setMainTitle(title);
  });

  if (language === "en") {
    return (
      <View style={[Style.container, Metrics.tinyVerticalMargin]}>
        <View style={[Helpers.rowCross]}>
          <TouchableOpacity
            style={[Metrics.horizontalMargin]}
            onPress={_navigateBack}
          >
            <Image resizeMode={"contain"} resizeMode={"contain"}source={Images.leftArrow} style={Style.icon} />
          </TouchableOpacity>
          <Text style={Style.title}>{mainTitle}</Text>
        </View>
        <View style={[Helpers.rowCross, Helpers.mainSpaceBetween]}>
          <TouchableOpacity
            style={[Metrics.smallHorizontalMargin]}
            onPress={_navigateToNotification}
          >
            <Image resizeMode={"contain"} resizeMode={"contain"}source={Images.notification} style={Style.icon} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  return (
    <View
      style={[
        Style.container,
        Metrics.tinyVerticalMargin,
        { flexDirection: "row-reverse" }
      ]}
    >
      <View style={[Helpers.rowCross]}>
        <Text style={Style.title}>{mainTitle}</Text>
        <TouchableOpacity
          style={[Metrics.horizontalMargin]}
          onPress={_navigateBack}
        >
          <Image resizeMode={"contain"} resizeMode={"contain"}source={Images.rightArrow} style={Style.icon} />
        </TouchableOpacity>
      </View>
      <View style={[Helpers.rowCross, Helpers.mainSpaceBetween]}>
        <TouchableOpacity
          style={[Metrics.smallHorizontalMargin]}
          onPress={_navigateToNotification}
        >
          <Image resizeMode={"contain"} resizeMode={"contain"}source={Images.notification} style={Style.icon} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const mapStateToProps = state => {
  return {
    language: state.auth.language
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onDispatchOpenPlaylistInfoModal: () => dispatch(openPlaylistInfoModal()),
    onDispatchClosePlaylistInfoModal: () => dispatch(closePlaylistInfoModal()),
    onDispatchSetRouteName: val => dispatch(setRouteName(val)),
    onDispatchShowMiniPlayer: () => dispatch(showMiniPlayer())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NestedHeaderComponent);
