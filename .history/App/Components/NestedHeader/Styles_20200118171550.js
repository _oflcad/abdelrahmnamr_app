import { StyleSheet } from 'react-native'
import { Helpers, Metrics, Fonts, Colors } from '../../Theme'

export default StyleSheet.create({
  container: {
    ...Helpers.fullWidth,
    height: 50,
    flexDirection: 'row',
    backgroundColor: Colors.white,
    alignItems: 'center',
    justifyContent: 'space-between',
   
  },
  error: {
    ...Fonts.normal,
    color: Colors.error,
    marginBottom: Metrics.tiny,
    textAlign: 'center',
  },
  rightContainer: {
    flexGrow: 0.2
  },
  logoContainer: {
    ...Helpers.fullWidth,
    height: 300,
    marginBottom: 25,
  },
  result: {
    ...Fonts.normal,
    marginBottom: Metrics.tiny,
    textAlign: 'center',
  },
  text: {
    ...Fonts.normal,
    marginBottom: Metrics.tiny,
    textAlign: 'center',
  },
  title: {
    ...Fonts.h4,
  },
})
