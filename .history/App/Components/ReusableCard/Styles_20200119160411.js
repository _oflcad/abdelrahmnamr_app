import { StyleSheet } from "react-native";
import { Helpers, Metrics, Fonts, Colors } from "../../Theme";

export default StyleSheet.create({
  container: {
    ...Helpers.column,
    ...Metrics.verticalMargin,
    alignSelf: 'center',
    width: Metrics.width - 45,
    height: Metrics.height / 2 + 100,
    backgroundColor: Colors.white,
    borderRadius: 10,
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 8
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,
    elevation: 16
  },
  titleContainer: {
    flex: 0.5,
    ...Helpers.row,
    ...Helpers.crossStart,
    ...Helpers.mainSpaceBetween,
    ...Metrics.horizontalMargin,
    ...Metrics.tinyVerticalMargin
  },
  imageContainer: {
    ...Helpers.center,
    flex: 3,
  },
  descriptionContainer: {
    ...Helpers.mainEnd,
    ...Metrics.horizontalMargin,
    ...Metrics.verticalMargin,
    flex: 1
  },
  imageUrl: {
    width: '90%',
    height: '90%',
    borderRadius: 10,
  }
});
