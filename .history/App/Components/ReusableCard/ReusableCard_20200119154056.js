import React from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import { Helpers, Fonts, Images, Colors, Metrics } from "../../Theme";
import Styles from "./Styles";
import { setItem, clearItem } from "../../Actions/App/Actions";
import moment from "moment";
import { connect } from "react-redux";
import NavigationService from "../../Services/NavigationService";
import { Ionicons } from "@expo/vector-icons";
const ReusableCard = props => {
  const { title, imageUrl, description, createdAt } = props.item;

  const _handleShareNews = () => {
    console.log("TO DO HANDLE NEWS SHARE WITH DEVICE OPTIONS");
  };

  const _handleItemNavigation = () => {
    props.onDispatchRemoveItem();
    props.onDispatchSetItem(props.item);
    NavigationService.navigate("Item");
  };
  return (
    <TouchableOpacity style={Styles.container} onPress={_handleItemNavigation}>
      <View style={[Styles.titleContainer, { flexDirection: "row-reverse" }]}>
        <View
          style={[
            Helpers.column,
            Helpers.crossStart,
            Helpers.mainSpaceBetween,
            Metrics.tinyVerticalMargin
          ]}
        >
          <Text style={[Fonts.h5, { color: Colors.grey200 }]}>{title}</Text>
        </View>
      </View>
      <View style={Styles.imageContainer}>
        <Image resizeMode={"contain"}source={{ uri: imageUrl }} style={Styles.imageUrl} />
      </View>
      <View style={Styles.descriptionContainer}>
        <Text style={{ textAlign: "right" }}>{description}</Text>
      </View>
      <View
        style={[
          Helpers.row,
          Helpers.mainEnd,
          Helpers.crossCenter,
          Metrics.horizontalMargin,
          Metrics.verticalMargin
        ]}
      >
        <Text
          style={[
            Fonts.label,
            { color: Colors.grey150 },
            Metrics.tinyHorizontalMargin
          ]}
        >
          {moment(createdAt).fromNow()}
        </Text>

        <Image resizeMode={"contain"}source={Images.calendar} style={{ width: 20, height: 20 }} />
      </View>
    </TouchableOpacity>
  );
};

const mapStateToProps = state => {
  return {
    language: state.app.language
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onDispatchSetItem: item => dispatch(setItem(item)),
    onDispatchRemoveItem: () => dispatch(clearItem())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ReusableCard);
