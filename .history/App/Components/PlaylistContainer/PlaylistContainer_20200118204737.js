import React, { useState, useEffect } from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import NavigationService from "../../Services/NavigationService";
import Styles from "./Styles";
import { Colors, Helpers, Metrics, Images } from "../../Theme";
import { setPlaylist } from "../../Actions/Media/Actions";
import { connect } from "react-redux";
import Reactotron from "reactotron-react-native";
const PlaylistContainer = props => {
  const [title, setTitle] = useState("");
  const [isDownloaded, setIsDownloaded] = useState(false);
  const playlistLength = props.item.songs.length;
  const playlist = props.item;

  useEffect(() => {
    Reactotron.log("APPLICATION LANGUAGE PLAYLISTCONTAINER" + props.language);
    setTitle(playlist.title);
    console.log("title to display", title);

    setIsDownloaded(playlist.downloaded);
    console.log("downloaded ? to display", isDownloaded);
  });

  const _handleNav = () => {
    props.onDispatchSetItem(playlist);
    NavigationService.navigate("PlaylistInfo");
  };

  if (props.language === "en") {
    return (
      <View style={Styles.container}>
        <View style={Styles.logoContainer}>
          <Image source={Images.defaultThumbnail} style={Styles.logo} />
        </View>
        <View
          style={[
            Styles.textContainer,
            Helpers.mainCenter,
            Helpers.crossStart,
            Metrics.smallHorizontalMargin
          ]}
        >
          {isDownloaded ? (
            <View style={[Helpers.row, Helpers.mainSpaceAround]}>
              <Text style={Styles.title}>{title}</Text>
              <View style={[Helpers.center, Metrics.horizontalMargin]}>
                <Image
                  source={Images.offlineAudio}
                  style={{ width: 25, height: 25 }}
                />
              </View>
            </View>
          ) : (
            <View style={[Helpers.row, Helpers.mainSpaceAround]}>
              <Text style={Styles.title}>{title}</Text>
              <View style={[Helpers.center, Metrics.horizontalMargin]}>
                <Image
                  source={Images.onlineAudio}
                  style={{ width: 25, height: 25 }}
                />
              </View>
            </View>
          )}

          <Text style={Styles.subTitle}>{playlistLength} audio</Text>
        </View>
        <View style={Styles.buttonContainer}>
          <TouchableOpacity onPress={_handleNav}>
            <Image
              source={Images.rightArrow}
              style={{ width: 30, height: 30 }}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  return (
    <View style={[Styles.container, Helpers.rowReverse]}>
      <View style={Styles.logoContainer}>
        <Image source={Images.defaultThumbnail} style={Styles.logo} />
      </View>
      <View
        style={[
          Styles.textContainer,
          Helpers.mainCenter,
          Helpers.crossStart,
          Metrics.smallHorizontalMargin
        ]}
      >
        {isDownloaded ? (
          <View style={[Helpers.row, Helpers.mainSpaceAround]}>
            <Text style={Styles.title}>{title}</Text>
            <View style={[Helpers.center, Metrics.horizontalMargin]}>
              <Image
                source={Images.offlineAudio}
                style={{ width: 25, height: 25 }}
              />
            </View>
          </View>
        ) : (
          <View style={[Helpers.row, Helpers.mainSpaceAround]}>
            <Text style={Styles.title}>{title}</Text>
            <View style={[Helpers.center, Metrics.horizontalMargin]}>
              <Image
                source={Images.onlineAudio}
                style={{ width: 25, height: 25 }}
              />
            </View>
          </View>
        )}

        <Text style={Styles.subTitle}>{playlistLength} audio</Text>
      </View>
      <View style={Styles.buttonContainer}>
        <TouchableOpacity onPress={_handleNav}>
          <Image source={Images.rightArrow} style={{ width: 30, height: 30 }} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const mapStateToProps = state => {
  return {
    language: state.app.language
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onDispatchSetItem: item => dispatch(setPlaylist(item))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PlaylistContainer);
