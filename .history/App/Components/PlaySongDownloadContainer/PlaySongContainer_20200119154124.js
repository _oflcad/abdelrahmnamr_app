import React, { useEffect } from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import { Images } from "../../Theme";
import Styles from "./Styles";
import Reactotron from "reactotron-react-native";
import {
  startPlaying,
  playSongFromDownload
} from "../../Actions/Media/Actions";
import { connect } from "react-redux";

const PlaySongDownloadContainer = ({ item, onDispatchPlaySong }) => {
  const { name, image } = item;
  const _handlePlaySong = () => {
    onDispatchPlaySong(item);
    onDispatchSetPlaySongFromDownload(true);
    Reactotron.log("we sould be playing this", item);
  };
  return (
    <TouchableOpacity style={Styles.container} onPress={_handlePlaySong}>
      <Image resizeMode={"contain"} resizeMode={"contain"}source={{ uri: image }} style={Styles.icon} />
      <View style={Styles.midContainer}>
        <Text style={Styles.title}>{name}</Text>
      </View>
      <View style={Styles.buttonContainer}>
        <Image resizeMode={"contain"} resizeMode={"contain"}source={Images.play} style={{ width: 25, height: 25 }} />
      </View>
    </TouchableOpacity>
  );
};

const mapStateToProps = state => {
  return {
    language: state.app.language
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onDispatchPlaySong: song => dispatch(startPlaying(song)),
    onDispatchSetPlaySongFromDownload: val =>
      dispatch(playSongFromDownload(val))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PlaySongDownloadContainer);
