import React from "react";
import { View, Text, Image } from "react-native";
import Styles from "./Styles";
import { Colors, Helpers, Words, Images } from "../../Theme";
import connect from "react-redux/lib/connect/connect";
const EmptyPlaylist = props => {
  const { title, downloads } = props;
  if (downloads) {
    return (
      <View style={[Styles.container, Helpers.center]}>
        <View style={Styles.contentContainer}>
          <Image resizeMode={"contain"} resizeMode={"contain"}style={Styles.icon} source={Images.download} />
          <Text style={Styles.text}>
            {props.language === "en"
              ? Words.en.emptyDownloadedPlaylist
              : Words.ar.emptyDownloadedPlaylist}
          </Text>
        </View>
      </View>
    );
  }
  return (
    <View style={[Styles.container, Helpers.center]}>
      <View style={Styles.contentContainer}>
        <Image resizeMode={"contain"} resizeMode={"contain"}style={Styles.icon} source={Images.download} />
        <Text style={Styles.text}>
          {props.language === "en"
            ? Words.en.emptyPlaylist
            : Words.ar.emptyPlaylist}
          {title
            ? props.language === "en"
              ? " songs"
              : " الأغاني"
            : props.language === "en"
            ? " playlists"
            : " قوائم التشغيل"}
        </Text>
      </View>
    </View>
  );
};

const mapStateToProps = state => {
  return {
    language: state.app.language
  };
};

export default connect(mapStateToProps, null)(EmptyPlaylist);
