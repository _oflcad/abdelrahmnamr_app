import React from "react";
import { View, Text } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import Styles from "./Styles";
import { Colors, Helpers, Words } from "../../Theme";
import connect from "react-redux/lib/connect/connect";
const EmptyPlaylist = props => {
  const { title, downloads } = props;
  if (downloads) {
    return (
      <View style={[Styles.container, Helpers.center]}>
        <View style={Styles.contentContainer}>
          <Ionicons name="ios-download" size={40} color={Colors.black} />
          <Text style={Styles.text}>
            {props.language === "en"
              ? Words.en.emptyDownloadedPlaylist
              : Words.ar.emptyDownloadedPlaylist}
          </Text>
        </View>
      </View>
    );
  }
  return (
    <View style={[Styles.container, Helpers.center]}>
      <View style={Styles.contentContainer}>
        <Ionicons name="ios-musical-notes" size={40} color={Colors.black} />
        <Text style={Styles.text}>
          {props.language === "en"
            ? Words.en.emptyPlaylist
            : Words.ar.emptyPlaylist}
          {title
            ? props.language === "en"
              ? " songs"
              : " الأغاني"
            : props.language === "en"
            ? " playlists"
            : " قوائم التشغيل"}
        </Text>
      </View>
    </View>
  );
};

const mapStateToProps = state => {
return{
  language: state.app.language
}
}

export default connect(mapStateToProps,null)(EmptyPlaylist);
