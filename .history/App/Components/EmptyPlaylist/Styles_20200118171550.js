import { StyleSheet } from "react-native";
import { Helpers, Metrics, Fonts, Colors } from "../../Theme";

export default StyleSheet.create({
  container: {
    flex: 1,
    ...Helpers.center,
    marginTop: "15%"
  },
  contentContainer: {
    width: Metrics.width - 50,
    alignItems: "center"
  },
  text: {
    ...Metrics.verticalMargin,
    fontFamily: "robotoB",
    fontSize: 25,
    color: Colors.black,
    textAlign: "center"
  }
});
