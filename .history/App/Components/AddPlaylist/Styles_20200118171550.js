import { StyleSheet } from "react-native";
import { Helpers, Metrics, Fonts, Colors } from "../../Theme";

export default StyleSheet.create({
  container: {
    width: Metrics.width,
    height: 60,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    margin: 10,
    paddingHorizontal: 10,
    paddingVertical: 10,
    backgroundColor: Colors.white
  },
  insideContainer: {
    width: 50,
    height: 50,
    backgroundColor: Colors.grey100,
    borderRadius: 5,
    justifyContent: "center",
    alignItems: "center"
  },
  text: {
    fontFamily: "robotoR",
    fontSize: 20,
    color: Colors.black,
    paddingHorizontal: 10
  },
});
