import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import Styles from "./Styles";
import { Words, Colors, Helpers, Metrics } from "../../Theme";
import { Ionicons } from "@expo/vector-icons";
import { connect } from "react-redux";
import { openPlaylistModal } from "../../Actions/Media/Actions";
const AddPlaylist = (props) => {
  if(props.language === 'en'){
    return (
      <TouchableOpacity onPress={props.onDispatchOpenModal} style={Styles.container}>
        <View style={[Styles.insideContainer, Metrics.smallHorizontalMargin]}>
          <Ionicons
            name="ios-add"
            color={Colors.black}
            size={30}
          />
        </View>
        <Text style={Styles.text}>
          {Words.en.addPlaylist}
        </Text>
      </TouchableOpacity>
    );  
  }
  return (
    <TouchableOpacity onPress={props.onDispatchOpenModal} style={[Styles.container, {...Helpers.rowReverse, ...Metrics.tinyHorizontalMargin}]}>
      <View style={[Styles.insideContainer, Metrics.smallHorizontalMargin]}>
        <Ionicons
          name="ios-add"
          color={Colors.black}
          size={30}
        />
      </View>
      <Text style={Styles.text}>
        {Words.ar.addPlaylist}
      </Text>
    </TouchableOpacity>
  );
};


const mapStateToProps = state => {
return{
  language: state.app.language
}
}

const mapDispatchToProps = dispatch => {
return{
    onDispatchOpenModal : () => dispatch(openPlaylistModal())
}
}

export default connect(mapStateToProps,mapDispatchToProps)(AddPlaylist);
