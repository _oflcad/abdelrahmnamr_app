import { StyleSheet } from "react-native";
import { Helpers, Metrics, Fonts, Colors } from "../../Theme";

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white
      },
      miniPlayer: {
        position: "absolute",
        right: 0,
        left: 0,
        bottom:5,
        zIndex: 10,
        backgroundColor: Colors.white,
        height: Metrics.height
      },
      miniPlayerContainer: {
        position: 'absolute',
        bottom: 49,
        right: 0,
        width: Metrics.width,
        height: 50,
        flexDirection: "row",
        alignItems: "center",
        zIndex: 2,
        backgroundColor: Colors.white
      },
      specialMiniPlayerContainer: {
        position: 'absolute',
        bottom: 49,
        right: 0,
        width: Metrics.width,
        height: 50,
        flexDirection: "row",
        alignItems: "center",
        zIndex: 2,
        backgroundColor: Colors.white
      },
      imageAndTextContainer: {
        flex: 4,
        flexDirection: "row",
        alignItems: "center"
      },
      imageContainer: {
        width: 32,
        height: 32,
        marginLeft: 10
      },
      thumbnail: {
        flex: 1,
        width: null,
        height: null,
        borderRadius: 10
      },
      textContainer: {
        flexGrow: 2,
        flexDirection: "column",
        justifyContent: "space-around",
        margin: 10
      },
      miniButtonContainer: {
        opacity: 1,
        flexGrow: 1,
        flexDirection: "row",
        justifyContent: "space-around"
      },
      title: {
        opacity: 1,
        fontFamily: "robotoB",
        fontSize: 16,
        color: Colors.black
      },
      author: {
        opacity: 1,
        fontFamily: "robotoL",
        fontSize: 12,
        color: Colors.black
      },
      songIconsPlayerContainer: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-around"
      },
      textPlayerContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "flex-end"
      },
      sliderContainer: {
        height: 40,
        width: Metrics.width,
        alignItems: 'center',
      },
      slider: {
        width: Metrics.width- 40,
      },
      timeContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "flex-start"
      },
      time: {
        fontFamily: 'robotoB',
        fontSize: 24,
        color: Colors.black
      }
});
