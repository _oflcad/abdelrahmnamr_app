import React from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  DeviceEventEmitter,
  NativeEventEmitter,
} from "react-native";
import { Metrics, Colors, Helpers } from "../../Theme";
import { connect } from "react-redux";
import { Ionicons } from "@expo/vector-icons";

import Styles from "./Styles";
import { Audio } from "expo-av";
import _ from "lodash";
import {
  pausePlaying,
  resumePlaying,
  hideMiniPlayer,
  setAudioTitle,
  stopPlaying
} from "../../Actions/Media/Actions";
import { navigateToPlayer } from "../../Actions/App/Actions";

class MiniPlayer extends React.Component {
  constructor(props) {
    super(props);
    this.playbackInstance = null;
    this.nextPlaybackInstance = null;
    this.index = 0;
    this.state = {
      isLoading: false,
      index: 0,
      data: null,
      playingObject: {
        uri: '',
        title: '',
      },
    };
  }

  componentDidMount() {
    this.listener = DeviceEventEmitter.addListener("PLAYPAUSE", data => {
      this._handlePlayAndPause();
      console.log("New Event is registered At Listeners PLAY_PAUSE");
      
    });
    this.listenerN = DeviceEventEmitter.addListener("NEXTSONG", data => {
      this._handleNextSong()
      console.log("New Event is registered At Listeners NEXT_SONG ");
      
    });
    this.listenerP = DeviceEventEmitter.addListener("PREVIOUSSONG", data => {
     this._handlePreviousSong()
      console.log("New Event is registered At Listeners PREVIOUS_SONG");
      
    });

    this.listenerS = DeviceEventEmitter.addListener("STOPSONG", data => {
      this._handleStopSong();
      console.log("New Event is registered At Listeners STOPSONG");
    })

    Audio.setAudioModeAsync({
      allowsRecordingIOS: false,
      interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
      playsInSilentModeIOS: true,
      shouldDuckAndroid: true,
      interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
      playThroughEarpieceAndroid: false
    });
    
  }

  async componentDidUpdate(prevProps) {
    if (prevProps.songToPlay !== null) {
      if (
        prevProps.audiosToPlay !== this.props.audiosToPlay &&
        this.playbackInstance !== null
      ) {
        this.index=0;
        this._loadNewPlaybackInstance();
      }
    }
  }

  async _loadNewPlaybackInstance() {
    const { audiosToPlay, isPlaying, audio } = this.props;

    this.setState({ isLoading: true });
    if (this.playbackInstance !== null) {
      await this.playbackInstance.unloadAsync();
      this.playbackInstance.setOnPlaybackStatusUpdate(null);
      this.playbackInstance = null;
    }
    console.log('we are loading this URI', audiosToPlay[this.index])
    this.props.onDispatchSetAudioTitle(audiosToPlay[this.index])
    const source = { uri: audiosToPlay[this.index].uri };
  
    
    const initialStatus = {
      //        Play by default
      shouldPlay: isPlaying,
      //        Control the speed
      rate: 1.0,
      //        Correct the pitchs
      shouldCorrectPitch: true,
      //        Control the Volume
      volume: 1.0,
      //        mute the Audio
      isMuted: false
    };
    const { sound, status } = await Audio.Sound.createAsync(
      source,
      initialStatus
    );
    //  Save the response of sound in playbackInstance
    this.playbackInstance = sound;
    this.setState({ isLoading: false });
    //  Make the loop of Audio

    //  Play the Music
    this.playbackInstance.playAsync();
  }

  _handlePlayAndPause = async () => {
    if (this.playbackInstance !== null) {
      if (this.props.isPlaying) {
        // Pause Player
        this.props.onDispatchPauseSong();
        await this.playbackInstance.pauseAsync();
      } else {
        this.props.onDispatchResumeSong();
        await this.playbackInstance.playAsync();
      }
    } else {
      this._loadNewPlaybackInstance();
    }
  };


  _advanceIndex = (forward) => {
    const {audiosToPlay, songToPlay} = this.props;
    const {isLoading} = this.state;
    const LENGTH = audiosToPlay.length;
    if(this.playbackInstance !== null && songToPlay.hasmultiple && !isLoading){
      this.index =
      (this.index + (forward ? 1 : LENGTH - 1)) % LENGTH;
      this._loadNewPlaybackInstance();
    }
  }

  _handleNextSong = () => {
    console.log("PLAYING NEXT SONG");
   this._advanceIndex(true);
  };

  _handlePreviousSong = () => {
    console.log("PLAYING PREVIOUS SONG");
    const {audiosToPlay, audio, songToPlay} = this.props;
    this._advanceIndex(false);
  }

  _handleStopSong = () => {
    if(this.playbackInstance !== null) {
      this.props.onDispatchStopSong();
      this.playbackInstance.stopAsync();
    }
  }

  _handlePlayer = () => {
    this.props.onDispatchHideMiniPlayer();
    this.props.onDispatchMove();
  };

  render() {
    const { isPlaying, songToPlay, isPlayerShowen, audiosToPlay } = this.props;
    const { isLoading, index } = this.state;
    if (isPlayerShowen) {
      return (
        <View style={Styles.miniPlayerContainer}>
          <TouchableOpacity
            onPress={this._handlePlayer}
            style={Styles.imageAndTextContainer}
          >
            <View style={[Styles.imageContainer]}>
              {songToPlay.image ? <Image
                resizeMode={"cover"}
                source={{ uri: songToPlay.image }}
                style={Styles.thumbnail}
              /> : 
              <View style={[Helpers.fillCenter]}>
                <Ionicons name="ios-musical-notes" size={50} color={Colors.grey200} />
              </View>}
            </View>
            <View style={Styles.textContainer}>
              {
                this.playbackInstance ?
                <Text style={[Styles.title]}>{audiosToPlay[index].title}</Text>
                :
                <Text style={[Styles.title]}>{songToPlay.name}</Text>
              }
              <Text style={[Styles.author]}>عبد الرحمن عمرو</Text>
            </View>
          </TouchableOpacity>
          {this.playbackInstance !== null || !isLoading ? (
            <View
              style={[
                Metrics.horizontalMargin,
                Metrics.smallVerticalMargin,
                Helpers.row,
                Helpers.mainSpaceBetween,
                Helpers.crossCenter
              ]}
            >
              
              <TouchableOpacity
                onPress={() => this._handlePlayAndPause()}
                style={[Metrics.tinyHorizontalMargin]}
              >
                {isPlaying && this.playbackInstance && isPlayerShowen ? (
                  <Ionicons name="ios-pause" size={30} color={Colors.black} />
                ) : (
                  <Ionicons
                    name="ios-play-circle"
                    color={Colors.black}
                    size={25}
                  />
                )}
              </TouchableOpacity>
            </View>
          ) : (
            <View style={[Metrics.horizontalMargin, Metrics.verticalMargin]}>
              <ActivityIndicator size={"small"} color={Colors.grey200} />
            </View>
          )}
        </View>
      );
    }
    return <View />;
  }
}

const mapStateToProps = state => ({
  isPlaying: state.media.isPlaying,
  isPlayerShowen: state.media.isPlayerShowen,
  songToPlay: state.media.songToPlay,
  audiosToPlay: state.media.audiosToPlay,
  indexToPlay: state.media.indexToPlay,
  audio: state.media.audio
});

const mapDispatchToProps = dispatch => {
  return {
    onDispatchPauseSong: () => dispatch(pausePlaying()),
    onDispatchResumeSong: () => dispatch(resumePlaying()),
    onDispatchStopSong: () => dispatch(stopPlaying()),
    onDispatchMove: () => dispatch(navigateToPlayer()),
    onDispatchHideMiniPlayer: () => dispatch(hideMiniPlayer()),
    onDispatchSetAudioTitle: (title) => dispatch(setAudioTitle(title))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MiniPlayer);
