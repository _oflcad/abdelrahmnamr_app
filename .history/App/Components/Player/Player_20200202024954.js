import React from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  DeviceEventEmitter,
  NativeEventEmitter
} from "react-native";
import { Metrics, Colors, Helpers, Images } from "../../Theme";
import { connect } from "react-redux";

import Styles from "./Styles";
import { Audio } from "expo-av";
import _ from "lodash";
import {
  pausePlaying,
  resumePlaying,
  hideMiniPlayer,
  setAudioTitle,
  stopPlaying
} from "../../Actions/Media/Actions";
import { navigateToPlayer } from "../../Actions/App/Actions";
import reactotron from "reactotron-react-native";

class MiniPlayer extends React.Component {
  constructor(props) {
    super(props);
    this.playbackInstance = null;
    this.nextPlaybackInstance = null;
    this.index = 0;
    this.source = {
      uri: null
    }
    this.state = {
      isLoading: false,
      index: 0,
      data: null,
      playingObject: {
        uri: "",
        title: ""
      }
    };
  }

  componentDidMount() {
    this.listener = DeviceEventEmitter.addListener("PLAYPAUSE", data => {
      this._handlePlayAndPause();
      console.log("New Event is registered At Listeners PLAY_PAUSE");
    });
    this.listenerN = DeviceEventEmitter.addListener("NEXTSONG", data => {
      this._handleNextSong();
      console.log("New Event is registered At Listeners NEXT_SONG ");
    });
    this.listenerP = DeviceEventEmitter.addListener("PREVIOUSSONG", data => {
      this._handlePreviousSong();
      console.log("New Event is registered At Listeners PREVIOUS_SONG");
    });

    this.listenerS = DeviceEventEmitter.addListener("STOPSONG", data => {
      this._handleStopSong();
      console.log("New Event is registered At Listeners STOPSONG");
    });

    Audio.setAudioModeAsync({
      shouldDuckAndroid: false,
      interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
      playThroughEarpieceAndroid: false,
      staysActiveInBackground: true
    });
  }

  async componentDidUpdate(prevProps) {
    if (prevProps.songToPlay !== null) {
      if (
        prevProps.audiosToPlay !== this.props.audiosToPlay &&
        this.playbackInstance !== null
      ) {
        this.index = 0;
        this._loadNewPlaybackInstance();
      }
    }
  }

  async _loadNewPlaybackInstance() {
    const { audiosToPlay, isPlaying, audio, songToPlay } = this.props;

    this.setState({ isLoading: true });
    if (this.playbackInstance !== null) {
      await this.playbackInstance.unloadAsync();
      this.playbackInstance.setOnPlaybackStatusUpdate(null);
      this.playbackInstance = null;
    }
    console.log("we are loading this URI", audiosToPlay[this.index]);
    this.props.onDispatchSetAudioTitle(audiosToPlay[this.index]);
    reactotron.log(this.props.media);
      if(songToPlay.downloadedAudios && !_.isEmpty(songToPlay.downloadedAudios[this.index].downloadUri)) {
        this.source.uri = songToPlay.downloadedAudios[this.index].downloadUri;
    } else {
      this.source.uri = audiosToPlay[this.index].uri;
    }

    console.log("Our source for playing", this.source);

    const initialStatus = {
      //        Play by default
      shouldPlay: isPlaying,
      //        Control the speed
      rate: 1.0,
      //        Correct the pitchs
      shouldCorrectPitch: true,
      //        Control the Volume
      volume: 1.0,
      //        mute the Audio
      isMuted: false
    };
    const { sound, status } = await Audio.Sound.createAsync(
      this.source,
      initialStatus
    );
    //  Save the response of sound in playbackInstance
    this.playbackInstance = sound;
    this.setState({ isLoading: false });
    //  Make the loop of Audio

    //  Play the Music
    this.playbackInstance.playAsync();
  }

  _handlePlayAndPause = async () => {
    if (this.playbackInstance !== null) {
      if (this.props.isPlaying) {
        // Pause Player
        this.props.onDispatchPauseSong();
        await this.playbackInstance.pauseAsync();
      } else {
        this.props.onDispatchResumeSong();
        await this.playbackInstance.playAsync();
      }
    } else {
      this._loadNewPlaybackInstance();
    }
  };

  _advanceIndex = forward => {
    const { audiosToPlay, songToPlay } = this.props;
    const { isLoading } = this.state;
    const LENGTH = audiosToPlay.length;
    if (
      this.playbackInstance !== null &&
      songToPlay.hasmultiple &&
      !isLoading
    ) {
      this.index = (this.index + (forward ? 1 : LENGTH - 1)) % LENGTH;
      this._loadNewPlaybackInstance();
    }
  };

  _handleNextSong = () => {
    console.log("PLAYING NEXT SONG");
    this._advanceIndex(true);
  };

  _handlePreviousSong = () => {
    console.log("PLAYING PREVIOUS SONG");
    const { audiosToPlay, audio, songToPlay } = this.props;
    this._advanceIndex(false);
  };

  _handleStopSong = () => {
    if (this.playbackInstance !== null) {
      this.props.onDispatchStopSong();
      this.playbackInstance.stopAsync();
    }
  };

  _handlePlayer = () => {
    this.props.onDispatchHideMiniPlayer();
    this.props.onDispatchMove();
  };

  render() {
    const { isPlaying, songToPlay, isPlayerShowen, audiosToPlay } = this.props;
    const { isLoading, index } = this.state;
    if (isPlayerShowen) {
      return (
        <View style={Styles.miniPlayerContainer}>
          <TouchableOpacity
            onPress={this._handlePlayer}
            style={Styles.imageAndTextContainer}
          >
            <View style={[Styles.imageContainer]}>
              {songToPlay.image ? (
                <Image
                  resizeMode={"cover"}
                  source={{ uri: songToPlay.image }}
                  style={Styles.thumbnail}
                />
              ) : (
                <View style={[Helpers.fillCenter]}>
                  <Image
                    resizeMode={"contain"}
                    resizeMode={"contain"}
                    source={Images.musicalNotes}
                    style={Styles.icon}
                  />
                </View>
              )}
            </View>
            <View style={Styles.textContainer}>
              {this.playbackInstance ? (
                <Text style={[Styles.title]}>{audiosToPlay[index].title}</Text>
              ) : (
                <Text style={[Styles.title]}>{songToPlay.name}</Text>
              )}
              <Text style={[Styles.author]}>عبد الرحمن عمرو</Text>
            </View>
          </TouchableOpacity>
          {this.playbackInstance !== null || !isLoading ? (
            <View
              style={[
                Metrics.horizontalMargin,
                Metrics.smallVerticalMargin,
                Helpers.row,
                Helpers.mainSpaceBetween,
                Helpers.crossCenter
              ]}
            >
              <TouchableOpacity
                onPress={() => this._handlePlayAndPause()}
                style={[Metrics.tinyHorizontalMargin]}
              >
                {isPlaying && this.playbackInstance && isPlayerShowen ? (
                  <Image
                    resizeMode={"contain"}
                    resizeMode={"contain"}
                    source={Images.pause}
                    style={Styles.icon}
                  />
                ) : (
                  <Image
                    resizeMode={"contain"}
                    resizeMode={"contain"}
                    source={Images.play}
                    style={Styles.icon}
                  />
                )}
              </TouchableOpacity>
            </View>
          ) : (
            <View style={[Metrics.horizontalMargin, Metrics.verticalMargin]}>
              <ActivityIndicator size={"small"} color={Colors.grey200} />
            </View>
          )}
        </View>
      );
    }
    return <View />;
  }
}

const mapStateToProps = state => ({
  media: state.media,
  isPlaying: state.media.isPlaying,
  isPlayerShowen: state.media.isPlayerShowen,
  songToPlay: state.media.songToPlay,
  audiosToPlay: state.media.audiosToPlay,
  indexToPlay: state.media.indexToPlay,
  audio: state.media.audio
});

const mapDispatchToProps = dispatch => {
  return {
    onDispatchPauseSong: () => dispatch(pausePlaying()),
    onDispatchResumeSong: () => dispatch(resumePlaying()),
    onDispatchStopSong: () => dispatch(stopPlaying()),
    onDispatchMove: () => dispatch(navigateToPlayer()),
    onDispatchHideMiniPlayer: () => dispatch(hideMiniPlayer()),
    onDispatchSetAudioTitle: title => dispatch(setAudioTitle(title))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MiniPlayer);
