import { StyleSheet } from "react-native";
import { Helpers, Metrics, Fonts, Colors } from "../../Theme";

export default StyleSheet.create({
    container: {
        width: "95%",
        backgroundColor: 'white',
    },
    buttonContainer: {
        width: "100%",
        height: 40,
        backgroundColor:Colors.white,
        shadowColor: Colors.grey200,
        borderRadius: 10,
        shadowOffset: {
          width: 0,
          height: 8
        },
        marginBottom: 3,
        shadowOpacity: 0.44,
        shadowRadius: 10.32,
        elevation: 16
    }
});
