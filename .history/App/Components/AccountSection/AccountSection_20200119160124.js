import React from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import NavigationService from "../../Services/NavigationService";
import Styles from "./Styles";
import { Helpers, Metrics, Fonts, Colors, Words, Images } from "../../Theme";

import { connect } from "react-redux";
const AccountSection = (props) => {
  
  
  const _navigationToPlaylist = () => {
    NavigationService.navigate("Playlist");
  };

  const _navigateToDownloads = () => {
    NavigationService.navigate("Library");
  };
  if(props.language === 'en'){
    return (
      <View style={[Styles.container,Helpers.center,]}>
        <TouchableOpacity
          onPress={_navigationToPlaylist}
          style={[
            Helpers.row,
            Helpers.mainSpaceBetween,
            Helpers.crossCenter,
            Styles.buttonContainer,
            Metrics.tinyHorizontalMargin,
            Metrics.tinyVerticalMargin
          ]}
        >
          <Text
            style={[
              Fonts.normal,
              { textAlign: "left" },
              Metrics.tinyHorizontalMargin
            ]}
          >
            {Words.en.accountSection.playlist}
          </Text>
          <View style={[Metrics.tinyHorizontalMargin, Helpers.center]}>
            <Image
              source={Images.rightArrow}
              style={Styles.icon}
            />
          </View>
        </TouchableOpacity>
      </View>
    )
  }
  return (
    <View style={[Styles.container,Helpers.center,]}>
      <TouchableOpacity
        onPress={_navigationToPlaylist}
        style={[
          Helpers.rowReverse,
          Helpers.mainSpaceBetween,
          Helpers.crossCenter,
          Styles.buttonContainer,
          Metrics.tinyHorizontalMargin,
          Metrics.tinyVerticalMargin
        ]}
      >
        <Text
          style={[
            Fonts.normal,
            { textAlign: "left" },
            Metrics.tinyHorizontalMargin
          ]}
        >
          {Words.ar.accountSection.playlist}
        </Text>
        <View style={[Metrics.tinyHorizontalMargin, Helpers.center]}>
        <Image
              source={Images.leftArrow}
              style={Styles.icon}
            />
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={_navigateToDownloads}
        style={[
          Helpers.rowReverse,
          Helpers.mainSpaceBetween,
          Helpers.crossCenter,
          Styles.buttonContainer,
          Metrics.tinyVerticalMargin,
          { borderBottomColor: Colors.white }
        ]}
      >
        <Text
          style={[
            Fonts.normal,
            { textAlign: "left" },
            Metrics.tinyHorizontalMargin
          ]}
        >
          {Words.ar.accountSection.downloads}
        </Text>
        <View style={[Metrics.tinyHorizontalMargin, Helpers.center]}>
        <Image
              source={Images.leftArrow}
              style={Styles.icon}
            />
        </View>
      </TouchableOpacity>
    </View>
  )
};
const mapStateToProps = state => {
return{
  language: state.app.language
}
}


export default connect(mapStateToProps,null)(AccountSection);
