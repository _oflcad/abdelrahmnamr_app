import React, { Component } from "react";
import { Text, TouchableOpacity, Image, FlatList, View } from "react-native";
import { connect } from "react-redux";
import Style from "./Styles";
import NavigationService from "../../Services/NavigationService";
import { Helpers, Metrics } from "../../Theme";
class SectionCarousel extends Component {
  constructor(props) {
    super(props);

    this.state = {
      items: [
        {
          title: "News",
          arabic: "الأخبار",
          imageUrl: require("../../Assets/Images/feed.png")
        },
        {
          title: "Sounds",
          arabic: "الإصدارات",
          imageUrl: require("../../Assets/Images/sounds.png")
        },
        {
          title: "Blog",
          arabic: "المدونات",
          imageUrl: require("../../Assets/Images/blog.png")
        }
      ]
    };
  }

  _handleNav = title => {
    if (title === "News") {
      NavigationService.navigate("News");
    } else if (title === "Sounds") {
      NavigationService.navigate("Search");
    } else if (title === "Sessions") {
      NavigationService.navigate("Session");
    } else if (title === "Blog") {
      NavigationService.navigate("Blog");
    }
  };

  _renderItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        style={[
          Style.card,
          this.props.language === "ar" ? { ...Helpers.rowReverse } : null
        ]}
        onPress={() => {
          this._handleNav(item.title);
        }}
      >
        <Text style={Style.text}>
          {this.props.language === "en" ? item.title : item.arabic}
        </Text>
        <Image resizeMode={"contain"} resizeMode={"contain"}source={item.imageUrl} style={Style.image} />
      </TouchableOpacity>
    );
  };

  render() {
    const { items } = this.state;
    const { language } = this.props;
    if (language === "en") {
      return (
        <FlatList
          style={[
            Style.list,
            Metrics.smallHorizontalMargin,
            Metrics.smallVerticalMargin
          ]}
          horizontal
          data={items}
          renderItem={this._renderItem}
          keyExtractor={(item, index) => item + "key" + index}
          showsHorizontalScrollIndicator={false}
          ListFooterComponent={<View style={{width: 20}} />}
        />
      );
    }
    return (
      <FlatList
        style={[
          Style.list,
          Metrics.horizontalMargin,
          Metrics.smallVerticalMargin
        ]}
        horizontal
        inverted
        data={items}
        renderItem={this._renderItem}
        keyExtractor={(item, index) => item + "key" + index}
        showsHorizontalScrollIndicator={false}
        ListHeaderComponent={<View style={{width: 20}} />}
        ListFooterComponent={<View style={{width: 20}} />}
      />
    );
  }
}

const mapStateToProps = state => ({
  language: state.app.language
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(SectionCarousel);
