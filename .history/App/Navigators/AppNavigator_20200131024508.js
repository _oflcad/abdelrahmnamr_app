import React from "react";
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { createBottomTabNavigator } from "react-navigation-tabs";

// Tab Screens
import HomeScreen from "../Containers/Home/HomeScreen";
import SearchScreen from "../Containers/Search/SearchScreen";
import PlaylistScreen from "../Containers/Playlist/PlaylistScreen";
import LibraryScreen from "../Containers/Library/LibraryScreen";
import SettingsScreen from "../Containers/Settings/SettingsScreen";

// Stack Screens
import BasketScreen from "../Containers/Basket/BasketScreen";
import NotificationScreen from "../Containers/Notifications/NotificationScreen";
import NewsScreen from "../Containers/News/NewsScreen";
import BlogScreen from "../Containers/Blog/BlogScreen";
import ItemScreen from "../Containers/Item/ItemScreen";
import PlaylistInfoScreen from "../Containers/PlaylistInfo/PlaylistInfoScreen";
import ReusableItemScreen from '../Containers/ReusableItem/ReusableItemScreen';
import PlayerScreen from '../Containers/Player/PlayerScreen'
import ProfileScreen from '../Containers/Profile/ProfileScreen'
import SignInScreen from '../Containers/SignIn/SingInScreen'
import SignUpScreen from '../Containers/SignUp/SignUpScreen'
import SplashScreen from '../Containers/Entry/SplashScreen';

// Utils
import { Ionicons } from "@expo/vector-icons";
import { Colors } from "../Theme";



/**
 * The LanguageScreen Stack.
 */

 const SplashStack = createStackNavigator({
   Splash: SplashScreen,
 })

/**
 * The Home screen Stack.
 *
 */
const HomeStack = createStackNavigator({
  Home: HomeScreen,
  Basket: BasketScreen,
  Notification: NotificationScreen,
  Blog: BlogScreen,
  News: NewsScreen,
  Item: ReusableItemScreen,
  Audio: ItemScreen,
},{
  initialRouteName: 'Splash',
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
})


/**
 *The Playlist screens Stack.
 */
const PlaylistStack = createStackNavigator({
  Playlist: PlaylistScreen,
  PlaylistInfo: PlaylistInfoScreen,
  Player: PlayerScreen,
},{
  initialRouteName: 'Playlist',
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
})

/**
 * The Settings screens Stack.
 *
 */
const SettingstStack = createStackNavigator({
  Settings: SettingsScreen,
  Profile: ProfileScreen,
  SignIn: SignInScreen,
  SignUp: SignUpScreen,
},{
  initialRouteName: 'Settings',
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
})



/**
 * The root screen contains the application's navigation.
 *
 */
const TabNavigator = createBottomTabNavigator(
  {
    // Create the application routes here (the key is the route name, the value is the target screen)

    // The main application screen is our "Home".
    Home: HomeStack,
    Search: SearchScreen,
    Playlist: PlaylistStack,
    Settings: SettingstStack
  },
  {
    initialRouteName: "Home",
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === "Home") {
          iconName = "ios-home";
        } else if (routeName === "Search") {
          iconName = "ios-search";
        } else if (routeName === "Playlist") {
          iconName = "ios-musical-notes";
        } else if (routeName === "Settings") {
          iconName = "ios-settings";
        }
        return <Ionicons name={iconName} size={25} color={tintColor} />;
      }
    }),
    tabBarOptions: {
      activeTintColor: Colors.black,
      inactiveTintColor: Colors.grey100,
      showLabel: false
    }
  }
);

const AppStack = createSwitchNavigator({
  Splash: SplashStack,
  App: TabNavigator,
},{
  initialRouteName: "Splash"
});

export default createAppContainer(AppStack);
