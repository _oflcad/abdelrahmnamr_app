import React, { Component } from "react";
import {StatusBar} from "react-native";
import { Provider } from "react-redux";
import { configureStore } from "./Stores";
import RootScreen from "./Containers/Root/RootScreen";
import { Colors } from "./Theme";

export default class App extends Component {
  render() {
    return (
      /**
       * @see https://github.com/reduxjs/react-redux/blob/master/docs/api/Provider.md
       */
      <Provider store={configureStore()}>
        <StatusBar backgroundColor={Colors.white}/>
        <RootScreen />
      </Provider>
    );
  }
}
