import { createStore, compose, applyMiddleware } from "redux";
import { AsyncStorage } from "react-native";
import { persistStore, autoRehydrate } from "redux-persist";
import thunk from "redux-thunk";
import reducers from "../Reducers";

import { INITIAL_STATEAUTH } from "../Actions/Auth/InitialState";
import INITIAL_STATEAPP from "../Actions/App/InitialState";
import INITIAL_STATEMEDIA from "../Actions/Media/InitialState";

defaultState = {
  auth: INITIAL_STATEAUTH,
  app: INITIAL_STATEAPP,
  media: INITIAL_STATEMEDIA
};

/* const authBlackFilter = createBlacklistFilter("auth", ["isLoading", "error"]);

const appBlackFilter = createBlacklistFilter("app", [
  "openModal",
  "imageUri",
  "profileImage",
  "betterImageUri"
]);

const mediaBlackFilter = createBlacklistFilter("app", [
  "openModal",
  "imageUri",
  "profileImage",
  "betterImageUri"
]); */

export const configureStore = (initialState = defaultState, action) => {
  var store = createStore(
    reducers,
    initialState,
    compose(
      autoRehydrate(),
      applyMiddleware(thunk)
    )
  );
     
    persistStore(store, {
    storage: AsyncStorage,
    //transforms: [authBlackFilter, appBlackFilter, mediaBlackFilter]
  })


  return store;
};
