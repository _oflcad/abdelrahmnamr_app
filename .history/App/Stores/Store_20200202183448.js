import { createStore, compose, applyMiddleware } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import AsyncStorage from "@react-native-community/async-storage";
import thunk from "redux-thunk";
import autoMergeLevel2 from "redux-persist/lib/stateReconciler/autoMergeLevel2";
import rootReducer from "../Reducers"; // the value from combineReducers
import { createBlacklistFilter } from "redux-persist-transform-filter";
import Reactotron from '../../ReactotronConfig';
const mediaReducerBlackList = createBlacklistFilter("media", [
  "isPlayerShowen",
  "isAddPlaylistModalVisible",
  "isAddSongsToPlaylistModalVisible",
]);

const persistConfig = {
  key: "root",
  storage: AsyncStorage,
  stateReconciler: autoMergeLevel2, // see "Merge Process" section for details.
  transforms: [mediaReducerBlackList]
};

const enhancer = compose(applyMiddleware(thunk),Reactotron.createEnhancer());
const pReducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(pReducer, enhancer);

export const persistor = persistStore(store);
