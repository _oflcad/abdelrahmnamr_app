import { StyleSheet } from "react-native";
const size = {
  h1: 38,
  h2: 34,
  h3: 30,
  h4: 28,
  title: 24,
  input: 18,
  regular: 17,
  medium: 14,
  small: 12
};

export default StyleSheet.create({
  center: {
    textAlign: "center"
  },
  font: {
    fontFamily: "robotoR",
    color: "black"
  },
  buttonText: {
    fontFamily: "robotoR",
    fontSize: size.medium,
    color: "black"
  },
  h1: {
    fontFamily: "RobotoBold",
    fontSize: size.h1,
    color: "black"
  },
  h2: {
    fontFamily: "RobotoBold",
    fontSize: size.h2,
    color: "black"
  },
  h3: {
    fontFamily: "RobotoBold",
    fontSize: size.h3,color: "black"
  },
  h4: {
    fontFamily: "RobotoBold",
    fontSize: size.h4,
    color: "black"
  },
  h5: {
    fontFamily: "RobotoBold",
    fontSize: size.title,
    color: "black"
  },
  subTitle: {
    fontFamily: "robotoL",
    fontSize: size.input
  },
  normal: {
    color: "black",
    fontFamily: "RobotoBold",
    fontSize: size.input
  },
  label: {
    color: "black",
    fontFamily: "robotoL",
    fontSize: size.small
  },
  small: {
    color: "black",
    fontFamily: "robotoL",
    fontSize: size.small
  },
  description: {
    color: "black",
    fontFamily: "robotoL",
    fontSize: size.regular
  },
  oldPrice: {
    fontFamily: "robotoR",
    fontSize: size.small,
    color: "#636e72",
    textDecorationLine: "line-through",
    textDecorationColor: "#636e72"
  },
  newPrice: {
    fontFamily: "RobotoBold",
    fontSize: size.regular,
    color: "black"
  },
  error: {
    color: "red",
    fontFamily: "robotoR",
    fontSize: size.title
  }
});
