/**
 * Images should be stored in the `App/Images` directory and referenced using variables defined here.
 */

export default {
  firstTimerBg: require("../Assets/Images/firstTimer.jpg"),
  abdo: require("../Assets/Images/first.jpg"),
  thumbnail: require("../Assets/Images/albumCover3.jpg"),
  facebookIcon: require("../Assets/Images/facebook-icon.png"),
  instagramIcon: require("../Assets/Images/instagram-icon.png"),
  youtubeIcon: require("../Assets/Images/youtube-icon.png"),
  logoThumbnails: require("../Assets/Images/albumCover.jpg"),
  emptyBasket: require("../Assets/Images/notification.png"),
  addPhoto: require("../Assets/Images/add-photo.png"),
  notFound: require("../Assets/Images/stones.png"),
  confirmEmail: require("../Assets/Images/mailConfirm.png"),
  cart: require("../Assets/Images/cart.png"),
  noNotification: require("../Assets/Images/no-notification.png"),
  rewind: require("../Assets/Images/rewind.png"),
  pause: require("../Assets/Images/pause.png"),
  play: require("../Assets/Images/play.png"),
  stop: require("../Assets/Images/stop.png"),
  forward: require("../Assets/Images/forward.png"),
};
