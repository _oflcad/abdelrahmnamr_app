/**
 * Images should be stored in the `App/Images` directory and referenced using variables defined here.
 */

export default {
  firstTimerBg: require('../Assets/Images/firstTimer.jpg'),
  abdo: require('../Assets/Images/first.jpg'),
  thumbnail: require('../Assets/Images/albumCover3.jpg'),
  facebookIcon: require('../Assets/Images/facebook-icon.png'),
  instagramIcon: require('../Assets/Images/instagram-icon.png'),
  youtubeIcon: require('../Assets/Images/youtube-icon.png'),
  logoThumbnails: require('../Assets/Images/albumCover.jpg'),
  //noNotification: require('../Assets/Animations/no-notification-state.json'),
  //emptyBasket: require('../Assets/Animations/empty-shopping-cart.json'),
  notFound: require('../Assets/Images/stones.png'),
  confirmEmail: require('../Assets/Images/mailConfirm.png')
}
