import { StyleSheet } from "react-native";
const size = {
  h1: 38,
  h2: 34,
  h3: 30,
  h4: 28,
  title: 24,
  input: 18,
  regular: 17,
  medium: 14,
  small: 12
};

export default StyleSheet.create({
  center: {
    textAlign: "center"
  },
  font: {
    fontFamily: "robotoR"
  },
  buttonText: {
    fontFamily: "robotoR",
    fontSize: size.medium
  },
  h1: {
    fontFamily: "robotoB",
    fontSize: size.h1
  },
  h2: {
    fontFamily: "robotoB",
    fontSize: size.h2
  },
  h3: {
    fontFamily: "robotoB",
    fontSize: size.h3
  },
  h4: {
    fontFamily: "robotoB",
    fontSize: size.h4
  },
  h5: {
    fontFamily: "robotoB",
    fontSize: size.title,
    color: "#263238"
  },
  subTitle: {
    fontFamily: "robotoL",
    fontSize: size.input
  },
  normal: {
    color: "black",
    fontFamily: "robotoB",
    fontSize: size.input
  },
  label: {
    color: "#263238",
    fontFamily: "robotoL",
    fontSize: size.small
  },
  small: {
    color: "#263238",
    fontFamily: "robotoL",
    fontSize: size.small
  },
  description: {
    color: "#263238",
    fontFamily: "robotoL",
    fontSize: size.regular
  },
  oldPrice: {
    fontFamily: "robotoR",
    fontSize: size.small,
    color: "#636e72",
    textDecorationLine: "line-through",
    textDecorationColor: "#636e72"
  },
  newPrice: {
    fontFamily: "robotoB",
    fontSize: size.regular,
    color: "black"
  },
  error: {
    color: "red",
    fontFamily: "robotoR",
    fontSize: size.title
  }
});
