import Reactotron from "reactotron-react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { reactotronRedux } from 'reactotron-redux'

/**
 * Migration to reactotron redux
 */

 const reactotron = Reactotron
 .configure({host:"192.168.100.156" })
 .setAsyncStorageHandler(AsyncStorage)
 .use(reactotronRedux)
 .connect()

 export default reactotron;


/* 
Reactotron.setAsyncStorageHandler(AsyncStorage) // AsyncStorage would either come from `react-native` or `@react-native-community/async-storage` depending on where you get it from
  .configure({ host: "192.168.100.156" }) // controls connection & communication settings
  .use(asyncStorage()) // <------   here we go!
  .useReactNative() // add all built-in react native plugins
  .connect(); // let's connect!
 */