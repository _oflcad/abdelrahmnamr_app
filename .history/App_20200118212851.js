import React, { Component } from 'react';
import AppRoot from './App/App'
import './ReactotronConfig'
export default class App extends Component {
  render() {
    return (
      <AppRoot />
    );
  }
}
