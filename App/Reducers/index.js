import { combineReducers } from "redux";
import  AppReducer  from "./App/AppReducer";
import AuthReducer  from "./Auth/AuthReducer";
import MediaReducer  from "./Media/MediaReducer";
const reducers = combineReducers({
  app: AppReducer,
  auth: AuthReducer,
  media: MediaReducer
});

export default reducers;
