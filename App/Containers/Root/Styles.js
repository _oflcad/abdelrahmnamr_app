import { StyleSheet } from 'react-native'
import { Helpers, Metrics, Fonts, Colors } from '../../Theme'

export default StyleSheet.create({
  buttonContainer: {
    backgroundColor: Colors.white,
    height: 50,
    width: Metrics.width / 2.5,
    borderRadius: 25
  },
  buttonWrapper: {
    bottom: 100,
    left: 0,
    position: 'absolute',
    right: 0,
  },
  error: {
    ...Fonts.normal,
    color: Colors.error,
    marginBottom: Metrics.tiny,
    textAlign: 'center',
  },
  instructions: {
    ...Fonts.normal,
    fontStyle: 'italic',
    marginBottom: Metrics.tiny,
    textAlign: 'center',
  },
  logoContainer: {
    ...Helpers.fullWidth,
    height: 300,
    marginBottom: 25,
  },
  result: {
    ...Fonts.normal,
    marginBottom: Metrics.tiny,
    textAlign: 'center',
  },
  text: {
    ...Fonts.normal,
    marginBottom: Metrics.tiny,
    textAlign: 'center',
  },
})
