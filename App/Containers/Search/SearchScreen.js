import React from "react";
import { SafeAreaView } from "react-native";
import { connect } from "react-redux";
import { Helpers } from "../../Theme";
import HeaderComponent from "../../Components/Header/HeaderComponent";
import SearchSoundScroller from "../../Components/SearchSoundScroller/SearchSoundScroller";
import { fetchAllAudios, getUserAudios } from "../../Actions/Media/Actions";
class SearchScreen extends React.Component {
  componentDidMount() {
    if (this.props.isAuthenticated) {
      this.props.onDispatchFetchLatestAudios();
    }
  }

  render() {
    return (
      <SafeAreaView style={[Helpers.fill]}>
        <HeaderComponent />

        <SearchSoundScroller />
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated
});

const mapDispatchToProps = dispatch => ({
  onDispatchFetchLatestAudios: () => dispatch(fetchAllAudios()),
  onDispatchFetchUserAudios: () => dispatch(getUserAudios())
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchScreen);
