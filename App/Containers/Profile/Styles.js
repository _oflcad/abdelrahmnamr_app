import { StyleSheet } from "react-native";
import { Helpers, Metrics, Fonts, Colors } from "../../Theme";

export default StyleSheet.create({
  addPictureContainer: {
    width: Metrics.width / 3,
    height: Metrics.width / 3,
    borderRadius: 100,
    backgroundColor: Colors.grey100,
    opacity: 0.8
  },
  absolute: {
    position: "absolute",
    zIndex:10,
    bottom: 10,
    justifyContent: "center",
    alignItems: "center"
  },
  textInput: {
    width: "100%",
    height: 50,
    textAlign: "left",
    borderBottomColor: Colors.grey200,
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  dialCodeTextInput: {
    width: "30%",
    height: 50,
    textAlign: "left",
    borderBottomColor: Colors.grey200,
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  phoneNumberTextInput: {
    width: "65%",
    height: 50,
    textAlign: "left",
    borderBottomColor: Colors.grey200,
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  dateInput: {
    width: "100%",
    height: 50,
    textAlign: "left",
    borderBottomColor: Colors.grey200,
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  buttonContainer: {
    width: "95%",
    height: 40,
    ...Helpers.center,
    backgroundColor: Colors.black,
    borderRadius: 10,
    borderColor: Colors.black,
    borderWidth: StyleSheet.hairlineWidth
  },
  profileUrl: {
    width: Metrics.width / 3,
    height: Metrics.width / 3,
    borderRadius: 100,
  },
  pickerSelectStyles: {
    fontSize: 16,
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    backgroundColor: 'white',
    color: 'black',
  },
});
