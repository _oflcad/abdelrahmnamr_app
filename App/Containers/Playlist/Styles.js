import { StyleSheet } from "react-native";
import { Helpers, Metrics, Fonts, Colors } from "../../Theme";

export default StyleSheet.create({
  list: {
    flex: 1
  },
  error: {
    ...Fonts.normal,
    color: Colors.error,
    marginBottom: Metrics.tiny,
    textAlign: "center"
  },
  instructions: {
    ...Fonts.normal,
    fontStyle: "italic",
    marginBottom: Metrics.tiny,
    textAlign: "center"
  },
  logoContainer: {
    ...Helpers.fullWidth,
    height: 300,
    marginBottom: 25
  },
  result: {
    ...Fonts.normal,
    marginBottom: Metrics.tiny,
    textAlign: "center"
  },
  text: {
    ...Fonts.normal,
    marginBottom: Metrics.tiny,
    textAlign: "center"
  }
});
