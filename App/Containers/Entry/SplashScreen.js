import React, { Component } from "react";
import { View, Text, ImageBackground, ActivityIndicator } from "react-native";
import { connect } from "react-redux";
import { Helpers, Images, Metrics, Colors, Fonts, Words } from "../../Theme";
import Style from "./Styles";
import { TouchableOpacity } from "react-native-gesture-handler";
import { setFirstTimer } from "../../Actions/Auth/Actions";
import { hideMiniPlayer } from "../../Actions/Media/Actions";
import NavigationService from "../../Services/NavigationService";
export class Entry extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false
    };
  }

  render() {
    return (
      <View style={[Helpers.fill]}>
        <ImageBackground
          source={Images.firstTimerBg}
          style={[Helpers.fullSize]}
        >
          <View
            style={[
              Metrics.mediumHorizontalMargin,
              Metrics.mediumVerticalMargin,
              Helpers.center,
              {
                bottom: 100,
                left: 0,
                position: "absolute",
                right: 0,
                backgroundColor: Colors.transparent
              }
            ]}
          >
            <ActivityIndicator size={"large"} color={Colors.white} />
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  isFirsTimer: state.auth.isFirsTimer
});

const mapDispatchToProps = dispatch => {
  return {
    onDispatchsetFirstTimer: val => dispatch(setFirstTimer(val)),
    onDispatchHideMiniPlayer: () => dispatch(hideMiniPlayer())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Entry);
