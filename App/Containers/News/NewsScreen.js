import React, { Component } from "react";
import { View, Text, FlatList, ScrollView, SafeAreaView } from "react-native";
import NestedHeader from "../../Components/NestedHeader";
import { connect } from "react-redux";
import { Helpers, Fonts, Metrics, Words, Data } from "../../Theme";
import { fetchLatestNews } from "../../Actions/Media/Actions";
import ReusableCard from "../../Components/ReusableCard/ReusableCard";
class NewsScreen extends Component {
  render() {
    const { language } = this.props;
    let title = language === "ar" ? Words.ar.nestedHeader.News : null;
    return (
      <SafeAreaView style={[Helpers.fill]}>
        <NestedHeader customTitle={title} />
        <View style={[language === "ar" ? Helpers.rowReverse : null]}>
          <Text
            style={[
              Fonts.h4,
              Metrics.mediumHorizontalMargin,
              Metrics.smallVerticalMargin
            ]}
          >
            {language === "en" ? Words.en.news.news : Words.ar.news.news}
          </Text>
        </View>
        <FlatList
          showsVerticalScrollIndicator={false}
          style={[Metrics.smallHorizontalMargin]}
          data={Data.News}
          renderItem={({ item }) => <ReusableCard item={item} />}
          keyExtractor={item => "key" + item.id}
        />
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  language: state.auth.language
});

const mapDispatchToProps = dispatch => {
  return {
    onDispatchFetchLatestNews: () => dispatch(fetchLatestNews())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsScreen);
