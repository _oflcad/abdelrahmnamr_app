import { StyleSheet } from "react-native";
import { Helpers, Metrics, Fonts, Colors } from "../../Theme";

export default StyleSheet.create({
    divider: {
        width: Metrics.width - 100,
        height: StyleSheet.hairlineWidth,
        backgroundColor: Colors.grey100,
    },
    input: {
        ...Metrics.verticalMargin,
        width: Metrics.width - 30,
        height: 40,
        color: Colors.black,
        textAlign: "left",
        borderRadius: 10,
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: Colors.grey200,
    },
    buttonContainer: {
        width: Metrics.width - 30,
        height: 50,
        backgroundColor: Colors.black,
        borderRadius: 10,
        ...Helpers.center
    }
});
