import { StyleSheet } from "react-native";
import { Helpers, Metrics, Fonts, Colors } from "../../Theme";

export default StyleSheet.create({
    image: {
        width: Metrics.width - 50,
        height: Metrics.width - 50,
        borderRadius: 10,
    },
    addButton: {
        ...Helpers.row,
        ...Helpers.mainSpaceAround,
        ...Helpers.crossCenter,
        ...Metrics.horizontalMargin,
        ...Metrics.verticalMargin,
        width: Metrics.width - 150,
        height: 50,
        backgroundColor: Colors.primary,
        borderRadius: 10,
    }
});
