/**
 * This file contains metric values that are global to the application.
 */

// Examples of metrics you can define:
import { Dimensions } from "react-native";
import { getBottomSpace } from "react-native-iphone-x-helper";

const { height, width } = Dimensions.get("window");

export const tiny = 5;
export const small = tiny * 2; // 10
export const normal = tiny * 3; // 15
export const medium = normal * 2; // 30
export const large = medium * 2; // 60

/**
 * PLAYER VARIABLES
 */
const TABBAR_HEIGHT = getBottomSpace() + 50;
const MINIMIZED_PLAYER_HEIGHT = 50;
const SNAP_TOP = 0;
const SNAP_BOTTOM = height - MINIMIZED_PLAYER_HEIGHT;

export default {
  height,
  width,
  TABBAR_HEIGHT,
  MINIMIZED_PLAYER_HEIGHT,
  SNAP_TOP,
  SNAP_BOTTOM,
  bottomMargin: {
    marginBottom: normal
  },
  mediumBottomMargin: {
    marginBottom: medium
  },

  tinyVerticalMargin: {
    marginVertical: tiny
  },
  smallVerticalMargin: {
    marginVertical: small
  },
  verticalMargin: {
    marginVertical: normal
  },
  mediumVerticalMargin: {
    marginVertical: medium
  },

  tinyHorizontalMargin: {
    marginHorizontal: tiny
  },
  smallHorizontalMargin: {
    marginHorizontal: small
  },
  horizontalMargin: {
    marginHorizontal: normal
  },
  mediumHorizontalMargin: {
    marginHorizontal: medium
  },

  tinyHorizontalPadding: {
    paddingHorizontal: tiny
  },
  smallHorizontalPadding: {
    paddingHorizontal: small
  },
  horizontalPadding: {
    paddingHorizontal: normal
  },
  mediumHorizontalPadding: {
    paddingHorizontal: medium
  },

  tinyVerticalPadding: {
    paddingVertical: tiny
  },
  smallVerticalPadding: {
    paddingVertical: small
  },
  verticalPadding: {
    paddingVertical: normal
  },
  mediumVerticalPadding: {
    paddingVertical: medium
  },
  marginTop: {
    marginTop: normal
  },
  tinyMarginTop: {
    marginTop: tiny
  },
  smallMarginTop: {
    marginTop: small
  },
  mediumMarginTop: {
    marginTop: medium
  },
  marginBottom: {
    marginBottom: normal
  },
  tinyMarginBottom: {
    marginBottom: tiny
  },
  smallMarginBottom: {
    marginBottom: small
  },
  mediumMarginBottom: {
    marginBottom: medium
  },
  scrollViewBottomMargin: {
    flexGrow: 1,
    paddingBottom: large + medium
  }
};
