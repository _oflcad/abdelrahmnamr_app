export default  [
  {
    "label": "+93 Afghanistan",
    "value": "AF"
  },
  {
    "label": "+355 Albania",
    "value": "AL"
  },
  {
    "label": "+213 Algeria",
    "value": "DZ"
  },
  {
    "label": "+1 684 AmericanSamoa",
    "value": "AS"
  },
  {
    "label": "+376 Andorra",
    "value": "AD"
  },
  {
    "label": "+244 Angola",
    "value": "AO"
  },
  {
    "label": "+1 264 Anguilla",
    "value": "AI"
  },
  {
    "label": "+1268 Antigua and Barbuda",
    "value": "AG"
  },
  {
    "label": "+54 Argentina",
    "value": "AR"
  },
  {
    "label": "+374 Armenia",
    "value": "AM"
  },
  {
    "label": "+297 Aruba",
    "value": "AW"
  },
  {
    "label": "+61 Australia",
    "value": "AU"
  },
  {
    "label": "+43 Austria",
    "value": "AT"
  },
  {
    "label": "+994 Azerbaijan",
    "value": "AZ"
  },
  {
    "label": "+1 242 Bahamas",
    "value": "BS"
  },
  {
    "label": "+973 Bahrain",
    "value": "BH"
  },
  {
    "label": "+880 Bangladesh",
    "value": "BD"
  },
  {
    "label": "+1 246 Barbados",
    "value": "BB"
  },
  {
    "label": "+375 Belarus",
    "value": "BY"
  },
  {
    "label": "+32 Belgium",
    "value": "BE"
  },
  {
    "label": "+501 Belize",
    "value": "BZ"
  },
  {
    "label": "+229 Benin",
    "value": "BJ"
  },
  {
    "label": "+1 441 Bermuda",
    "value": "BM"
  },
  {
    "label": "+975 Bhutan",
    "value": "BT"
  },
  {
    "label": "+387 Bosnia and Herzegovina",
    "value": "BA"
  },
  {
    "label": "+267 Botswana",
    "value": "BW"
  },
  {
    "label": "+55 Brazil",
    "value": "BR"
  },
  {
    "label": "+246 British Indian Ocean Territory",
    "value": "IO"
  },
  {
    "label": "+359 Bulgaria",
    "value": "BG"
  },
  {
    "label": "+226 Burkina Faso",
    "value": "BF"
  },
  {
    "label": "+257 Burundi",
    "value": "BI"
  },
  {
    "label": "+855 Cambodia",
    "value": "KH"
  },
  {
    "label": "+237 Cameroon",
    "value": "CM"
  },
  {
    "label": "+1 Canada",
    "value": "CA"
  },
  {
    "label": "+238 Cape Verde",
    "value": "CV"
  },
  {
    "label": "+ 345 Cayman Islands",
    "value": "KY"
  },
  {
    "label": "+236 Central African Republic",
    "value": "CF"
  },
  {
    "label": "+235 Chad",
    "value": "TD"
  },
  {
    "label": "+56 Chile",
    "value": "CL"
  },
  {
    "label": "+86 China",
    "value": "CN"
  },
  {
    "label": "+61 Christmas Island",
    "value": "CX"
  },
  {
    "label": "+57 Colombia",
    "value": "CO"
  },
  {
    "label": "+269 Comoros",
    "value": "KM"
  },
  {
    "label": "+242 Congo",
    "value": "CG"
  },
  {
    "label": "+682 Cook Islands",
    "value": "CK"
  },
  {
    "label": "+506 Costa Rica",
    "value": "CR"
  },
  {
    "label": "+385 Croatia",
    "value": "HR"
  },
  {
    "label": "+53 Cuba",
    "value": "CU"
  },
  {
    "label": "+537 Cyprus",
    "value": "CY"
  },
  {
    "label": "+420 Czech Republic",
    "value": "CZ"
  },
  {
    "label": "+45 Denmark",
    "value": "DK"
  },
  {
    "label": "+253 Djibouti",
    "value": "DJ"
  },
  {
    "label": "+1 767 Dominica",
    "value": "DM"
  },
  {
    "label": "+1 849 Dominican Republic",
    "value": "DO"
  },
  {
    "label": "+593 Ecuador",
    "value": "EC"
  },
  {
    "label": "+20 Egypt",
    "value": "EG"
  },
  {
    "label": "+503 El Salvador",
    "value": "SV"
  },
  {
    "label": "+240 Equatorial Guinea",
    "value": "GQ"
  },
  {
    "label": "+291 Eritrea",
    "value": "ER"
  },
  {
    "label": "+372 Estonia",
    "value": "EE"
  },
  {
    "label": "+251 Ethiopia",
    "value": "ET"
  },
  {
    "label": "+298 Faroe Islands",
    "value": "FO"
  },
  {
    "label": "+679 Fiji",
    "value": "FJ"
  },
  {
    "label": "+358 Finland",
    "value": "FI"
  },
  {
    "label": "+33 France",
    "value": "FR"
  },
  {
    "label": "+594 French Guiana",
    "value": "GF"
  },
  {
    "label": "+689 French Polynesia",
    "value": "PF"
  },
  {
    "label": "+241 Gabon",
    "value": "GA"
  },
  {
    "label": "+220 Gambia",
    "value": "GM"
  },
  {
    "label": "+995 Georgia",
    "value": "GE"
  },
  {
    "label": "+49 Germany",
    "value": "DE"
  },
  {
    "label": "+233 Ghana",
    "value": "GH"
  },
  {
    "label": "+350 Gibraltar",
    "value": "GI"
  },
  {
    "label": "+30 Greece",
    "value": "GR"
  },
  {
    "label": "+299 Greenland",
    "value": "GL"
  },
  {
    "label": "+1 473 Grenada",
    "value": "GD"
  },
  {
    "label": "+590 Guadeloupe",
    "value": "GP"
  },
  {
    "label": "+1 671 Guam",
    "value": "GU"
  },
  {
    "label": "+502 Guatemala",
    "value": "GT"
  },
  {
    "label": "+224 Guinea",
    "value": "GN"
  },
  {
    "label": "+245 Guinea-Bissau",
    "value": "GW"
  },
  {
    "label": "+595 Guyana",
    "value": "GY"
  },
  {
    "label": "+509 Haiti",
    "value": "HT"
  },
  {
    "label": "+504 Honduras",
    "value": "HN"
  },
  {
    "label": "+36 Hungary",
    "value": "HU"
  },
  {
    "label": "+354 Iceland",
    "value": "IS"
  },
  {
    "label": "+91 India",
    "value": "IN"
  },
  {
    "label": "+62 Indonesia",
    "value": "ID"
  },
  {
    "label": "+964 Iraq",
    "value": "IQ"
  },
  {
    "label": "+353 Ireland",
    "value": "IE"
  },
  {
    "label": "+972 Israel",
    "value": "IL"
  },
  {
    "label": "+39 Italy",
    "value": "IT"
  },
  {
    "label": "+1 876 Jamaica",
    "value": "JM"
  },
  {
    "label": "+81 Japan",
    "value": "JP"
  },
  {
    "label": "+962 Jordan",
    "value": "JO"
  },
  {
    "label": "+7 7 Kazakhstan",
    "value": "KZ"
  },
  {
    "label": "+254 Kenya",
    "value": "KE"
  },
  {
    "label": "+686 Kiribati",
    "value": "KI"
  },
  {
    "label": "+965 Kuwait",
    "value": "KW"
  },
  {
    "label": "+996 Kyrgyzstan",
    "value": "KG"
  },
  {
    "label": "+371 Latvia",
    "value": "LV"
  },
  {
    "label": "+961 Lebanon",
    "value": "LB"
  },
  {
    "label": "+266 Lesotho",
    "value": "LS"
  },
  {
    "label": "+231 Liberia",
    "value": "LR"
  },
  {
    "label": "+423 Liechtenstein",
    "value": "LI"
  },
  {
    "label": "+370 Lithuania",
    "value": "LT"
  },
  {
    "label": "+352 Luxembourg",
    "value": "LU"
  },
  {
    "label": "+261 Madagascar",
    "value": "MG"
  },
  {
    "label": "+265 Malawi",
    "value": "MW"
  },
  {
    "label": "+60 Malaysia",
    "value": "MY"
  },
  {
    "label": "+960 Maldives",
    "value": "MV"
  },
  {
    "label": "+223 Mali",
    "value": "ML"
  },
  {
    "label": "+356 Malta",
    "value": "MT"
  },
  {
    "label": "+692 Marshall Islands",
    "value": "MH"
  },
  {
    "label": "+596 Martinique",
    "value": "MQ"
  },
  {
    "label": "+222 Mauritania",
    "value": "MR"
  },
  {
    "label": "+230 Mauritius",
    "value": "MU"
  },
  {
    "label": "+262 Mayotte",
    "value": "YT"
  },
  {
    "label": "+52 Mexico",
    "value": "MX"
  },
  {
    "label": "+377 Monaco",
    "value": "MC"
  },
  {
    "label": "+976 Mongolia",
    "value": "MN"
  },
  {
    "label": "+382 Montenegro",
    "value": "ME"
  },
  {
    "label": "+1664 Montserrat",
    "value": "MS"
  },
  {
    "label": "+212 Morocco",
    "value": "MA"
  },
  {
    "label": "+95 Myanmar",
    "value": "MM"
  },
  {
    "label": "+264 Namibia",
    "value": "NA"
  },
  {
    "label": "+674 Nauru",
    "value": "NR"
  },
  {
    "label": "+977 Nepal",
    "value": "NP"
  },
  {
    "label": "+31 Netherlands",
    "value": "NL"
  },
  {
    "label": "+599 Netherlands Antilles",
    "value": "AN"
  },
  {
    "label": "+687 New Caledonia",
    "value": "NC"
  },
  {
    "label": "+64 New Zealand",
    "value": "NZ"
  },
  {
    "label": "+505 Nicaragua",
    "value": "NI"
  },
  {
    "label": "+227 Niger",
    "value": "NE"
  },
  {
    "label": "+234 Nigeria",
    "value": "NG"
  },
  {
    "label": "+683 Niue",
    "value": "NU"
  },
  {
    "label": "+672 Norfolk Island",
    "value": "NF"
  },
  {
    "label": "+1 670 Northern Mariana Islands",
    "value": "MP"
  },
  {
    "label": "+47 Norway",
    "value": "NO"
  },
  {
    "label": "+968 Oman",
    "value": "OM"
  },
  {
    "label": "+92 Pakistan",
    "value": "PK"
  },
  {
    "label": "+680 Palau",
    "value": "PW"
  },
  {
    "label": "+507 Panama",
    "value": "PA"
  },
  {
    "label": "+675 Papua New Guinea",
    "value": "PG"
  },
  {
    "label": "+595 Paraguay",
    "value": "PY"
  },
  {
    "label": "+51 Peru",
    "value": "PE"
  },
  {
    "label": "+63 Philippines",
    "value": "PH"
  },
  {
    "label": "+48 Poland",
    "value": "PL"
  },
  {
    "label": "+351 Portugal",
    "value": "PT"
  },
  {
    "label": "+1 939 Puerto Rico",
    "value": "PR"
  },
  {
    "label": "+974 Qatar",
    "value": "QA"
  },
  {
    "label": "+40 Romania",
    "value": "RO"
  },
  {
    "label": "+250 Rwanda",
    "value": "RW"
  },
  {
    "label": "+685 Samoa",
    "value": "WS"
  },
  {
    "label": "+378 San Marino",
    "value": "SM"
  },
  {
    "label": "+966 Saudi Arabia",
    "value": "SA"
  },
  {
    "label": "+221 Senegal",
    "value": "SN"
  },
  {
    "label": "+381 Serbia",
    "value": "RS"
  },
  {
    "label": "+248 Seychelles",
    "value": "SC"
  },
  {
    "label": "+232 Sierra Leone",
    "value": "SL"
  },
  {
    "label": "+65 Singapore",
    "value": "SG"
  },
  {
    "label": "+421 Slovakia",
    "value": "SK"
  },
  {
    "label": "+386 Slovenia",
    "value": "SI"
  },
  {
    "label": "+677 Solomon Islands",
    "value": "SB"
  },
  {
    "label": "+27 South Africa",
    "value": "ZA"
  },
  {
    "label": "+500 South Georgia and the South Sandwich Islands",
    "value": "GS"
  },
  {
    "label": "+34 Spain",
    "value": "ES"
  },
  {
    "label": "+94 Sri Lanka",
    "value": "LK"
  },
  {
    "label": "+249 Sudan",
    "value": "SD"
  },
  {
    "label": "+597 Suriname",
    "value": "SR"
  },
  {
    "label": "+268 Swaziland",
    "value": "SZ"
  },
  {
    "label": "+46 Sweden",
    "value": "SE"
  },
  {
    "label": "+41 Switzerland",
    "value": "CH"
  },
  {
    "label": "+992 Tajikistan",
    "value": "TJ"
  },
  {
    "label": "+66 Thailand",
    "value": "TH"
  },
  {
    "label": "+228 Togo",
    "value": "TG"
  },
  {
    "label": "+690 Tokelau",
    "value": "TK"
  },
  {
    "label": "+676 Tonga",
    "value": "TO"
  },
  {
    "label": "+1 868 Trinidad and Tobago",
    "value": "TT"
  },
  {
    "label": "+216 Tunisia",
    "value": "TN"
  },
  {
    "label": "+90 Turkey",
    "value": "TR"
  },
  {
    "label": "+993 Turkmenistan",
    "value": "TM"
  },
  {
    "label": "+1 649 Turks and Caicos Islands",
    "value": "TC"
  },
  {
    "label": "+688 Tuvalu",
    "value": "TV"
  },
  {
    "label": "+256 Uganda",
    "value": "UG"
  },
  {
    "label": "+380 Ukraine",
    "value": "UA"
  },
  {
    "label": "+971 United Arab Emirates",
    "value": "AE"
  },
  {
    "label": "+44 United Kingdom",
    "value": "GB"
  },
  {
    "label": "+1 United States",
    "value": "US"
  },
  {
    "label": "+598 Uruguay",
    "value": "UY"
  },
  {
    "label": "+998 Uzbekistan",
    "value": "UZ"
  },
  {
    "label": "+678 Vanuatu",
    "value": "VU"
  },
  {
    "label": "+681 Wallis and Futuna",
    "value": "WF"
  },
  {
    "label": "+967 Yemen",
    "value": "YE"
  },
  {
    "label": "+260 Zambia",
    "value": "ZM"
  },
  {
    "label": "+263 Zimbabwe",
    "value": "ZW"
  },
  {
    "label": " land Islands",
    "value": "AX"
  },
  {
    "label": "+591 Bolivia, Plurinational State of",
    "value": "BO"
  },
  {
    "label": "+673 Brunei Darussalam",
    "value": "BN"
  },
  {
    "label": "+61 Cocos (Keeling) Islands",
    "value": "CC"
  },
  {
    "label": "+243 Congo, The Democratic Republic of the",
    "value": "CD"
  },
  {
    "label": "+225 Cote d'Ivoire",
    "value": "CI"
  },
  {
    "label": "+500 Falkland Islands (Malvinas)",
    "value": "FK"
  },
  {
    "label": "+44 Guernsey",
    "value": "GG"
  },
  {
    "label": "+379 Holy See (Vatican City State)",
    "value": "VA"
  },
  {
    "label": "+852 Hong Kong",
    "value": "HK"
  },
  {
    "label": "+98 Iran, Islamic Republic of",
    "value": "IR"
  },
  {
    "label": "+44 Isle of Man",
    "value": "IM"
  },
  {
    "label": "+44 Jersey",
    "value": "JE"
  },
  {
    "label": "+850 Korea, Democratic People's Republic of",
    "value": "KP"
  },
  {
    "label": "+82 Korea, Republic of",
    "value": "KR"
  },
  {
    "label": "+856 Lao People's Democratic Republic",
    "value": "LA"
  },
  {
    "label": "+218 Libyan Arab Jamahiriya",
    "value": "LY"
  },
  {
    "label": "+853 Macao",
    "value": "MO"
  },
  {
    "label": "+389 Macedonia, The Former Yugoslav Republic of",
    "value": "MK"
  },
  {
    "label": "+691 Micronesia, Federated States of",
    "value": "FM"
  },
  {
    "label": "+373 Moldova, Republic of",
    "value": "MD"
  },
  {
    "label": "+258 Mozambique",
    "value": "MZ"
  },
  {
    "label": "+970 Palestinian Territory, Occupied",
    "value": "PS"
  },
  {
    "label": "+872 Pitcairn",
    "value": "PN"
  },
  {
    "label": "+262 Réunion",
    "value": "RE"
  },
  {
    "label": "+7 Russia",
    "value": "RU"
  },
  {
    "label": "+590 Saint Barthélemy",
    "value": "BL"
  },
  {
    "label": "+290 Saint Helena, Ascension and Tristan Da Cunha",
    "value": "SH"
  },
  {
    "label": "+1 869 Saint Kitts and Nevis",
    "value": "KN"
  },
  {
    "label": "+1 758 Saint Lucia",
    "value": "LC"
  },
  {
    "label": "+590 Saint Martin",
    "value": "MF"
  },
  {
    "label": "+508 Saint Pierre and Miquelon",
    "value": "PM"
  },
  {
    "label": "+1 784 Saint Vincent and the Grenadines",
    "value": "VC"
  },
  {
    "label": "+239 Sao Tome and Principe",
    "value": "ST"
  },
  {
    "label": "+252 Somalia",
    "value": "SO"
  },
  {
    "label": "+47 Svalbard and Jan Mayen",
    "value": "SJ"
  },
  {
    "label": "+963 Syrian Arab Republic",
    "value": "SY"
  },
  {
    "label": "+886 Taiwan, Province of China",
    "value": "TW"
  },
  {
    "label": "+255 Tanzania, United Republic of",
    "value": "TZ"
  },
  {
    "label": "+670 Timor-Leste",
    "value": "TL"
  },
  {
    "label": "+58 Venezuela, Bolivarian Republic of",
    "value": "VE"
  },
  {
    "label": "+84 Viet Nam",
    "value": "VN"
  },
  {
    "label": "+1 284 Virgin Islands, British",
    "value": "VG"
  },
  {
    "label": "+1 340 Virgin Islands, U.S.",
    "value": "VI"
  }
];
