/**
 * This file contains the application's colors.
 *
 * Define color here instead of duplicating them throughout the components.
 * That allows to change them more easily later on.
 */

export default {
  transparent: 'rgba(0,0,0,0)',
  // Example colors:
  white: '#ffffff',
  black: '#000',
  text: '#212529',
  primary: '#3282b8',
  success: '#28a745',
  error: '#dc3545',
  activeColor: '#2d3436',
  inActiveColor: '#b2bec3',
  grey100: "#CFD8DC",
  grey150: "#636e72",
  grey200: "#263238",
  green100: "#55efc4",
  modalTop: '#e6e6e6'
}
