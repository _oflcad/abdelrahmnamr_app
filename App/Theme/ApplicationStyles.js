/**
 * This file defines the base application styles.
 *
 * Use it to define generic component styles (e.g. the default text styles, default button styles...).
 */

import Colors from "./Colors";
import { StyleSheet } from "react-native";
import Metrics from "./Metrics";

export default {
  button: {
    backgroundColor: Colors.primary
  },
  dividerHorizontal: {
    ...Metrics.smallHorizontalMargin,
    ...Metrics.tinyVerticalMargin,
    width: Metrics.width - 25,
    height: StyleSheet.hairlineWidth,
    backgroundColor: Colors.black,
  },
  modalDividerHorizontal: {
    width: '95%',
    height: StyleSheet.hairlineWidth,
    backgroundColor: Colors.black,
    margin: 10
  },
  modalBackground: {
    width: "100%",
    height: "100%",
    backgroundColor: Colors.grey100,
    opacity: 0.2,
    justifyContent: "center",
    alignItems: "center"
  },
  playlistModal: {
    position: "absolute",
    width: Metrics.width - 60,
    height: Metrics.width - 60,
    backgroundColor: "white",
    flex: 1,
    justifyContent: "space-around",
    alignItems: "center",
    borderRadius: 20
  },
  addSongsModal: {
    position: "absolute",
    width: Metrics.width - 20,
    height: Metrics.width ,
    backgroundColor: "white",
    flex: 1,
    borderRadius: 20
  },
  playlistInfoModal: {
    position: "absolute",
    width: Metrics.width - 60,
    height: Metrics.width - 60,
    backgroundColor: "white",
    flex: 1,
    justifyContent: "space-around",
    alignItems: "center",
    borderRadius: 20
  },
  basketItemModal: {
    position: 'absolute',
    width: Metrics.width - 60,
    height: Metrics.width - 60,
    backgroundColor: Colors.white,
    flex:1,
    flexDirection: 'column',
    justifyContent: "space-around",
    alignItems: "center",
    borderRadius: 20,
  },
  settingsSectionTitleContainer: {
    width: '100%',
    height: 40,
    backgroundColor: Colors.grey100,
    justifyContent: "center"
  }
};
