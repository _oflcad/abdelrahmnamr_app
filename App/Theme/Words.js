/**
 * This file contains the application's common used text.
 *
 * Define color here instead of duplicating them throughout the components.
 * That allows to change them more easily later on.
 */

export default {
  arabic: "العربية",
  english: "English",

  en: {
    auth: {
      authErrorMsg: "Please check your credentials",
      forgotPassword: "Forgot Password ?"
    },
    registerSuccess: "Please check your email",
    logout: "Log out",
    addToBasket: "Add To Basket",
    emptyBasket: "Your cart is empty",
    audioAddedToBasket: "Audio Added to Basket",
    audioAlreadyInBasket: "Audio is Already bought",
    itemsPurshased: "Items Purchased Successfully",
    purshased: "Purchased",
    emptyDownloadedPlaylist: "You don't have any downloaded playlists",
    emptyPlaylist: "You don't have any",
    allAudios: "All Audios",
    homeTitle: "Latest Audio",
    addPlaylist: "Create a new playlist",
    addPlaylistPlaceholder: "Title",
    addButtonText: "Add Playlist",
    addPlaylistError: "At leaset 4 characters",
    newPlaylistAdded: "New Playlist Added",
    renamePlaylist: "Rename",
    deletePlaylist: "Delete",
    check: "Are You Sure ?",
    confirm: "Yes",
    deny: "No",
    playAll: "Play All",
    addAudio: "Add Audio",
    songAdded: "been added to playlist",
    Help: "Help",
    password: "Password",
    singInMessage: "Sign in or create an account",
    accesAccountMessage: "Access your account",
    signUp: {
      welcomeMsg: "Welcome",
      buttonText: "Sign Up",
      email: "Email",
      name: "Name",
      lastName: "Surname",
      country: "Country",
      phone: "Phone",
      password: "Password",
      username: "Username",
      error: "Please check your information"
    },
    signIn: {
      welcomeMsg: "Welcome",
      socialMediaTitle: "Continue With Social Media",
      socialMediaSubtitle: "or continue with email",
      buttonText: "Log in",
      email: "Email",
      password: "Password",
      settings: {
        Profile: "My Account",
        Settings: "My Settings",
        Language: "My Language",
        Support: "Support",
        GetInTouch: "Get In Touch"
      },
      profile: {
        fullName: "Enter Full Name",
        dialCode: "Dial Code",
        mobileNumber: "Mobile Number",
        email: "Email Address",
        date: "Date(dd/mm/yyyy)",
        gender: "Gender",
        dialCodePlaceHolder: "Select Code"
      },
      accountSection: {
        basket: "Basket",
        playlist: "My Playlists",
        downloads: "Downloads"
      },
      settingsSection: {
        autoPlay: "Auto Play"
      },
      languageSection: {
        english: "English",
        arabic: "العربية"
      },
      SupportSection: {
        contactUs: "Contact Us",
        shareTheApp: "Share",
        rateTheApp: "Rate"
      },
      SocialSection: {
        facebook: "Facebook",
        youtube: "Youtube",
        instagram: "Instagram"
      },
      downloads: {
        downloads: "My Downloads"
      },
      news: {
        news: "Latest News",
        noData: "There are no sessions"
      },
      blog: {
        blog: "Latest Blogs"
      },
      session: {
        session: "Latest Sessions"
      }
    },
    settings: {
      Profile: "My Account",
      Settings: "My Settings",
      Language: "My Language",
      Support: "Support",
      GetInTouch: "Get In Touch"
    },
    profile: {
      fullName: "Enter Full Name",
      dialCode: "Dial Code",
      mobileNumber: "Mobile Number",
      email: "Email Address",
      date: "Date(dd/mm/yyyy)",
      gender: "Gender",
      dialCodePlaceHolder: "Select Code"
    },
    accountSection: {
      basket: "Basket",
      playlist: "My Playlists",
      downloads: "Downloads"
    },
    settingsSection: {
      autoPlay: "Auto Play"
    },
    languageSection: {
      english: "English",
      arabic: "العربية"
    },
    SupportSection: {
      contactUs: "Contact Us",
      shareTheApp: "Share",
      rateTheApp: "Rate"
    },
    SocialSection: {
      facebook: "Facebook",
      youtube: "Youtube",
      instagram: "Instagram"
    },
    downloads: {
      downloads: "My Downloads"
    },
    news: {
      news: "Latest News",
      noData: "There are no sessions"
    },
    blog: {
      blog: "Latest Blogs"
    },
    session: {
      session: "Latest Sessions"
    },
    playlist: {
      msg: "Please Authenticate before",
    },
    checkOut: {
      msg: "Please Provide Receit's Picture",
    },
  },
  ar: {
    auth: {
      authErrorMsg: "يرجى التحقق من المعلومات الخاصة بك",
      forgotPassword: "نسيت كلمة المرور؟ّ"
    },
    registerSuccess: "من فضلك تفقد بريدك الالكتروني",
    logout: "تسجيل خروج",
    addToBasket: "اضف الى السلة",
    emptyBasket: "سلتك فارغة",
    emptyDownloadedPlaylist: "ليس لديك أي قوائم تشغيل تم تنزيلها",
    audioAddedToBasket: "تم إضافة الصوت إلى السلة",
    audioAlreadyInBasket: "تم شراء الصوت من قبل",
    emptyPlaylist: "ليس لديك أي",
    itemsPurshased: "تمة شراء الصوتيات بنجاح",
    purshased: "صوتياتك",
    allAudios: "إصدارات حديثة",
    homeTitle: "إصدارات حديثة",
    addPlaylist: "قائمة تشغيل جديدة",
    addPlaylistPlaceholder: "العنوان",
    addButtonText: "إضافة قائمة التشغيل",
    addPlaylistError: "ما لا يقل عن 3 أحرف",
    newPlaylistAdded: "تمت إضافة قائمة تشغيل جديدة",
    renamePlaylist: "إعادة تسمية",
    deletePlaylist: "حذف",
    check: "هل أنت واثق ?",
    confirm: "نعم",
    deny: "لا",
    playAll: "لعب جميع الصوتيات",
    addAudio: "أضف صوتيات",
    songAdded: "تمت إضافته إلى قائمة التشغيل",
    Help: "مساعدة",
    password: "كلمه السر",
    singInMessage: "تسجيل الدخول أو إنشاء حساب",
    accesAccountMessage: "حسابك",
    signIn: {
      welcomeMsg: "مرحبا بك",
      socialMediaTitle: "Continue With Social Media",
      socialMediaSubtitle: "or continue with email",
      buttonText: "تسجيل الدخول",
      email: "البريد الإلكتروني",
      password: "كلمة المرور"
    },
    signUp: {
      welcomeMsg: "مرحبا بك",
      buttonText: "تسجيل",
      email: "البريد الإلكتروني",
      password: "كلمة المرور",
      name: "الإ سم",
      lastName: "إسم العائلة",
      country: "البلد",
      phone: "رقم الهاتف",
      error: "يرجى التحقق من المعلومات الخاصة بك",
      username: "اسم المستخدم"
    },
    settings: {
      Profile: "الحساب",
      Settings: "الإعدادات",
      Language: "اللغة",
      Support: "الدعم",
      GetInTouch: "اتصال"
    },
    profile: {
      fullName: "الاسم الكامل",
      dialCode: "رمز الاتصال",
      mobileNumber: "رقم الهاتف المحمول",
      email: "البريد الإلكتروني",
      date: "Date(dd/mm/yyyy)",
      gender: "الجنس",
      dialCodePlaceHolder: "اختر الكود"
    },
    accountSection: {
      basket: "Basket",
      playlist: "قوائم التشغيل",
      downloads: "التنزيلات"
    },
    settingsSection: {
      autoPlay: "تشغيل تلقائي"
    },
    languageSection: {
      english: "English",
      arabic: "العربية"
    },
    SupportSection: {
      contactUs: "اتصال",
      shareTheApp: "مشاركة التطبيق",
      rateTheApp: "تقييم التطبيق"
    },
    SocialSection: {
      facebook: "Facebook",
      youtube: "Youtube",
      instagram: "Instagram"
    },
    downloads: {
      downloads: "التنزيلات"
    },
    news: {
      news: "أحدث الأخبار",
      noData: "لا توجد جلسات"
    },
    blog: {
      blog: "أحدث المدونات"
    },
    session: {
      session: "آخر الجلسات"
    },
    playlist: {
      msg: "Please Authenticate before",
    },
    header: {
      home: "الرئيسية",
      search: "ابحث هنا",
      playlist: "قائمة التشغيل",
      library: "مكتبتي",
      settings: "الإعدادات"
    },
    nestedHeader: {
      Player: "",
      Profile: "الملف الشخصي",
      SignIn: "تسجيل الدخول",
      SignUp: "التسجيل",
      PlaylistInfo: "",
      Basket: "السلة",
      Notification: "الإشعارات",
      Blog: " المدونات",
      News: "الأخبار",
      Item: "", // Item Card Screen for Blog Display,
      Audio: "الإصدارات" // ItemScreen for audios
    },
    checkOut: {
      msg: 'يرجى تقديم صورة الوصل'
    },
  }
};
