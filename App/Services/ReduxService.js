import Reactotron from "reactotron-react-native";

/**
 * Helper function that take curent song and add it to selected songs in redux
 */
export const addSongToPlaylist = (song, selectedSongs) => {
  //   Reactotron.log('ADD SONGS SERVICE // update selected songs array', selectedSongs.concat(song))
  var index = selectedSongs.some(elem => elem === song);
  console.log("our index ", index);
  if (!index) {
    return selectedSongs.concat(song);
  }
  return selectedSongs;
};

/**
 * Helper function that take curent song and remove it from selected songs in redux
 */
export const removeSongFromPlaylist = (song, selectedSongs) => {
  //   Reactotron.log('REMOVE SONGS SERVICE // update selected songs array', selectedSongs.filter(el => el.title !== song.title))
  Reactotron.log("SENT VARIABLES ", song, selectedSongs);
  var updateArrayCp = selectedSongs.filter(el => el != song);
  Reactotron.log("REMOVE SONS SERVICE ", updateArrayCp);
  return updateArrayCp;
};

/**
 * Helper function that take curent playlist  and update the global user playslists array
 */
export const updateUserPlaylist = (playlist, userPlaylists, updatedSongs) => {
  //   Reactotron.log('UPDATE GLOBAL USER PLAYLIST SERVICE // BEFORE update', userPlaylists)

  var index = userPlaylists.findIndex(play => play.id === playlist.id);

  var updatedCopy = updateSongsInPlaylist(updatedSongs, userPlaylists[index]);
  userPlaylists.splice(index, 1, updatedCopy);
  Reactotron.log(
    "UPDATE GLOBAL USER PLAYLIST SERVICE // AFTER update",
    playlist,
    userPlaylists,
    updatedSongs,
  );
  return userPlaylists;
};

/**
 * Helper function that take curent playlist  and update the displayed user playslist object
 * 
 */
const updateSongsInPlaylist = (songs, obj) => {
  obj.songs = songs;
  return obj;
};


/**
 * UPDATE DOWNLOAD SONGS IN PLAYLIST TO DISPLAY;
 * * @param {*} downloadedAudioArray 
 * * @param {*} downloadedPlaylist 
 * * @param {*} newUserPlaylists 
 */
 export const updatedDownloadedUserPlaylist = (downloadedPlaylist, newUserPlaylists) => {
    let index = newUserPlaylists.findIndex(playlist => _.isEqual(playlist.id, downloadedPlaylist.id));
    let updatedCopy = newUserPlaylists;
    updatedCopy[index] = downloadedPlaylist;
    return updatedCopy;
 }