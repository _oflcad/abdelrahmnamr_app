import React from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import Style from "./Styles";
import { Colors, Helpers, Metrics } from "../../Theme";
import { connect } from "react-redux";
import SoundPriceContainer from "../SoundPriceContainer/SoundPriceContainer";
import { setItemToBasket } from '../../Actions/App/Actions';
import NavigationService from "../../Services/NavigationService";
const SongContainer = props => {
  const { name, price, image, delprice } = props.item;
  const _handleDisplayItem = () => {
    // props.onDispatchPlaySong(props.item)
    //Check if he is authenticated then check if item might exist in user collection
    //if true play song, if false show modal add to basket item description
    props.onDispatchSetItemToBasket(props.item)
    NavigationService.navigate('Audio')
  };

  if (props.language === "en") {
    return (
      <TouchableOpacity style={Style.container} onPress={_handleDisplayItem}>
        <Image resizeMode={"contain"} resizeMode={"contain"}source={{ uri: image }} style={Style.icon} />
        <View style={Style.midContainer}>
          <Text style={Style.title}>{name}</Text>
        </View>
        <View style={Style.buttonContainer}>
          <SoundPriceContainer oldPrice={delprice} newPrice={price} />
        </View>
      </TouchableOpacity>
    );
  }
  return (
    <TouchableOpacity
      style={[Style.container, { ...Helpers.rowReverse }]}
      onPress={_handleDisplayItem}
    >
      <Image
        source={{ uri: image }}
        style={[
          Style.icon,
          props.language === "ar"
            ? {
                borderTopRightRadius: 10,
                borderBottomRightRadius: 10,
                borderTopLeftRadius: 0,
                borderBottomLeftRadius: 0
              }
            : null
        ]}
      />
      <View
        style={[
          Style.midContainer,
          props.language === "ar"
            ? [Helpers.crossEnd, Metrics.tinyHorizontalMargin]
            : null
        ]}
      >
        <Text
          style={[
            Style.title,
            props.language === "ar" ? { textAlign: "right" } : null
          ]}
        >
          {name}
        </Text>
      </View>
      <View style={Style.buttonContainer}>
        <SoundPriceContainer oldPrice={delprice} newPrice={price} />
      </View>
    </TouchableOpacity>
  );
};

const mapStateToProps = state => {
  return {
    language: state.auth.language,
    isAuthenticated: state.app.isAuthenticated,

  };
};

const mapDispatchToProps = dispatch => {
  return {
    onDispatchSetItemToBasket: song => dispatch(setItemToBasket(song))
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(SongContainer);
