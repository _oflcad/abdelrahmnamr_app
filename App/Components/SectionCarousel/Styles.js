import { StyleSheet } from "react-native";
import { Metrics,  Colors } from "../../Theme";

export default StyleSheet.create({
  list: {
    width: Metrics.width,
    height: 140,
    ...Metrics.smallHorizontalMargin,
  },
  card: {
    width: 200,
    height: 100,
    margin: 10,
    padding: 10,
    backgroundColor: Colors.white,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    borderRadius: 10,
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 8
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,
    elevation: 16
  },
  image: {
    width: 60,
    height: 60,
    aspectRatio: 1,
    position: "absolute",
    top: "20%",
    right: 10,
    zIndex: 10
  },
  text: {
    position: "absolute",
    bottom: "50%",
    left: 10,
    fontFamily: "robotoB",
    fontSize: 20,
    color: Colors.black
  }
});