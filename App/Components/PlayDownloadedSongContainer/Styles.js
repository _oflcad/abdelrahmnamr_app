import { StyleSheet } from "react-native";
import { Helpers, Metrics, Fonts, Colors } from "../../Theme";

export default StyleSheet.create({
    container: {
        ...Metrics.smallHorizontalMargin,
        ...Metrics.smallVerticalMargin,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        width: Metrics.width - 20,
        height: 60,
        backgroundColor: Colors.primary,
        borderRadius: 10,
        shadowColor: Colors.grey200,
    
        shadowOffset: {
          width: 0,
          height: 8
        },
        shadowOpacity: 0.44,
        shadowRadius: 7,
        elevation: 16
      },
      Image: {
        width: Metrics.width - 20,
        height: 160,
        padding: 5,
        borderRadius: 10
      },
      icon: {
        width: 60,
        height: 60,
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 10
      },
      midContainer: {
        flexGrow: 2,
        alignItems: "flex-start",
        justifyContent: "flex-start",
        marginLeft: 10
      },
      title: {
        fontFamily: "robotoR",
        fontSize: 18,
        color: Colors.black
      },
      buttonContainer: {
        ...Metrics.smallHorizontalMargin,
        ...Metrics.smallVerticalMargin,
    
      }
});

