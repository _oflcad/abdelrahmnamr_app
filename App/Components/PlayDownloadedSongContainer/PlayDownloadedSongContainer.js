import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import { Colors, Helpers, Images } from "../../Theme";
import Styles from "./Styles";
import Reactotron from "reactotron-react-native";
import {
    startPlayingDownload,
} from "../../Actions/Media/Actions";
import { connect } from "react-redux";

const PlayDownloadedSongContainer = ({ item, onDispatchPlaySong }) => {
    const [hasMultiple, setHasMultiple] = useState(false);
    const [canPlay, setCanPlay] = useState(false);
    useEffect(() => {
        item.hasmultiple ? setHasMultiple(true) : setHasMultiple(false);
        if(hasMultiple){
            item.downloadedAudios.forEach(el => {
                if(el.downloadUri !== null) {
                    setCanPlay(true);
                }
            })
        } else {
            item.downloadedAudios[0].downloadUri !== null ? setCanPlay(true) : setCanPlay(false);
        }
        
    })

  const { name, image } = item;

  const _handleDownloadPlaySong = () => {
    onDispatchPlaySong(item);
  };
  return (
    <TouchableOpacity disabled={canPlay} style={Styles.container} onPress={_handleDownloadPlaySong}>
      <Image resizeMode={"contain"} resizeMode={"contain"}source={{ uri: image }} style={Styles.icon} />
      <View style={Styles.midContainer}>
        <Text style={Styles.title}>{name}</Text>
      </View>
      <View style={Styles.buttonContainer}>
        <Image resizeMode={"contain"} resizeMode={"contain"}source={Images.play} style={{ width: 25, height: 25 }} />
      </View>
    </TouchableOpacity>
  );
};


const mapDispatchToProps = dispatch => {
  return {
    onDispatchPlaySong: song => dispatch(startPlayingDownload(song)),  
  }
};
export default connect(null, mapDispatchToProps)(PlayDownloadedSongContainer);