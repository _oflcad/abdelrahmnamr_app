import React, { useState, useEffect } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { Words, Helpers, Metrics, Fonts, Colors } from "../../Theme";
import { Ionicons } from "@expo/vector-icons";
import Styles from "./Styles";
import { handleAutoPlay } from "../../Actions/Media/Actions";
import { connect } from "react-redux";
const SettingsSection = props => {
  const [autoPlay, setAutoPlay] = useState(false);

  useEffect(() => {
    setAutoPlay(props.isAutoPlay);
  });

  const _handleAutoPlay = () => {
    props.onDispatchHandleAutoPlay();
  };
  if(props.language === 'en'){
    return (
      <View style={[Styles.container]}>
        <TouchableOpacity
          onPress={_handleAutoPlay}
          style={[
            Helpers.row,
            Helpers.mainSpaceBetween,
            Helpers.crossCenter,
            Metrics.tinyHorizontalMargin,
            Styles.buttonContainer
          ]}
        >
          <Text style={[Fonts.normal, { textAlign: "left" }, Metrics.tinyHorizontalMargin]}>
            {Words.en.settingsSection.autoPlay}
          </Text>
          <Ionicons
            name={
              autoPlay ? "ios-checkmark-circle" : "ios-checkmark-circle-outline"
            }
            size={25}
            color={autoPlay ? Colors.success : Colors.grey100}
          />
        </TouchableOpacity>
      </View>
    );
  }
  return (
    <View style={[Styles.container]}>
      <TouchableOpacity
        onPress={_handleAutoPlay}
        style={[
          Helpers.rowReverse,
          Helpers.mainSpaceBetween,
          Helpers.crossCenter,
          Metrics.tinyHorizontalMargin,
          Styles.buttonContainer
        ]}
      >
        <Text style={[Fonts.normal, { textAlign: "left" }, Metrics.tinyHorizontalMargin]}>
          {Words.ar.settingsSection.autoPlay}
        </Text>
        <Ionicons
          name={
            autoPlay ? "ios-checkmark-circle" : "ios-checkmark-circle-outline"
          }
          size={25}
          color={autoPlay ? Colors.success : Colors.grey100}
        />
      </TouchableOpacity>
    </View>
  );
};

const mapStateToProps = state => {
  return {
    isAutoPlay: state.media.isAutoPlay,
    language: state.auth.language,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onDispatchHandleAutoPlay: () => dispatch(handleAutoPlay())
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(SettingsSection);
