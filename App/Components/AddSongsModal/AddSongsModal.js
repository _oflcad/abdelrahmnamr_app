import React, { useState } from "react";
import { View, TouchableOpacity, Modal, FlatList, Text } from "react-native";
import Styles from "./Styles";
import {
  Colors,
  ApplicationStyles,
  Helpers,
  Metrics,
  Fonts,
  Words
} from "../../Theme";
import { connect } from "react-redux";
import {
  createPlaylist,
  closeAddSongsModal
} from "../../Actions/Media/Actions";
import SelecteableSongContainer from "../SelecteableSongContainer/SelecteableSongContainer";
import EmptyPlaylist from "../EmptyPlaylist/EmptyPlaylist";

const AddSongsModal = props => {
  const _handleCloseModam = () => {
    props.onDispatchCloseModal();
  };

  const _renderItem = ({ item }) => {
    return <SelecteableSongContainer item={item} />;
  };

  return (
    <Modal
      style={Styles.wrapper}
      animationType={"slide"}
      transparent
      visible={props.isAddSongsToPlaylistModalVisible}
      hasBackdrop
      backdropColor={Colors.grey200}
      useNativeDriver
      onBackdropPress={props.onDispatchCloseModal}
    >
      <View style={Styles.container}>
        <TouchableOpacity
          style={[ApplicationStyles.modalBackground]}
          onPress={_handleCloseModam}
        ></TouchableOpacity>
        <View style={[ApplicationStyles.addSongsModal]}>
          <View style={[Metrics.horizontalMargin, Metrics.verticalMargin]}>
            <Text style={[Fonts.h5]}>
              {props.language === "en"
                ? Words.en.allAudios
                : Words.ar.allAudios}
            </Text>
          </View>
          <View style={[ApplicationStyles.modalDividerHorizontal]} />
          <FlatList
            showsVerticalScrollIndicator={false}
            contentContainerStyle={[
              Helpers.mainCenter,
              Metrics.tinyVerticalMargin,
              { paddingBottom: 60 }
            ]}
            contentInset={{ top: 0, bottom: 20, left: 0, right: 0 }}
            contentInsetAdjustmentBehavior="automatic"
            data={props.userAudios}
            renderItem={_renderItem}
            keyExtractor={(item, index) => "key" + index}
            ListEmptyComponent={<EmptyPlaylist />}
          />
        </View>
      </View>
    </Modal>
  );
};

const mapStateToProps = state => {
  return {
    isAddSongsToPlaylistModalVisible:
      state.media.isAddSongsToPlaylistModalVisible,
    latestAudios: state.media.latestAudios,
    language: state.auth.language,
    userAudios: state.media.userAudios
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onDispatchCreatePlaylist: obj => dispatch(createPlaylist(obj)),
    onDispatchCloseModal: () => dispatch(closeAddSongsModal())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddSongsModal);
