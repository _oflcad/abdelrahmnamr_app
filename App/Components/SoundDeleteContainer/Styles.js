import { StyleSheet } from "react-native";
import { Helpers, Metrics, Fonts, Colors } from "../../Theme";

export default StyleSheet.create({
    deleteButton: {
        ...Helpers.center,
        width: 40,
        height: 60,
    }
});
