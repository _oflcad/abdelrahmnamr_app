import { StyleSheet } from "react-native";
import { Helpers, Metrics, Fonts, Colors } from "../../Theme";

export default StyleSheet.create({
    buttonContainer: {
        width: 50,
        height: 50,
        borderRadius: 20,
        backgroundColor: Colors.white,
        ...Helpers.center,
    },
    wrapper: {
        width: Metrics.width,
        height: 80,
        ...Helpers.center,
    }
});
