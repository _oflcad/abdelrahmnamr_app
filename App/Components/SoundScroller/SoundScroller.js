import React, { Component } from "react";
import { View, Text, FlatList, ActivityIndicator } from "react-native";
import Style from "./Styles";
import { connect } from "react-redux";
import { Helpers, Colors, Metrics } from "../../Theme";
import SongContainer from "../SongContainer/SongContainer";
class SoundScroller extends Component {
  constructor(props) {
    super(props);
  }

  _renderItem = ({ item }) => {
    return <SongContainer item={item} />;
  };

  render() {
    const { latestAudios } = this.props;
    return (
      <FlatList
        style={[Metrics.smallVerticalMargin]}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={[
          Helpers.mainCenter,
          Metrics.tinyVerticalMargin,
          { paddingBottom: 60 }
        ]}
        data={latestAudios}
        contentInset={{ top: 0, bottom: 20, left: 0, right: 0 }}
        contentInsetAdjustmentBehavior="automatic"
        renderItem={this._renderItem}
        keyExtractor={(item, index) => "key" + item.name}
        ListEmptyComponent={
          <View style={[Helpers.center, Metrics.horizontalMargin]}>
            <ActivityIndicator size={"large"} color={Colors.grey200} />
          </View>
        }
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    latestAudios: state.media.latestAudios
  };
};

export default connect(mapStateToProps, null)(SoundScroller);
