import { StyleSheet } from "react-native";
import { Helpers, Metrics, Fonts, Colors } from "../../Theme";

export default StyleSheet.create({
  container: {
      ...Metrics.marginTop,
    ...Helpers.column,
    ...Metrics.smallHorizontalMargin,
    width: Metrics.width - 25,
    height: 90,
    borderRadius: 10,
    backgroundColor: Colors.white,
    borderRadius: 10,
    shadowColor: Colors.grey200,

    shadowOffset: {
      width: 0,
      height: 8
    },
    shadowOpacity: 0.66,
    shadowRadius: 11,
    elevation: 16
  },
  topContainer: {
      ...Helpers.row,
      ...Helpers.mainStart,
      ...Metrics.horizontalMargin,
  }
});
