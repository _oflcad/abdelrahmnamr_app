import { StyleSheet } from "react-native";
import { Helpers, Metrics, Fonts, Colors } from "../../Theme";

export default StyleSheet.create({
    container: {
        height: 60,
        width: 60,
        ...Helpers.column,
        ...Metrics.smallVerticalMargin,
    }   
});
