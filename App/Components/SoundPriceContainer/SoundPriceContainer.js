import React from 'react'
import { View, Text } from 'react-native'
import { Helpers, Fonts, Metrics } from '../../Theme'
import Styles from './Styles'

const SoundPriceContainer = ({oldPrice, newPrice}) => {
    return (
        <View style={[Styles.container]}>
            <View>
                <Text style={[Fonts.oldPrice]}>{oldPrice}$</Text>
            </View>
            <View style={[Metrics.crossEnd, Metrics.tinyVerticalMargin]}>
                <Text style={[Fonts.newPrice]}>{newPrice}$</Text>
            </View>
        </View>
    )
}


export default SoundPriceContainer
