import { StyleSheet } from "react-native";
import { Helpers, Metrics, Fonts, Colors } from "../../Theme";

export default StyleSheet.create({
  container: {
    ...Helpers.row,
    width: "95%",
    height: 80,
    backgroundColor:Colors.white,
    shadowColor: Colors.black,
    borderRadius: 10,
    shadowOffset: {
      width: 0,
      height: 8
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,
    elevation: 16
  },
  thumbnailContainer: {
    flex:1,
    ...Helpers.center,
  },
  midContainer: {
      flex:2,
      ...Helpers.column,
      ...Helpers.crossStart,
      ...Helpers.mainCenter
  },
  icon: {
    width: 30,
    height: 30
  }
});
