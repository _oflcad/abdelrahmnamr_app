import React, { useState, useEffect } from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import Style from "./Styles";
import { connect } from "react-redux";
import RoundCheckbox from "rn-round-checkbox";
import { Helpers, Metrics } from "../../Theme";
import { addSongToPlaylist, removeSongFromPlaylist } from "../../Actions/Media/Actions";
const SongContainer = props => {
  const [isChecked, setChecked] = useState(false);
  const { name, image } = props.item;
  
  const _selectSong = () => {
    if (isChecked) {
      setChecked(false)
      props.onDispatchRemoveSongFromPlaylist(props.item)
    }
    else {
      setChecked(true)
      props.onDispatchAddSongToPlaylist(props.item)
    }
  }

  useEffect(() => {
    setChecked(props.playlistToDisplay.songs.some(elem => elem === props.item))
  });
  
    return (
      <TouchableOpacity style={[Style.container, Metrics.smallHorizontalMargin]} onPress={_selectSong}>
        <Image resizeMode={"contain"} resizeMode={"contain"}source={{uri: image}} style={Style.icon} />
        <View style={Style.midContainer}>
          <Text style={Style.title}>{name}</Text>
        </View>
        <View style={[Helpers.colCenter, Helpers.mainEnd, Metrics.smallHorizontalMargin]} >
          <RoundCheckbox
            size={24}
            checked={isChecked}
          />
        </View>
      </TouchableOpacity>  
    );
 
};

const mapStateToProps = state => {
  return {
    selectedSongs: state.media.selectedSongs,
    playlistToDisplay: state.media.playlistToDisplay,
    language: state.auth.language,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onDispatchAddSongToPlaylist: song => dispatch(addSongToPlaylist(song)),
    onDispatchRemoveSongFromPlaylist: song => dispatch(removeSongFromPlaylist(song))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SongContainer);
