import { StyleSheet } from "react-native";
import { Helpers, Metrics, Fonts, Colors } from "../../Theme";

export default StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    width: '95%',
    height: 60,
    backgroundColor: Colors.white,
    borderRadius: 10,
    margin: 5,
    shadowColor: Colors.grey200,

    shadowOffset: {
      width: 0,
      height: 8
    },
    shadowOpacity: 0.66,
    shadowRadius: 11,
    elevation: 16
  },
  Image: {
    width: Metrics.width - 20,
    height: 160,
    padding: 5,
    borderRadius: 10
  },
  icon: {
    width: 60,
    height: 60,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10
  },
  midContainer: {
    flexGrow: 2,
    alignItems: "flex-start",
    justifyContent: "flex-start",
    marginLeft: 10
  },

  title: {
    fontFamily: "robotoR",
    fontSize: 18,
    color: Colors.black
  },
});
