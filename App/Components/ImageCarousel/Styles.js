import { StyleSheet } from "react-native";
import { Helpers, Metrics, Fonts, Colors } from "../../Theme";

export default StyleSheet.create({
  container: {
    ...Helpers.fullWidth,
  },
  Image: {
    width: Metrics.width - 20,
    height: 160,
    padding: 5,
    borderRadius: 10
  }
});
