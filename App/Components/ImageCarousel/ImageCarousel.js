import React, { Component } from "react";
import { View, Text, StyleSheet, Image } from "react-native";
import { connect } from "react-redux";
import Carousel from "react-native-snap-carousel";
import { Helpers, Metrics, Fonts, Colors } from "../../Theme";
import Style from "./Styles";

class CarouselContainer extends Component {
  state = {
    activeSlide: 0,
    albums: [
      {
        title: "albumCovers",
        imageUrl: require("../../Assets/Images/zero.jpg")
      },
      {
        title: "albumCovers",
        imageUrl: require("../../Assets/Images/first.jpg")
      },
      {
        title: "albumCovers",
        imageUrl: require("../../Assets/Images/second.jpg")
      },
      {
        title: "albumCovers",
        imageUrl: require("../../Assets/Images/third.jpg")
      },
      {
        title: "albumCovers",
        imageUrl: require("../../Assets/Images/fourth.jpg")
      },
      {
        title: "albumCovers",
        imageUrl: require("../../Assets/Images/fifth.jpg")
      }
    ]
  };

  _renderItem({ item, index }) {
    return <Image resizeMode={"contain"} resizeMode={"contain"}source={item.imageUrl} style={Style.Image} resizeMode='cover'/>;
  }

  render() {
    const { albums } = this.state;
    return (
      <View style={Style.container}>
        <Carousel
          loop
          autoplay
          layout={"stack"}
          layoutCardOffset={18}
          ref={c => {
            this._carousel = c;
          }}
          data={albums}
          renderItem={this._renderItem}
          sliderWidth={Metrics.width - 5}
          itemWidth={Metrics.width - 10}
          onSnapToItem={index => this.setState({ activeSlide: index })}
        />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    latestImages: state.app.latestImages
  };
};

export default connect(mapStateToProps, null)(CarouselContainer);
