import React, { useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Modal,
  KeyboardAvoidingView,
  Image
} from "react-native";
import Styles from "./Styles";
import {
  Helpers,
  Fonts,
  Words,
  Colors,
  ApplicationStyles,
  Metrics,
  Images
} from "../../Theme";
import { connect } from "react-redux";
import {
  changePlaylistTitle,
  closePlaylistInfoModal,
  deletePlaylist
} from "../../Actions/Media/Actions";
import NavigationService from "../../Services/NavigationService";
import moment from "moment";

const InfoModal = props => {
  let playlistLength = props.playlistToDisplay.songs.length;
  const { createdAt } = props.playlistToDisplay;
  const [isChecked, setIsChecked] = useState(false);
  const _handleEditTitle = () => {
    console.log("edit title");
  };

  const _handleDeletePlaylist = () => {
    setIsChecked(true);
    console.log("delete playlist");
  };

  const _handleCloseModal = () => {
    setIsChecked(false);
    props.onDispatchCloseModal();
  };

  const _handleDenial = () => {
    setIsChecked(false);
  };

  const _handleApproval = () => {
    //TO DO DELETE PLAYLIST
    props.onDispatchDeletePlaylist(props.playlistToDisplay);
    props.onDispatchCloseModal();
    NavigationService.navigate("Playlist");
  };

  return (
    <KeyboardAvoidingView>
      <Modal
        style={Styles.wrapper}
        animationType={"slide"}
        transparent
        visible={props.isAddPlaylistInfoModalVisible}
        hasBackdrop
        backdropColor={Colors.grey200}
        useNativeDriver
        onBackdropPress={props.onDispatchCloseModal}
      >
        <View style={Styles.container}>
          <TouchableOpacity
            activeOpacity={1}
            style={[ApplicationStyles.modalBackground]}
            onPress={_handleCloseModal}
          ></TouchableOpacity>
          <View style={[ApplicationStyles.playlistInfoModal]}>
            <View style={Styles.topContainer}>
              <Image resizeMode={"contain"} resizeMode={"contain"}source={Images.musicalNotes} style={Styles.icon} />
              <Text style={[Fonts.h2, Metrics.tinyVerticalMargin]}>
                {props.playlistToDisplay.title}
              </Text>
              <Text style={[Fonts.subTitle]}>{playlistLength} audio</Text>
              <Text style={[Fonts.label]}>{moment(createdAt).fromNow()} </Text>
            </View>
            {isChecked ? (
              <View style={Styles.deleteContainer}>
                <View style={[Helpers.center, { flex: 1 }]}>
                  <Text style={Styles.error}>{Words.en.check}</Text>
                </View>
                <View style={[Styles.deleteButtonContainer]}>
                  <TouchableOpacity
                    onPress={_handleDenial}
                    style={[
                      Styles.buttonContainer,
                      { backgroundColor: Colors.black }
                    ]}
                  >
                    <Image resizeMode={"contain"} resizeMode={"contain"}source={Images.alert} style={Styles.smallIcon} />
                    <Text
                      style={[
                        Fonts.normal,
                        Metrics.tinyHorizontalPadding,
                        { color: Colors.white }
                      ]}
                    >
                      {Words.ar.deny}
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={_handleApproval}
                    style={[Styles.buttonContainer, Styles.borderForButton]}
                  >
                    <Image resizeMode={"contain"} resizeMode={"contain"}source={Images.checkMark} style={Styles.smallIcon} />
                    <Text
                      style={[
                        Fonts.normal,
                        Metrics.tinyHorizontalPadding,
                        { color: Colors.black }
                      ]}
                    >
                      {Words.ar.confirm}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            ) : (
              <View style={Styles.bottomContainer}>
                <TouchableOpacity
                  onPress={_handleDeletePlaylist}
                  style={[Styles.buttonContainer, Styles.borderForButton]}
                >
                  <Image resizeMode={"contain"} resizeMode={"contain"}source={Images.trashBin} style={Styles.smallIcon} />
                  <Text style={[Fonts.normal, Metrics.tinyHorizontalPadding]}>
                    {Words.ar.deletePlaylist}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={_handleEditTitle}
                  style={[
                    Styles.buttonContainer,
                    { backgroundColor: Colors.black }
                  ]}
                >
                  <Image resizeMode={"contain"} resizeMode={"contain"}source={Images.create} style={Styles.smallIcon} />
                  <Text
                    style={[
                      Fonts.subTitle,
                      Metrics.tinyHorizontalPadding,
                      { color: Colors.white }
                    ]}
                  >
                    {Words.ar.renamePlaylist}
                  </Text>
                </TouchableOpacity>
              </View>
            )}
          </View>
        </View>
      </Modal>
    </KeyboardAvoidingView>
  );
};

const mapStateToProps = state => {
  return {
    playlistToDisplay: state.media.playlistToDisplay,
    isAddPlaylistInfoModalVisible: state.media.isAddPlaylistInfoModalVisible
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onDispatchCEditPlaylistTitle: title => dispatch(changePlaylistTitle(title)),
    onDispatchCloseModal: () => dispatch(closePlaylistInfoModal()),
    onDispatchDeletePlaylist: playlist => dispatch(deletePlaylist(playlist))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(InfoModal);
